<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!-- SAB BANNER START-->
        <div class="sab_banner overlay">
            <div class="container">
                <div class="sab_banner_text">
                    <h2>Blog Post</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Blog Post</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SAB BANNER END-->

        <!-- CITY EVENT2 WRAP START-->
        <div class="city_blog2_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="city_blog2_fig">
                            <figure class="overlay">
                                <img src="extra-images/blog-01.jpg" alt="">
                                <a class="paly_btn" data-rel="prettyPhoto"
                                    href="extra-images/blog1.jpg">+</a>
                                <span class="city_blog2_met">Post</span>
                            </figure>
                            <div class="city_blog2_list">
                                <ul class="city_meta_list">
                                    <li><a href="blog_contabilidad" target="_blank"><i class="fa fa-calendar"></i>Enero 28,2022</a></li>
                                    <!--li><a href="#"><i class="fa fa-comment-o"></i>0 Comments</a></li-->
                                </ul>
                                <div class="city_blog2_text">
                                    <h5><a href="blog_contabilidad" target="_blank">CONOCE NUESTRA AREA DE CONTABILIDAD Y FINANZAS</a></h5>
                                    <p>Porque tu seguridad es importante para nosotros, hemos implementado esta nueva área de Contabilidad y Finanzas...</p>
                                    <a class="see_more_btn" href="blog_contabilidad" target="_blank">Leer más<i class="fa icon-right-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="city_blog2_fig">
                            <figure class="overlay">
                                <img src="extra-images/blog-02.jpg" alt="">
                                <a class="paly_btn" data-rel="prettyPhoto"
                                    href="extra-images/blog2.jpg">+</a>
                                <span class="city_blog2_met">Post</span>
                            </figure>
                            <div class="city_blog2_list">
                                <ul class="city_meta_list">
                                    <li><a href="blog_rehabilitacion" target="_blank"><i class="fa fa-calendar"></i>Enero 28,2022</a></li>
                                    <!--li><a href="#"><i class="fa fa-comment-o"></i>0 Comments</a></li-->
                                </ul>
                                <div class="city_blog2_text">
                                    <h5><a href="blog_rehabilitacion" target="_blank">REHABILITACIÓN EN PACIENTES POST COVID-19</a></h5>
                                    <p>La rehabilitación es de suma importancia para las personas que han sido afectadas por la covid-19...</p>
                                    <a class="see_more_btn" href="blog_rehabilitacion" target="_blank">Leer más<i class="fa icon-right-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-4 col-sm-6">
                        <div class="city_blog2_fig">
                            <figure class="overlay">
                                <img src="extra-images/blog03.jpg" alt="">
                                <a class="paly_btn" data-rel="prettyPhoto"
                                    href="https://www.youtube.com/watch?v=SAaevusBnNI">+</a>
                                <span class="city_blog2_met">Post</span>
                            </figure>
                            <div class="city_blog2_list">
                                <ul class="city_meta_list">
                                    <li><a href="#"><i class="fa fa-calendar"></i>Enero 28,2022</a></li> 
                                    <li><a href="#"><i class="fa fa-comment-o"></i>0 Comments</a></li>
                                </ul>
                                <div class="city_blog2_text">
                                    <h5><a href="#">REHABILITACIÓN EN PACIENTES POST COVID-19</a></h5>
                                    <p>La rehabilitación es de suma importancia para las personas que han sido afectadas por la covid-19...</p>
                                    <a class="see_more_btn" href="#">READ MORE<i class="fa icon-right-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- CITY EVENT2 WRAP END-->

        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->
    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="js/jquery.js"></script>
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Image Filterable JavaScript-->
    <script src="js/jquery-filterable.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>

    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <script>
    document.documentElement.className = 'js';
    </script>
</body>

</html>