<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/sidebar-widget.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!-- SAB BANNER START-->
        <div class="sab_banner overlay">
            <div class="container">
                <div class="sab_banner_text">
                    <h2>FISIOHOGAR</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">HOME</a></li>
                        <li class="breadcrumb-item active"><a href="#">SEDES</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SAB BANNER END-->

        <!-- CITY EVENT2 WRAP START-->
        <div class="city_event2_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="city_event2_list2">
                            <ul>
                                <li>
                                    <div class="city_event2_list2_row">
                                        <div class="city_event2_list2_fig">
                                            <figure class="box">
                                                <div class="box-layer layer-1"></div>
                                                <div class="box-layer layer-2"></div>
                                                <div class="box-layer layer-3"></div>
                                                <img src="extra-images/sede-trujillo.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="city_blog_text event2">
                                            <span>Sede</span>
                                            <h4><a href="#">Trujillo</a></h4>
                                            <ul class="city_meta_list">
                                                <li><a href="#"><i class="fa fa-calendar"></i>Lunes - Sabado</a></li>
                                                <li><a href="#"><i class="fa fa-clock-o"></i>08.00AM - 08.00PM</a></li>
                                                <li><a href="#"><i class="fa fa-map-marker"></i>La Libertad</a>
                                                </li>
                                            </ul>
                                            <p>Av. Nicolas de Pierola 872 Urb. Primavera, 4to y 5to piso, área de
                                                Terapias Integrales</p>
                                            <div class="city_blog_social">
                                                <a class="theam_btn border-color" href="trujillo" tabindex="0" target="_blank">Ver mapa</a>
                                                <div class="city_blog_icon_list">
                                                    <ul class="social_icon">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-google"></i></a></li>
                                                    </ul>
                                                    <a class="share_icon" href="#"><i class="fa icon-social"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                <div class="city_event2_list2_row">
                                        <div class="city_event2_list2_fig">
                                            <figure class="box">
                                                <div class="box-layer layer-1"></div>
                                                <div class="box-layer layer-2"></div>
                                                <div class="box-layer layer-3"></div>
                                                <img src="extra-images/sede-chimbote.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="city_blog_text event2">
                                            <span>Sede</span>
                                            <h4><a href="#">Chimbote</a></h4>
                                            <ul class="city_meta_list">
                                                <li><a href="#"><i class="fa fa-calendar"></i>Lunes - Sabado</a></li>
                                                <li><a href="#"><i class="fa fa-clock-o"></i>08.00AM - 08.00PM</a></li>
                                                <li><a href="#"><i class="fa fa-map-marker"></i>Ancash</a>
                                                </li>
                                            </ul>
                                            <p>Av. Jose Balta 558, 4to piso, área de Medicina Física y
                                                Rehabilitación</p>
                                            <div class="city_blog_social">
                                                <a class="theam_btn border-color" href="chimbote" tabindex="0" target="_blank">Ver mapa</a>
                                                <div class="city_blog_icon_list">
                                                    <ul class="social_icon">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-google"></i></a></li>
                                                    </ul>
                                                    <a class="share_icon" href="#"><i class="fa icon-social"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                <div class="city_event2_list2_row">
                                        <div class="city_event2_list2_fig">
                                            <figure class="box">
                                                <div class="box-layer layer-1"></div>
                                                <div class="box-layer layer-2"></div>
                                                <div class="box-layer layer-3"></div>
                                                <img src="extra-images/sede-huamachuco.jpg" alt="">
                                            </figure>
                                        </div>
                                        <div class="city_blog_text event2">
                                            <span>Sede</span>
                                            <h4><a href="#">Huamachuco</a></h4>
                                            <ul class="city_meta_list">
                                                <li><a href="#"><i class="fa fa-calendar"></i>Lunes - Sabado</a></li>
                                                <li><a href="#"><i class="fa fa-clock-o"></i>08.00AM - 08.00PM</a></li>
                                                <li><a href="#"><i class="fa fa-map-marker"></i>La Libertad</a>
                                                </li>
                                            </ul>
                                            <p>Av. 10 de Julio N°104, 2do piso, área de Terapia Física</p>
                                            <div class="city_blog_social">
                                                <a class="theam_btn border-color" href="huamachuco" tabindex="0" target="_blank">Ver mapa</a>
                                                <div class="city_blog_icon_list">
                                                    <ul class="social_icon">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-google"></i></a></li>
                                                    </ul>
                                                    <a class="share_icon" href="#"><i class="fa icon-social"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CITY EVENT2 WRAP END-->


        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->

    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="js/jquery.js"></script>
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Image Filterable JavaScript-->
    <script src="js/jquery-filterable.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>

    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <!--Custom JavaScript-->
    <script src="js/visita.js"></script>
    <script>
    document.documentElement.className = 'js';
    </script>
</body>

</html>