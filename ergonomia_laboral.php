<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/sidebar-widget.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!-- SAB BANNER START-->
        <div class="sab_banner overlay">
            <div class="container">
                <div class="sab_banner_text">
                    <h2>PROGRAMAS PROFESIONALES</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../">Home</a></li>
                        <li class="breadcrumb-item active"><a href="atencion_domicilio">Programas profesionales</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SAB BANNER END-->

        <!-- CITY SERVICES2 WRAP START-->
        <div class="city_health_department">
            <div class="container">
                <div class="section_heading border">
                    <h2>Ergonomía Laboral</h2>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="city_about_fig fig2">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/about-ergonomia-laboral.jpg" alt="No disponible">
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="city_about_list list2">
                            <!--SECTION HEADING START-->
                            <!--SECTION HEADING END -->
                            <div class="city_about_text ">
                                <p>Es importante conocer el estado de salud de cada trabajador, con el programa de ergonomía laboral
                                    podemos obtener conocimiento de cómo se encuentra la postura de cada uno, y que medidas preventivas se puede llevar a cabo,
                                    para salvaguardar la seguridad en el trabajo, ayudando a la parte postural y fisiológica del cuerpo.</p>
                                <p>
                                    También permite aplicar tratamientos fisioterapéuticos para aquellas personas que
                                    sufren dificultades motoras o disponen de poco tiempo para desplazarse en la
                                    clínica. Esta es una buena</p>
                            </div>
                            <!--SECTION HEADING START-->
                            <div class="section_heading border">
                                <!-- <span>FisioHogar</span> -->
                                <h2>Objetivo</h2>
                            </div>
                            <!--SECTION HEADING END-->
                            <div class="city_about_text ">
                                <p>Se busca adaptar a la persona al puesto de trabajo, mejorando la eficiencia, seguridad y bienestar de usuarios y trabajadores.</p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="city_health2_wrap">
                    <div class="container"><br><br><br></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="city_about_list list2">
                            <div class="section_heading border">
                                <h3>Resultados</h3>
                            </div>
                            <ul class="city_about_link">
                                <li><a href="#"><i class="fa fa-star"></i>Ayudar a mejorar las condiciones de trabajo y por tanto la salud de los trabajadores.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Reducen las demandas físicas del trabajo dando como resultado menos lesiones o dolencias.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>La mejora del entorno laboral, repercute positivamente en la eficiencia y la productividad.</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CITY SERVICES2 WRAP END-->

        <div class="city_health2_wrap">
            <div class="container"><br> </div>
        </div>
        <!-- CITY SERVICES2 WRAP END-->

        <!-- CITY TREATMENT WRAP END-->

        <!--CITY AWARD WRAP START-->
        <!--<div class="city_award_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="city_award_list">
                            <span><i class="fa  icon-politician"></i></span>
                            <div class="city_award_text">
                                <h3 class="counter">1495</h3>
                                <h3>Established</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="city_award_list">
                            <span><i class="fa icon-cube"></i></span>
                            <div class="city_award_text">
                                <h3 class="counter">75,399</h3>
                                <h3>KM Square</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="city_award_list">
                            <span><i class="fa icon-demographics"></i></span>
                            <div class="city_award_text">
                                <h3 class="counter">1,435,268</h3>
                                <h3>Total Population</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!--CITY AWARD WRAP END-->

        <!--CITY PROJECT WRAP START-->
        <div class="city_project_wrap">
            <div class="container">
                <!--SECTION HEADING START-->
                <div class="section_heading center margin-bottom">
                    <span>FisioHogar</span>
                    <h2>Servicios a domicilio</h2>
                </div>
                <div class="city-project-slider">
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-pediatrica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA PEDIÁTRICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-lenguaje.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">TERAPIA DE LENGUAJE</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-traumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA TRAUMATOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-reumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA REUMATOLÓGICA</a>
                                    </h3>
                                </div>
                            </figure>
                        </div>
                    </div>

                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-neurologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA NEUROLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-respiratoria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA RESPIRATORIA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-complementaria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-ocupacional.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">TERAPIA OCUPACIONAL</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-uroginecologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA UROGINECOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--CITY PROJECT WRAP END-->

        <!--CITY SPECIAL2 DREAM START-->
        <div class="city_special2_dream">
            <div class="container">
                <div class="city_special2_text">
                    <h3>FisioHogar</h3>
                    <h2 style="line-height: initial;">Llámanos o escríbenos para mayor información sobre nuestros servicios</h2>
                    <h3>Llámanos al <a href="tel:+51934571849" style="color:white;">+51 934 571 849 </a> o <a href="tel:+51949307984" style="color:white;">+51 949 307 984</a></h3>
                    <a class="theam_btn" href="https://wa.link/fxm9m8" target="_blank" tabindex="0">Escríbenos</a>
                </div>
            </div>
        </div>
        <!--CITY SPECIAL2 DREAM END-->


        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->
    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="js/jquery.js"></script>
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Image Filterable JavaScript-->
    <script src="js/jquery-filterable.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>

    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <script>
        document.documentElement.className = 'js';
    </script>
</body>

</html>