<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/sidebar-widget.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
    <!-- Sweetalert2 -->
    <link href="admin/assets/css/sweetalert2.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!-- SAB BANNER START-->
        <div class="sab_banner overlay">
            <div class="container">
                <div class="sab_banner_text">
                    <h2>Terapia Ocupacional</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">HOME</a></li>
                        <li class="breadcrumb-item active"><a href="#">Terapia Ocupacional</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SAB BANNER END-->

        <!-- CITY SERVICES2 WRAP START-->
        <div class="city_health_department">
            <div class="container">
                <div class="city_health2_fig">
                    <figure class="box">
                        <div class="box-layer layer-1"></div>
                        <div class="box-layer layer-2"></div>
                        <div class="box-layer layer-3"></div>
                        <img src="extra-images/banner-ocupacional.jpg" alt="">
                    </figure>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="sidebar_widget">
                            <!-- SIDE SUBMIT FORM START-->
                            <div class="side_submit_form">
                                <h4 class="sidebar_title">Escribenos!</h4>
                                <div class="side_submit_field">
                                    <input type="text" id="txtNombreSide" placeholder="Nombre">
                                    <input type="text" id="txtCiudadSide" placeholder="Ciudad">
                                    <input type="text" id="txtDNISide" placeholder="DNI">
                                    <input type="text" id="txtEmailSide" placeholder="Email">
                                    <input type="text" id="txtTelefonoSide" placeholder="Telefono">
                                    <textarea id="txtTextSide">Hola! Quiero reservar una cita.</textarea>
                                    <button style="width: 100%" class="theam_btn btn2" id="btnConsultarSide">CONSULTAR</button>
                                </div>
                            </div>
                            <!-- SIDE SUBMIT FORM END-->

                            <!-- SIDE CONTACT INFO START-->
                            <div class="side_contact_info">
                                <h4 class="sidebar_title">Contacto</h4>
                                <ul class="side_contact_list">
                                    <li>
                                        <div class="side_contact_text">
                                            <h6><i class="fa fa-tty"></i>Telefonos</h6>
                                            <a href="tel:+51934571849">+51 934 571 849 </a>
                                            <a href="tel:+51949307984">+51 949 307 984</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side_contact_text">
                                            <h6><i class="fa icon-charity"></i>Horarios </h6>
                                            <a href="#">Lunes a Sábado</a>
                                            <a href="#">08 am - 08 pm</a>
                                        </div>
                                    </li>
                                    <!--<li>
                                        <div class="side_contact_text">
                                            <h6><i class="fa fa-phone"></i>Address</h6>
                                            <p>Health and Care Services Peter Crill House Gloucester Street Jersey JE1
                                                3QS</p>
                                        </div>
                                    </li>-->
                                </ul>
                            </div>
                            <!-- SIDE CONTACT INFO END-->

                            <!-- SIDE CONTACT INFO START-->
                            <div class="side_contact_info">
                                <h4 class="sidebar_title">Oficinas</h4>
                                <ul class="side_news_list">
                                    <li>
                                        <div class="side_news_text">
                                            <span>Trujillo</span>
                                            <p> Av. Nicolas de Pierola 872 Urb. Primavera, <br>4to y 5to piso, área de
                                                Terapias Integrales</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side_news_text">
                                            <span>Chimbote</span>
                                            <p> Av. Jose Balta 558, <br>4to piso, área de Medicina Física y
                                                Rehabilitación</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side_news_text">
                                            <span>Huamachuco</span>
                                            <p>Av. 10 de Julio N°104, <br>2do piso, área de Terapia Física</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- SIDE CONTACT INFO END-->
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="city_health2_row ">
                            <!--CITY HEALTH TEXT START-->
                            <div class="city_health2_text ">
                                <!--SECTION HEADING START-->
                                <div class="section_heading">
                                    <span>FisioHogar</span>
                                    <h3> Terapia Ocupacional </h3>
                                </div>
                                <!--SECTION HEADING END-->
                                <p>Dentro de la terapia ocupacional tratamos muy diversas patologías como: </p>
                                <ul class="city_local_link">
                                    <li><a href="#">Esclerosis múltiple</a></li>
                                    <li><a href="#">Parkinson</a></li>
                                    <li><a href="#">Alzheimer</a></li>
                                    <li><a href="#">Retraso mental</a></li>
                                    <li><a href="#">Epilepsia</a></li>
                                    <li><a href="#">Neuropatías</a></li>
                                    <li><a href="#">Paralisis cerebral</a></li>
                                    <li><a href="#">Sindrome de down</a></li>
                                </ul>
                                <p>Los objetivos del tratamiento son: ayudar al paciente a mejorar su autonomía en las
                                    tareas de la vida diaria, asistir y apoyar su desarrollo hacia una vida
                                    independiente, satisfecha y productiva.
                                </p>
                                <div class="city_health2_service">
                                </div>
                            </div>
                            <!--CITY HEALTH TEXT END-->
                            <!--CITY HEALTH TEXT START-->
                            <div class="city_health2_text text2">
                                <!--SECTION HEADING START-->
                                <div class="section_heading">
                                    <span>FisioHogar</span>
                                    <h3>Sedes</h3>
                                </div>
                                <!--SECTION HEADING END-->
                                <p>Terapia Ocupacional se encuentra en las siguientes sedes</p>
                                <ul class="city_local_link">
                                    <li><a href="#">Trujillo</a></li>
                                    <li><a href="#">Chimbote</a></li>
                                    <li><a href="#">Huamachuco</a></li>
                                    <li></li>
                                </ul>
                            </div>
                            <!--CITY HEALTH TEXT END-->
                            <!--CITY HEALTH TEXT START-->
                            
                            <!--CITY HEALTH TEXT END-->



                            <!-- SIDE CONTACT INFO START-->
                            <div class="side_notice_list">
                                <h4 class="sidebar_title">Archivos</h4>
                                <ul class="side_notice_row">
                                    <li>
                                        <div class="side_notice_detail">
                                            <a href="#"><i class="fa icon-pdf"></i></a>
                                            <div class="side_notice_text">
                                                <h6><a href="terapia_ocupacional_pdf" target="_blank">Terapia
                                                        Ocupacional PDF</a></h6>
                                                <span>Tamaño 3 MB</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- SIDE CONTACT INFO START-->


                            <!--CITY HEALTH TEXT START-->
                            <!--<div class="city_health2_text text2 margin0">
                                <div class="section_heading margin30">
                                    <span>Welcome to Local City</span>
                                    <h3> Asked Question</h3>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="accordion">
                                            <div class="accordion-section">
                                                <a class="accordion-section-title" href="#accordion-1">Lorem quis
                                                    bibendum auctor, consequat </a>
                                                <div id="accordion-1" class="accordion-section-content">
                                                    <figure class="box">
                                                        <div class="box-layer layer-1"></div>
                                                        <div class="box-layer layer-2"></div>
                                                        <div class="box-layer layer-3"></div>
                                                        <img src="extra-images/accordian-fig.jpg" alt="">
                                                    </figure>
                                                    <div class="acoordian_text">
                                                        <p>Mauris interdum fringilla fringilla augue vitae augue vitae
                                                            tincidunt. Curabitur vitae tortor id eros euismod ultrices.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-section">
                                                <a class="accordion-section-title" href="#accordion-2">Lorem quis
                                                    bibendum auctor, consequat </a>
                                                <div id="accordion-2" class="accordion-section-content">
                                                    <figure class="box">
                                                        <div class="box-layer layer-1"></div>
                                                        <div class="box-layer layer-2"></div>
                                                        <div class="box-layer layer-3"></div>
                                                        <img src="extra-images/accordian-fig.jpg" alt="">
                                                    </figure>
                                                    <div class="acoordian_text">
                                                        <p>Mauris interdum fringilla augue vitae fringilla augue vitae
                                                            tincidunt. Curabitur vitae tortor id eros euismod ultrices.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-section">
                                                <a class="accordion-section-title" href="#accordion-3">Lorem quis
                                                    bibendum auctor, consequat </a>
                                                <div id="accordion-3" class="accordion-section-content">
                                                    <figure class="box">
                                                        <div class="box-layer layer-1"></div>
                                                        <div class="box-layer layer-2"></div>
                                                        <div class="box-layer layer-3"></div>
                                                        <img src="extra-images/accordian-fig.jpg" alt="">
                                                    </figure>
                                                    <div class="acoordian_text">
                                                        <p>Mauris interdum fringilla fringilla augue vitae augue vitae
                                                            tincidunt. Curabitur vitae tortor id eros euismod ultrices.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="accordion">
                                            <div class="accordion-section">
                                                <a class="accordion-section-title" href="#accordion-4">Lorem quis
                                                    bibendum auctor, consequat </a>
                                                <div id="accordion-4" class="accordion-section-content">
                                                    <figure class="box">
                                                        <div class="box-layer layer-1"></div>
                                                        <div class="box-layer layer-2"></div>
                                                        <div class="box-layer layer-3"></div>
                                                        <img src="extra-images/accordian-fig.jpg" alt="">
                                                    </figure>
                                                    <div class="acoordian_text">
                                                        <p>Mauris interdum fringilla fringilla augue vitae augue vitae
                                                            tincidunt. Curabitur vitae tortor id eros euismod ultrices.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-section">
                                                <a class="accordion-section-title" href="#accordion-5">Lorem quis
                                                    bibendum auctor, consequat </a>
                                                <div id="accordion-5" class="accordion-section-content">
                                                    <figure class="box">
                                                        <div class="box-layer layer-1"></div>
                                                        <div class="box-layer layer-2"></div>
                                                        <div class="box-layer layer-3"></div>
                                                        <img src="extra-images/accordian-fig.jpg" alt="">
                                                    </figure>
                                                    <div class="acoordian_text">
                                                        <p>Mauris interdum fringilla augue vitae fringilla augue vitae
                                                            tincidunt. Curabitur vitae tortor id eros euismod ultrices.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-section">
                                                <a class="accordion-section-title" href="#accordion-6">Lorem quis
                                                    bibendum auctor, consequat </a>
                                                <div id="accordion-6" class="accordion-section-content">
                                                    <figure class="box">
                                                        <div class="box-layer layer-1"></div>
                                                        <div class="box-layer layer-2"></div>
                                                        <div class="box-layer layer-3"></div>
                                                        <img src="extra-images/accordian-fig.jpg" alt="">
                                                    </figure>
                                                    <div class="acoordian_text">
                                                        <p>Mauris interdum fringilla fringilla augue vitae augue vitae
                                                            tincidunt. Curabitur vitae tortor id eros euismod ultrices.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <!--CITY HEALTH TEXT END-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CITY SERVICES2 WRAP END-->


        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->

    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Image Filterable JavaScript-->
    <script src="js/jquery-filterable.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>
    <!-- Sweetalert2 -->
    <script src="admin/assets/js/sweetalert2.js"></script>
    <!--Consultas JavaScript-->
    <script src="js/consulta.js"></script>
    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <!--Custom JavaScript-->
    <script src="js/visita.js"></script>

    <script>
        document.documentElement.className = 'js';
    </script>
</body>

</html>