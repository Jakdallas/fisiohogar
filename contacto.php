<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/sidebar-widget.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!-- SAB BANNER START-->
        <div class="sab_banner overlay">
            <div class="container">
                <div class="sab_banner_text">
                    <h2>Contacto</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">HOME</a></li>
                        <li class="breadcrumb-item active"><a href="#">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SAB BANNER END-->

        <!-- CITY EVENT2 WRAP START-->
        <div class="city_blog2_wrap team">
            <div class="container">
                <div class="city_contact_row">
                    <div class="city_event_detail contact">
                        <div class="section_heading center">
                            <span>¿Tienes alguna consulta?</span>
                            <h2>Escríbenos</h2>
                        </div>
                        <div class="event_booking_form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="event_booking_field">
                                        <input type="text"id="txtNombreSide" placeholder="Nombre">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="event_booking_field">
                                        <input type="text"id="txtCiudadSide" placeholder="Ciudad de Procedencia">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="event_booking_field">
                                        <input type="text"id="txtDNISide" placeholder="DNI">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="event_booking_field">
                                        <input type="text"id="txtEmailSide" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="event_booking_field">
                                        <input type="text"id="txtTelefonoSide" placeholder="Telefono">
                                    </div>
                                </div>
                                <!--div class="col-md-12">
                                <div class="event_booking_field">
                                    <select class="small">
                                        <option data-display="Please select the service you require ">Please select the
                                            service you require </option>
                                        <option value="1">All Event 1</option>
                                        <option value="2">All Event 2</option>
                                        <option value="4">All Event 3</option>
                                        <option value="4">All Event 4</option>
                                        <option value="4">All Event 5</option>
                                        <option value="4">All Event 6</option>
                                    </select>
                                </div>
                            </div-->
                                <!--div class="col-md-6">
                                <div class="event_booking_field">
                                    <input type="text" placeholder="Subject">
                                </div>
                            </div-->
                                <div class="col-md-12">
                                    <div class="event_booking_area">
                                        <textarea id="txtTextSide">Hola! Quiero reservar una cita</textarea>
                                    </div>
                                    <a class="theam_btn btn2" href="#" id="btnConsultarSide"
                                        data-dismiss="modal">Solicitar
                                        cita</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CITY EVENT2 WRAP END-->


        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->

    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="js/jquery.js"></script>
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Image Filterable JavaScript-->
    <script src="js/jquery-filterable.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>

    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <!--Consultas JavaScript-->
    <script src="js/consulta.js"></script>
    <!--Custom JavaScript-->
    <script src="js/visita.js"></script>
    <script>
    document.documentElement.className = 'js';
    </script>
</body>

</html>