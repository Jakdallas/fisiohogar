<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/sidebar-widget.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!-- SAB BANNER START-->
        <div class="sab_banner overlay">
            <div class="container">
                <div class="sab_banner_text">
                    <h2>Atención a domicilio</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../">Home</a></li>
                        <li class="breadcrumb-item active"><a href="atencion_domicilio">Atención a domicilio</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SAB BANNER END-->

        <!-- CITY SERVICES2 WRAP START-->
        <div class="city_health_department">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="city_about_fig fig2">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/atencion-domicilio.jpg" alt="">
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="city_about_list list2">
                            <!--SECTION HEADING START-->
                            <div class="section_heading border">
                                <span>FisioHogar</span>
                                <h2>Fisioterapia a Domicilio</h2>
                            </div>
                            <!--SECTION HEADING END-->
                            <div class="city_about_text ">
                                <h6>El objetivo común de todos nuestros fisioterapeutas es su completa recuperación,
                                    autonomía personal y mejorar su calidad de vida.</h6>
                                <p>La <b>Fisioterapia a Domicilio</b> permite que determinados tratamientos sean más
                                    efectivos, debido a la personalización
                                    y al entorno familiar del paciente.</p>
                                <p>
                                    También permite aplicar tratamientos fisioterapéuticos para aquellas personas que
                                    sufren dificultades motoras o disponen de poco tiempo para desplazarse en la
                                    clínica. Esta es una buena</p>
                            </div>
                            <div class="section_heading border">
                                <h3>Beneficios</h3>
                            </div>
                            <ul class="city_about_link">
                                <li><a href="#"><i class="fa fa-star"></i>Existe una relación más cercana entre el
                                        fisioterapeuta y el paciente.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Se realiza ejercicios más adaptados a su hogar
                                        y sus necesidades.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Más comodidad, especialmente cuando el
                                        paciente es mayor o son niños.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>La rehabilitación en casa permite una
                                        evolución más rápida.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Tratamientos más personalizados y
                                        flexibles.</a></li>
                            </ul>
                            <a class="theam_btn border-color color" href="https://wa.link/fxm9m8" target="_blank"
                                tabindex="0"> Solicitar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CITY SERVICES2 WRAP END-->

        <div class="city_health2_wrap">
            <div class="container"><br> </div>
        </div>
        <!-- CITY HEALTH2 WRAP START-->
        <div class="city_health2_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="city_health2_service list">
                            <span><i class="fa  icon-health"></i></span>
                            <h5><a href="#">Paso 1</a></h5>
                            <p> Evaluación fisioterapéutica</p>
                            <!--<a href="#" tabindex="-1">See More<i class="fa fa-angle-double-right"></i></a>-->
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="city_health2_service list">
                            <span><i class="fa  icon-doctor"></i></span>
                            <h5><a href="#">Paso 2</a></h5>
                            <p> Diagnóstico terapéutico</p>
                            <!--<<a href="#" tabindex="-1">See More<i class="fa fa-angle-double-right"></i></a>-->
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="city_health2_service list">
                            <span><i class="fa  icon-pie-chart"></i></span>
                            <h5><a href="#">Paso 3</a></h5>
                            <p> Se plantea objetivos a corto, medio y largo plazo</p>
                            <!--<<a href="#" tabindex="-1">See More<i class="fa fa-angle-double-right"></i></a>-->

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="city_health2_service list">
                            <span><i class="fa  icon-heart"></i></span>
                            <h5><a href="#">Paso 4</a></h5>
                            <p> Plan de tratamiento especializado</p>
                            <!--<<a href="#" tabindex="-1">See More<i class="fa fa-angle-double-right"></i></a>-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CITY HEALTH WRAP END-->

        <!-- CITY TREATMENT WRAP START-->
        <div class="city_treatment_wrap">
            <div class="container">
                <ul class="bxslider bx-pager">
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-1.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://youtu.be/ZCDrsBGicOY"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Yuliana Pezantes</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-2.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://www.youtube.com/watch?v=AJ3z_l8EWzU"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Valeria Ricalde</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-3.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://www.youtube.com/watch?v=w6RHdyyJFy0"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Shirley Diaz Plasencia</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-4.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://www.youtube.com/watch?v=127_A3qv_uA"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Marcelina</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-5.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://youtu.be/6Bb9Sn1UTE0"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Domenica</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-6.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://youtu.be/zNPxNlgijTk"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Enma</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-7.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://youtu.be/LqtBsyRNB20"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Noheli Leon Pascual</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-8.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://youtu.be/cQLGay9sufk"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Elisabeth Valladares</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-9.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://youtu.be/4cYLDbFuCaM"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Salomón</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-10.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://youtu.be/WtyoDmDxM2s"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - America Soto Mayor</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-11.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://youtu.be/PB90OJ4gJcs"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - America Soto Mayor</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                </ul>
                <div id="bx-pager" class="city-project-slider">
                    <a data-slide-index="0" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-1.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="1" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-2.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="2" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-3.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="3" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-4.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="4" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-5.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="5" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-6.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="6" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-7.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="7" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-8.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="8" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-9.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="9" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-10.jpg" alt="">
                            </figure>
                    </a>
                    <a data-slide-index="10" class="" style="padding: 0 10px; cursor: pointer;">
                            <figure >
                                <img src="extra-images/pagar-list-11.jpg" alt="">
                            </figure>
                    </a>
                    <!-- <a class="overlay" data-slide-index="0" href=""><img src="extra-images/pagar-list-1.jpg" alt=""></a>
                    <a class="overlay" data-slide-index="1" href=""><img src="extra-images/pagar-list-2.jpg" alt=""></a>
                    <a class="overlay" data-slide-index="2" href=""><img src="extra-images/pagar-list-3.jpg" alt=""></a>
                    <a class="overlay" data-slide-index="3" href=""><img src="extra-images/pagar-list-4.jpg" alt=""></a>
                    <a class="overlay" data-slide-index="4" href=""><img src="extra-images/pagar-list-4.jpg" alt=""></a> -->
                </div>
            </div>
        </div>
        <!-- CITY TREATMENT WRAP END-->

        <!--CITY AWARD WRAP START-->
        <!--<div class="city_award_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="city_award_list">
                            <span><i class="fa  icon-politician"></i></span>
                            <div class="city_award_text">
                                <h3 class="counter">1495</h3>
                                <h3>Established</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="city_award_list">
                            <span><i class="fa icon-cube"></i></span>
                            <div class="city_award_text">
                                <h3 class="counter">75,399</h3>
                                <h3>KM Square</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="city_award_list">
                            <span><i class="fa icon-demographics"></i></span>
                            <div class="city_award_text">
                                <h3 class="counter">1,435,268</h3>
                                <h3>Total Population</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!--CITY AWARD WRAP END-->

        <!--CITY PROJECT WRAP START-->
        <div class="city_project_wrap">
            <div class="container">
                <!--SECTION HEADING START-->
                <div class="section_heading center margin-bottom">
                    <span>FisioHogar</span>
                    <h2>Servicios a domicilio</h2>
                </div>
                <div class="city-project-slider">
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-pediatrica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_pediatrica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_pediatrica">FISIOTERAPIA PEDIÁTRICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-lenguaje.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="terapia_lenguaje">TERAPIAS</a>
                                    <h3><a href="terapia_lenguaje">TERAPIA DE LENGUAJE</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-traumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_traumatologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_traumatologica">FISIOTERAPIA TRAUMATOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-reumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_reumatologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_reumatologica">FISIOTERAPIA REUMATOLÓGICA</a>
                                    </h3>
                                </div>
                            </figure>
                        </div>
                    </div>

                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-neurologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_neurologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_neurologica">FISIOTERAPIA NEUROLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-respiratoria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_respiratoria">TERAPIAS</a>
                                    <h3><a href="fisioterapia_respiratoria">FISIOTERAPIA RESPIRATORIA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-complementaria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="terapia_complementaria">TERAPIAS</a>
                                    <h3><a href="terapia_complementaria">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-ocupacional.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="terapia_ocupacional">TERAPIAS</a>
                                    <h3><a href="terapia_ocupacional">TERAPIA OCUPACIONAL</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-uroginecologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_uroginecologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_uroginecologica">FISIOTERAPIA UROGINECOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--CITY PROJECT WRAP END-->

        <!--CITY SPECIAL2 DREAM START-->
        <div class="city_special2_dream">
            <div class="container">
                <div class="city_special2_text">
                    <h3>FisioHogar</h3>
                    <h2 style="line-height: initial;">Llámanos o escríbenos para mayor información sobre nuestros servicios</h2>
                    <h3>Llámanos al <a href="tel:+51934571849" style="color:white;">+51 934 571 849 </a> o <a
                            href="tel:+51949307984" style="color:white;">+51 949 307 984</a></h3>
                    <a class="theam_btn" href="https://wa.link/fxm9m8" target="_blank" tabindex="0">Escríbenos</a>
                </div>
            </div>
        </div>
        <!--CITY SPECIAL2 DREAM END-->


        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->
    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="js/jquery.js"></script>
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Image Filterable JavaScript-->
    <script src="js/jquery-filterable.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>

    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <!--Custom JavaScript-->
    <script src="js/visita.js"></script>
    <script>
    document.documentElement.className = 'js';
    </script>
</body>

</html>