<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/sidebar-widget.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!-- SAB BANNER START-->
        <div class="sab_banner overlay">
            <div class="container">
                <div class="sab_banner_text">
                    <h2>Servicios</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">HOME</a></li>
                        <li class="breadcrumb-item active"><a href="#">Servicios</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SAB BANNER END-->
        <div class="city_blog2_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="city_project_fig">
                            <figure class="overlay" style="margin: 5px;">
                                <img src="extra-images/service-pediatrica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_pediatrica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_pediatrica">FISIOTERAPIA PEDIÁTRICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="city_project_fig">
                            <figure class="overlay" style="margin: 5px;">
                                <img src="extra-images/service-lenguaje.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="terapia_lenguaje">TERAPIAS</a>
                                    <h3><a href="terapia_lenguaje">TERAPIA DE LENGUAJE</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="city_project_fig">
                            <figure class="overlay" style="margin: 5px;">
                                <img src="extra-images/service-traumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_traumatologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_traumatologica">FISIOTERAPIA TRAUMATOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="city_project_fig">
                            <figure class="overlay" style="margin: 5px;">
                                <img src="extra-images/service-reumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_reumatologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_reumatologica">FISIOTERAPIA REUMATOLÓGICA</a>
                                    </h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="city_project_fig">
                            <figure class="overlay" style="margin: 5px;">
                                <img src="extra-images/service-neurologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_neurologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_neurologica">FISIOTERAPIA NEUROLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="city_project_fig">
                            <figure class="overlay" style="margin: 5px;">
                                <img src="extra-images/service-respiratoria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_respiratoria">TERAPIAS</a>
                                    <h3><a href="fisioterapia_respiratoria">FISIOTERAPIA RESPIRATORIA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div><div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="city_project_fig">
                            <figure class="overlay" style="margin: 5px;">
                                <img src="extra-images/service-complementaria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="terapia_complementaria">TERAPIAS</a>
                                    <h3><a href="terapia_complementaria">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="city_project_fig">
                            <figure class="overlay" style="margin: 5px;">
                                <img src="extra-images/service-ocupacional.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="terapia_ocupacional">TERAPIAS</a>
                                    <h3><a href="terapia_ocupacional">TERAPIA OCUPACIONAL</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="city_project_fig">
                            <figure class="overlay" style="margin: 5px;">
                                <img src="extra-images/service-uroginecologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_uroginecologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_uroginecologica">FISIOTERAPIA UROGINECOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->

    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="js/jquery.js"></script>
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Image Filterable JavaScript-->
    <script src="js/jquery-filterable.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>

    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <!--Custom JavaScript-->
    <script src="js/visita.js"></script>
    <script>
    document.documentElement.className = 'js';
    </script>
</body>

</html>