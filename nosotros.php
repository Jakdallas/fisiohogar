<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/sidebar-widget.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
    <!-- Sweetalert2 -->
    <link href="admin/assets/css/sweetalert2.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!-- SAB BANNER START-->
        <div class="sab_banner overlay">
            <div class="container">
                <div class="sab_banner_text">
                    <h2>FISIOHOGAR</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">HOME</a></li>
                        <li class="breadcrumb-item active"><a href="#">NOSOTROS</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SAB BANNER END-->

        <!-- CITY SERVICES2 WRAP START-->
        <div class="city_health_department">
            <div class="container">
                <div class="city_health2_fig">
                    <figure class="box">
                        <div class="box-layer layer-1"></div>
                        <div class="box-layer layer-2"></div>
                        <div class="box-layer layer-3"></div>
                        <img src="extra-images/banner-nosotros.jpg" alt="">
                    </figure>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="sidebar_widget">
                            <!-- SIDE SUBMIT FORM START-->
                            <div class="side_submit_form">
                                <h4 class="sidebar_title">Escribenos!</h4>
                                <div class="side_submit_field">
                                    <input type="text" id="txtNombreSide" placeholder="Nombre">
                                    <input type="text" id="txtCiudadSide" placeholder="Ciudad">
                                    <input type="text" id="txtDNISide" placeholder="DNI">
                                    <input type="text" id="txtEmailSide" placeholder="Email">
                                    <input type="text" id="txtTelefonoSide" placeholder="Telefono">
                                    <textarea id="txtTextSide" >Hola! Quiero reservar una cita.</textarea>
                                    <button style="width: 100%" class="theam_btn btn2" id="btnConsultarSide">CONSULTAR</button>
                                </div>
                            </div>
                            <!-- SIDE SUBMIT FORM END-->

                            <!-- SIDE CONTACT INFO START-->
                            <div class="side_contact_info">
                                <h4 class="sidebar_title">Contacto</h4>
                                <ul class="side_contact_list">
                                    <li>
                                        <div class="side_contact_text">
                                            <h6><i class="fa fa-tty"></i>Telefono</h6>
                                            <a href="tel:+51934571849"> +51 934 571 849 </a>
                                            <a href="tel:+51949307984"> +51 949 307 984</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side_contact_text">
                                            <h6><i class="fa icon-charity"></i>Horarios </h6>
                                            <a href="#">Lunes a Sábado</a>
                                            <a href="#">08 am - 08 pm</a>
                                        </div>
                                    </li>
                                    <!--<li>
                                        <div class="side_contact_text">
                                            <h6><i class="fa fa-phone"></i>Address</h6>
                                            <p>Health and Care Services Peter Crill House Gloucester Street Jersey JE1
                                                3QS</p>
                                        </div>
                                    </li>-->
                                </ul>
                            </div>
                            <!-- SIDE CONTACT INFO END-->

                            <!-- SIDE CONTACT INFO START-->
                            <div class="side_contact_info">
                                <h4 class="sidebar_title">Oficinas</h4>
                                <ul class="side_news_list">
                                    <li>
                                        <div class="side_news_text">
                                            <span>Trujillo</span>
                                            <p> Av. Nicolas de Pierola 872 Urb. Primavera, <br>4to y 5to piso, área de
                                                Terapias Integrales</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side_news_text">
                                            <span>Chimbote</span>
                                            <p> Av. Jose Balta 558, <br>4to piso, área de Medicina Física y
                                                Rehabilitación</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side_news_text">
                                            <span>Huamachuco</span>
                                            <p>Av. 10 de Julio N°104, <br>2do piso, área de Terapia Física</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- SIDE CONTACT INFO END-->

                            <!-- SIDE CONTACT INFO START-->
                            <!--<div class="side_notice_list">
                                <h4 class="sidebar_title">Public Notice</h4>
                                <ul class="side_notice_row">
                                    <li>
                                        <div class="side_notice_detail">
                                            <a href="#"><i class="fa icon-pdf"></i></a>
                                            <div class="side_notice_text">
                                                <h6><a href="#">Download PDF</a></h6>
                                                <span>SIze 3 MB</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side_notice_detail">
                                            <a href="#"><i class="fa icon-pdf"></i></a>
                                            <div class="side_notice_text">
                                                <h6><a href="#">Download PDF</a></h6>
                                                <span>SIze 3 MB</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side_notice_detail">
                                            <a href="#"><i class="fa icon-pdf"></i></a>
                                            <div class="side_notice_text">
                                                <h6><a href="#">Download PDF</a></h6>
                                                <span>SIze 3 MB</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>-->
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="city_health2_row">
                            <!--CITY HEALTH TEXT START-->
                            <div class="city_health2_text">
                                <!--SECTION HEADING START-->
                                <div class="section_heading">
                                    <span>FisioHogar</span>
                                    <h3> Nuestro Compromiso</h3>
                                </div>
                                <!--SECTION HEADING END-->
                                <p>Somos licenciados especializados y colegiados en Medicina Física y Rehabilitación,
                                    con un alto rango profesional, orientados a la excelencia y comprometidos con el
                                    éxito de nuestros pacientes, ellos son nuestro principal y único objetivo, por eso
                                    queremos ver al paciente como evoluciona ante su dolor, compartimos una parte de su
                                    vida durante su tratamiento, queremos de esta manera brindar un servicio completo y
                                    de altísima calidad a nivel nacional. Nos destacamos por ser una empresa de
                                    fisioterapia que ofrece atención a domicilio con equipos biomédicos portátiles. </p>
                                <div class="row">
                                    <div class="col-md-5 col-sm-6">
                                        <div class="city_health2_service">
                                            <span><i class="fa icon-star"></i></span>
                                            <h5><a href="#">Visión</a></h5>
                                            <p> Ser líderes en el servicio de Medicina Física y Rehabilitación, en la
                                                región Norte del Perú, con sedes en las principales provincias, y la
                                                expansión de las mismas, brindando una atención de calidad y calidez, con
                                                profesionales colegiados y calificados.</p>
                                            <!--a href="#" tabindex="-1">See More<i
                                                    class="fa fa-angle-double-right"></i></a-->
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="city_health2_service">
                                            <span><i class="fa  icon-heart"></i></span>
                                            <h5><a href="#">Misión</a></h5>
                                            <p> Brindar el mejor servicio de Medicina Física y Rehabilitación, de manera
                                                oportuna, eficiente y con calidez, realizando terapias de excelencia
                                                para lograr una óptima recuperación de nuestros pacientes, asegurando un
                                                pronto retorno a sus actividades, en todas nuestras unidades de negocio.
                                            </p>
                                            <!--<a href="#" tabindex="-1">See More<i
                                                    class="fa fa-angle-double-right"></i></a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--CITY HEALTH TEXT END-->
                            <!--CITY HEALTH TEXT START-->
                            <div class="city_health2_text text2">
                                <!--SECTION HEADING START-->
                                <div class="section_heading">
                                    <span>FisioHogar</span>
                                    <h3>Nuestros valores</h3>
                                </div>
                                <!--SECTION HEADING END-->
                                <!--<p>Nuestros valores</p>-->
                                <ul class="city_local_link">
                                    <li><a href="#">Excelencia en la Atención Médica y Fisioterapéutica</a></li>
                                    <li><a href="#">Calidez en la atención al paciente</a></li>
                                    <li><a href="#">Respeto por la persona</a></li>
                                    <li><a href="#">Compromiso con el paciente y su familia</a></li>
                                    <li class="margin0"><a href="#">Manejo integral y multidisciplinario </a>
                                    </li>
                                    <li class="margin0"><a href="#">Integridad, rapidez, profesionalismo y ética </a>
                                    </li>
                                </ul>
                            </div>
                            <!--CITY HEALTH TEXT END-->
                            <!--CITY HEALTH TEXT START-->
                            <div class="city_health2_text text2">
                                <!--SECTION HEADING START-->
                                <div class="section_heading margin30">
                                    <span>FisioHogar</span>
                                    <h3>Nuestros Programas</h3>
                                </div>
                                <!--SECTION HEADING END-->
                                <div class="row program-slider">
                                    <div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-pediatrica.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="fisioterapia_pediatrica" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="fisioterapia_pediatrica" tabindex="-1">FISIOTERAPIA PEDIÁTRICA</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-lenguaje.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="terapia_lenguaje" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="terapia_lenguaje" tabindex="-1">FISIOTERAPIA DE LENGUAJE</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-traumatologica.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="fisioterapia_traumatologica" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="fisioterapia_traumatologica" tabindex="-1">FISIOTERAPIA TRAUMATOLÓGICA</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-reumatologica.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="fisioterapia_reumatologica" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="fisioterapia_reumatologica" tabindex="-1">FISIOTERAPIA REUMATOLÓGICA</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-neurologica.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="fisioterapia_neurologica" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="fisioterapia_neurologica" tabindex="-1">FISIOTERAPIA NEUROLÓGICA</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-complementaria.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="terapia_complementaria" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="terapia_complementaria" tabindex="-1">FISIOTERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-respiratoria.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="fisioterapia_respiratoria" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="fisioterapia_respiratoria" tabindex="-1">FISIOTERAPIA RESPIRATORIA</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-uroginecologica.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="fisioterapia_uroginecologica" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="fisioterapia_uroginecologica" tabindex="-1">FISIOTERAPIA UROGINECOLÓGICA</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-ocupacional.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="terapia_ocupacional" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="terapia_ocupacional" tabindex="-1">FISIOTERAPIA OCUPACIONAL</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-lenguaje.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="terapia_lenguaje" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="terapia_lenguaje" tabindex="-1">FISIOTERAPIA DE LENGUAJE</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-traumatologica.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="fisioterapia_traumatologica" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="fisioterapia_traumatologica" tabindex="-1">FISIOTERAPIA TRAUMATOLÓGICA</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="city_project_fig">
                                                <figure class="overlay">
                                                    <img src="extra-images/service-reumatologica.jpg" alt="">
                                                    <div class="city_project_text">
                                                        <span><i class="fa"></i></span>
                                                        <a href="fisioterapia_reumatologica" tabindex="-1">TERAPIAS</a>
                                                        <h3><a href="fisioterapia_reumatologica" tabindex="-1">FISIOTERAPIA REUMATOLÓGICA</a></h3>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--CITY HEALTH TEXT END-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CITY SERVICES2 WRAP END-->


        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->

    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="js/jquery.js"></script>
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Image Filterable JavaScript-->
    <script src="js/jquery-filterable.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>
    <!-- Sweetalert2 -->
    <script src="admin/assets/js/sweetalert2.js"></script>
    <!--Consultas JavaScript-->
    <script src="js/consulta.js"></script>

    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <!--Custom JavaScript-->
    <script src="js/visita.js"></script>
    <script>
    document.documentElement.className = 'js';
    </script>
</body>

</html>