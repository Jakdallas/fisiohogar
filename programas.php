<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>FisioHogar - Profesionales Especializados</title>
	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<!-- Slick Slider CSS -->
	<link href="css/slick-theme.css" rel="stylesheet" />
	<!-- ICONS CSS -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- ICONS CSS -->
	<link href="css/animation.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/jquery.bxslider.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/style5.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/demo.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/fig-hover.css" rel="stylesheet">
	<!-- Typography CSS -->
	<link href="css/typography.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="style.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="css/component.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="css/shotcode.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="svg-icon.css" rel="stylesheet">
	<!-- Color CSS -->
	<link href="css/color.css" rel="stylesheet">
	<!-- Responsive CSS -->
	<link href="css/responsive.css" rel="stylesheet">
</head>

<body class="demo-5">
	<!--WRAPPER START-->
	<div class="wrapper">
		<!-- HEADER -->
		<?php include('includes/header.php') ?>
		<!-- /HEADER -->

		<!-- SAB BANNER START-->
		<div class="sab_banner overlay">
			<div class="container">
				<div class="sab_banner_text">
					<h2>PROGRAMAS</h2>
					<ul class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Programas</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- SAB BANNER END-->

		<!-- CITY SERVICES2 WRAP START-->
		<div class="city_services2_wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="city_business_fig" style="height: 600px;">
							<figure class="overlay">
								<img src="extra-images/business_fig.jpg" alt="">
								<div class="city_service2_list">
									<span><i class="fa icon-user"></i></span>
									<div class="city_service2_caption">
										<h5>PROGRAMAS INDIVIDUALES</h5>
									</div>
								</div>
							</figure>
							<div class="city_business_list">
								<ul class="city_busine_detail">
									<li><a href="fisioterapia_domicilio" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa de Fisioterapia a Domicilio</a></li>
									<li><a href="masajes_domicilio" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa de Masajes a Domicilio</a></li>
									<li><a href="adulto_mayor" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa del Adulto Mayor</a></li>
									<li><a href="fisioterapia_deportiva" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa de Fisioterapia Deportiva</a></li>
								</ul>
								<!-- <a class="see_more_btn" href="#">Ver mas detalles <i class="fa icon-next-1"></i></a> -->
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="city_business_fig" style="height: 600px;">
							<figure class="overlay">
								<img src="extra-images/business_fig1.jpg" alt="">
								<div class="city_service2_list">
									<span><i class="fa icon-team"></i></span>
									<div class="city_service2_caption">
										<h5>PROGRAMAS EMPRESARIALES</h5>
									</div>
								</div>
							</figure>
							<div class="city_business_list">
								<ul class="city_busine_detail">
									<li><a href="pausas_activas" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa de Pausas Activas</a></li>
									<li><a href="masajes_corporativos" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa de Masajes Corporativos</a></li>
									<li><a href="terapia_ocupacional_det" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa de Terapia Ocupacional</a></li>
									<li><a href="integracion_laboral" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa de Integración Laboral</a></li>
									<li><a href="ergonomia_laboral" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa de Ergonomía Laboral</a></li>
									<li><a href="charlas_preventivas" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Programa de Charlas preventivas</a></li>
									<li><a href="programas_empresariales_pdf" target="_blank" style="display: flex; min-height:50px;"><i class="fa"></i>
													<i class="fa icon-pdf"></i>Programas empresariales PDF
										</a></li>
								</ul>
								<!-- <a class="see_more_btn" href="#">Ver mas detalles <i class="fa icon-next-1"></i></a> -->
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="city_business_fig" style="height: 600px;">
							<figure class="overlay">
								<img src="extra-images/business_fig2.jpg" alt="">
								<div class="city_service2_list">
									<span><i class="fa icon-heart"></i></span>
									<div class="city_service2_caption">
										<h5>SERVICIOS FISIOTERAPÉUTICOS</h5>
									</div>
								</div>
							</figure>
							<div class="city_business_list">
								<ul class="city_busine_detail">
									<li><a href="servicios_fisioterapeuticos" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Medicina Física y Rehabilitación</a></li>
									<li><a href="servicios_fisioterapeuticos" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Fisioterapia Virtual</a></li>
									<li><a href="servicios_fisioterapeuticos" style="display: flex; min-height:50px;"><i class="fa fa-star-o"></i>Evaluaciones Fisioterapéuticas (Consulta)</a></li>
									<!-- <li><a href="#" style="display: flex; min-height:50px;"><i class="fa"></i></a></li> -->
								</ul>
								<!--<a class="see_more_btn" href="#">Ver mas detalles <i class="fa icon-next-1"></i></a>-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- CITY SERVICES2 WRAP END-->
		<!-- FOOTER -->
		<?php include('includes/footer.php') ?>
		<!-- /FOOTER -->
	</div>
	<!--WRAPPER END-->
	<!--Jquery Library-->
	<script src="js/jquery.js"></script>
	<!--Bootstrap core JavaScript-->
	<script src="js/bootstrap.js"></script>
	<!--Slick Slider JavaScript-->
	<script src="js/slick.min.js"></script>
	<!--Pretty Photo JavaScript-->

	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.prettyPhoto.js"></script>

	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
	<!--Image Filterable JavaScript-->
	<script src="js/jquery-filterable.js"></script>
	<!--Pretty Photo JavaScript-->

	<!--Pretty Photo JavaScript-->
	<script src="js/modernizr.custom.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.dlmenu.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/downCount.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/waypoints-sticky.js"></script>
	<!--Counter up JavaScript-->
	<script src="js/waypoints.js"></script>
	<!--Custom JavaScript-->
	<script src="js/custom.js"></script>
	<!--Custom JavaScript-->
	<script src="js/visita.js"></script>
	<script>
		document.documentElement.className = 'js';
	</script>
</body>

</html>