<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/sidebar-widget.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!-- SAB BANNER START-->
        <div class="sab_banner overlay">
            <div class="container">
                <div class="sab_banner_text">
                    <h2>PROGRAMAS EMPRESARIALES</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../">Home</a></li>
                        <li class="breadcrumb-item active"><a href="atencion_domicilio">Programas enpresariales</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- SAB BANNER END-->

        <!-- Programa de Fisioterapia Deportiva-->
        <div class="city_health_department">
            <div class="container">
                <div class="section_heading border">
                    <span>FisioHogar</span>
                    <h2>Masajes Corporativos</h2>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="city_about_fig fig2">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/about-corporativo.jpg" alt="Masajes Corporativos">
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="city_about_list list2">
                            <!--SECTION HEADING START-->
                            <!--SECTION HEADING END-->
                            <div class="city_about_text ">
                                <p>Esta es una técnica que se viene aplicando en pequeñas, medianas y grandes empresas,
                                    incentivando mejorar el clima laboral obteniendo así beneficios tanto en los colaboradores, como en la propia organización.</p>
                                <p>Se logra disminuir dolores y tensiones ocasionados por las largas jornadas laborales.</p>
                            </div>
                            <div class="section_heading border">
                                <h3>Beneficios</h3>
                            </div>
                            <ul class="city_about_link">
                                <li><a href="#"><i class="fa fa-star"></i>Previene lesiones y contracturas.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Aumenta los niveles de energía.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Disminuye el estrés.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Mejora la concentración..</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Brinda una sensación de relajación y bienestar total.</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="city_health2_wrap">
                    <div class="container"><br><br><br></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="city_about_list list2">
                            <!--SECTION HEADING START-->
                            <div class="section_heading border">
                                <!-- <span>FisioHogar</span> -->
                                <h2>Tipos de Masaje</h2>
                            </div>
                            <!--SECTION HEADING END-->
                            <div class="city_about_text ">
                                <ul class="city_about_link">
                                    <li><a href="#"><i class="fa fa-star"></i>MASAJE EN SILLA DEL COLABORADOR.</a></li>
                                    <li>Se realiza en la silla de cada colaborador, así que no hay necesidad de moverse a otro lugar. Por su corta duración se enfoca en el agotamiento mental y el estrés, eliminando contracturas en cuello, espalda, brazos y manos.</li>
                                    <li><a href="#"><i class="fa fa-star"></i>MASAJE EN SILLA ERGONÓMICA.</a></li>
                                    <li>Los colaboradores reciben un masaje integral, beneficiando a zonas como: cabeza, cuello, espalda, hombros, brazos, muñecas y cadera. Ayuda a trabajar los músculos de forma específica.</li>
                                    <li><a href="#"><i class="fa fa-star"></i>MASAJE RELAJANTE EN CAMILLA.</a></li>
                                    <li>Se realiza en una camilla de masajes, donde los colaboradores adoptan una posición horizontal. Este masaje abarca las zonas de cuello, hombros, espalda, brazos, manos, cadera, piernas y pies.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="city_about_list list2">
                            <!--SECTION HEADING START-->
                            <div class="section_heading border">
                                <!-- <span>FisioHogar</span> -->
                                <h2>Resultados</h2>
                            </div>
                            <!--SECTION HEADING END-->
                            <div class="city_about_text ">
                                <ul class="city_about_link">
                                    <li><a href="#"><i class="fa fa-star"></i>Mejora el manejo del estrés.</a></li>
                                    <li><a href="#"><i class="fa fa-star"></i>Incrementa la productividad de los colaboradores.</a></li>
                                    <li><a href="#"><i class="fa fa-star"></i>Aumenta los niveles de energía.</a></li>
                                    <li><a href="#"><i class="fa fa-star"></i>Previene enfermedades y fortalece el sistema inmunológico.</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- CITY SERVICES2 WRAP END-->

        <div class="city_health2_wrap">
            <div class="container"><br> </div>
        </div>
        <!-- CITY TREATMENT WRAP END-->

        <!--CITY AWARD WRAP START-->
        <!--<div class="city_award_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="city_award_list">
                            <span><i class="fa  icon-politician"></i></span>
                            <div class="city_award_text">
                                <h3 class="counter">1495</h3>
                                <h3>Established</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="city_award_list">
                            <span><i class="fa icon-cube"></i></span>
                            <div class="city_award_text">
                                <h3 class="counter">75,399</h3>
                                <h3>KM Square</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="city_award_list">
                            <span><i class="fa icon-demographics"></i></span>
                            <div class="city_award_text">
                                <h3 class="counter">1,435,268</h3>
                                <h3>Total Population</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!--CITY AWARD WRAP END-->

        <!--CITY PROJECT WRAP START-->
        <div class="city_project_wrap">
            <div class="container">
                <!--SECTION HEADING START-->
                <div class="section_heading center margin-bottom">
                    <span>FisioHogar</span>
                    <h2>Servicios a domicilio</h2>
                </div>
                <div class="city-project-slider">
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-pediatrica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA PEDIÁTRICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-lenguaje.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">TERAPIA DE LENGUAJE</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-traumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA TRAUMATOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-reumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA REUMATOLÓGICA</a>
                                    </h3>
                                </div>
                            </figure>
                        </div>
                    </div>

                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-neurologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA NEUROLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-respiratoria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA RESPIRATORIA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-complementaria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-ocupacional.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">TERAPIA OCUPACIONAL</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-uroginecologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="#">TERAPIAS</a>
                                    <h3><a href="#">FISIOTERAPIA UROGINECOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--CITY PROJECT WRAP END-->

        <!--CITY SPECIAL2 DREAM START-->
        <div class="city_special2_dream">
            <div class="container">
                <div class="city_special2_text">
                    <h3>FisioHogar</h3>
                    <h2 style="line-height: initial;">Llámanos o escríbenos para mayor información sobre nuestros servicios</h2>
                    <h3>Llámanos al <a href="tel:+51934571849" style="color:white;">+51 934 571 849 </a> o <a href="tel:+51949307984" style="color:white;">+51 949 307 984</a></h3>
                    <a class="theam_btn" href="https://wa.link/fxm9m8" target="_blank" tabindex="0">Escríbenos</a>
                </div>
            </div>
        </div>
        <!--CITY SPECIAL2 DREAM END-->


        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->
    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="js/jquery.js"></script>
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>

    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Image Filterable JavaScript-->
    <script src="js/jquery-filterable.js"></script>
    <!--Pretty Photo JavaScript-->

    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>

    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <script>
        document.documentElement.className = 'js';
    </script>
</body>

</html>