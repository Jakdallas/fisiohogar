<header>
    <!--CITY TOP WRAP START-->
    <div class="city_top_wrap">
        <div class="container-fluid">
            <div class="city_top_logo navigation" style="width: auto !important; margin-right: 30px;">
                <figure>
                    <h1><a href="#"><img src="images/top-logo.png" alt="FisioHogar"></a></h1>
                </figure>
            </div>
            <div class="city_top_news">
                <span>FisioHogar</span>
                <div class="city-news-slider" style="text-align:center;">
                    <div>
                        <p><i class="fa fa-star"></i> Excelencia en la Atención Médica y Fisioterapéutica <i
                                class="fa fa-star"></i></p>
                    </div>
                    <div>
                        <p><i class="fa fa-star"></i> Manejo integral y multidisciplinario <i class="fa fa-star"></i>
                        </p>
                    </div>
                    <div>
                        <p> Integridad, rapidez, profesionalismo y ética <i class="fa fa-star"></i></p>
                    </div>
                </div>
            </div>
            <div class="city_top_social">
                <ul>
                    <li><a href="https://www.facebook.com/FisioHogarOficial" target="_blank" rel="noreferrer noopener"><i
                                class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/fisiohogarpe" target="_blank"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="https://www.instagram.com/fisiohogarperu/" target="_blank"><i
                                class="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCc1hR2v8mJ13lN6Rls3i1bw" target="_blank"><i
                                class="fa fa-youtube"></i></a></li>
                    <li><a href="https://wa.link/fxm9m8" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                </ul>
            </div>

        </div>
    </div>
    <!--CITY TOP WRAP END-->

    <!--CITY TOP NAVIGATION START-->
    <div class="city_top_navigation">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9">
                    <div class="navigation">
                        <ul>
                            <li><a href="../">HOME</a></li>
                            <li><a href="nosotros">NOSOTROS</a></li>
                            <li><a href="servicios">SERVICIOS</a>
                                <ul class="child">
                                    <li><a href="fisioterapia_traumatologica">FISIOTERAPIA TRAUMATOLÓGICA</a></li>
                                    <li><a href="fisioterapia_reumatologica">FISIOTERAPIA REUMATOLÓGICA</a></li>
                                    <li><a href="fisioterapia_neurologica">FISIOTERAPIA NEUROLÓGICA</a></li>
                                    <li><a href="fisioterapia_respiratoria">FISIOTERAPIA RESPIRATORIA</a></li>
                                    <li><a href="fisioterapia_pediatrica">FISIOTERAPIA PEDIÁTRICA </a></li>
                                    <li><a href="terapia_lenguaje">TERAPIA DE LENGUAJE</a></li>
                                    <li><a href="terapia_complementaria">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></li>
                                    <li><a href="terapia_ocupacional">TERAPIA OCUPACIONAL</a></li>
                                    <li><a href="fisioterapia_uroginecologica">FISIOTERAPIA UROGINECOLÓGICA</a></li>
                                </ul>
                            </li>
                            <li><a href="programas">PROGRAMAS</a>
                                <ul class="child">
                                    <li><a href="programas">INDIVIDUAL</a>
                                        <ul class="child">
                                            <li><a href="fisioterapia_domicilio">Fisioterapia a Domicilio</a></li>
                                            <li><a href="masajes_domicilio">Masajes a Domicilio</a></li>
                                            <li><a href="adulto_mayor">Adulto Mayor</a></li>
                                            <li><a href="fisioterapia_deportiva">Fisioterapia Deportiva</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="programas">EMPRESARIAL</a>
                                        <ul class="child">
                                            <li><a href="pausas_activas">Pausas Activas</a></li>
                                            <li><a href="masajes_corporativos">Masajes Corporativos</a></li>
                                            <li><a href="terapia_ocupacional_det">Terapia Ocupacional</a></li>
                                            <li><a href="integracion_laboral">Integración Laboral</a></li>
                                            <li><a href="ergonomia_laboral">Ergonomía Laboral</a></li>
                                            <li><a href="charlas_preventivas">Charlas preventivas</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="servicios_fisioterapeuticos">FISIOTERAPÉUTICO</a>
                                        <ul class="child">
                                            <li><a href="servicios_fisioterapeuticos">Medicina Física y Rehabilitación</a></li>
                                            <li><a href="servicios_fisioterapeuticos">Fisioterapia Virtual</a></li>
                                            <li><a href="servicios_fisioterapeuticos">Evaluaciones Fisioterapéuticas</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="sedes">SEDES</a>
                                <ul class="child">
                                    <li><a href="trujillo">Trujillo</a></li>
                                    <li><a href="chimbote">Chimbote</a></li>
                                    <li><a href="huamachuco">Huamachuco</a></li>
                                </ul>
                            </li>
                            <li><a href="contacto">CONTACTO</a></li>
                            <li><a href="atencion_domicilio">ATENCIÓN A DOMICILIO</a></li>
                        </ul>
                    </div>
                    <div class=" dl-menuwrapper city_top_logo" style="width: 40%;">
                        <figure>
                            <h1><a href="#"><img src="images/widget-logo.png" alt="FisioHogar"></a></h1>
                        </figure>
                    </div>
                    <!--DL Menu Start-->
                    <div id="kode-responsive-navigation" class="dl-menuwrapper">
                        <button class="dl-trigger" style="float: inherit;">Open Menu</button>
                        <ul class="dl-menu">
                            <li><a class="active" href="../">HOME</a></li>
                            <li><a href="nosotros">NOSOTROS</a></li>
                            <li class="menu-item kode-parent-menu"><a href="servicios">SERVICIOS</a>
                                <ul class="dl-submenu">
                                    <li><a href="fisioterapia_traumatologica">FISIOTERAPIA TRAUMATOLÓGICA</a></li>
                                    <li><a href="fisioterapia_reumatologica">FISIOTERAPIA REUMATOLÓGICA</a></li>
                                    <li><a href="fisioterapia_neurologica">FISIOTERAPIA NEUROLÓGICA</a></li>
                                    <li><a href="fisioterapia_respiratoria">FISIOTERAPIA RESPIRATORIA</a></li>
                                    <li><a href="fisioterapia_pediatrica">FISIOTERAPIA PEDIÁTRICA </a></li>
                                    <li><a href="terapia_lenguaje">TERAPIA DE LENGUAJE</a></li>
                                    <li><a href="terapia_complementaria">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></li>
                                    <li><a href="terapia_ocupacional">TERAPIA OCUPACIONAL</a></li>
                                    <li><a href="fisioterapia_uroginecologica">FISIOTERAPIA UROGINECOLÓGICA</a></li>
                                </ul>
                            </li>
                            <li class="menu-item kode-parent-menu"><a href="programas">PROGRAMAS</a>
                                <ul class="dl-submenu">
                                    <li><a href="programas">INDIVIDUAL</a>
                                        <ul class="dl-submenu">
                                            <li><a href="fisioterapia_domicilio">Fisioterapia a Domicilio</a></li>
                                            <li><a href="masajes_domicilio">Masajes a Domicilio</a></li>
                                            <li><a href="adulto_mayor">Adulto Mayor</a></li>
                                            <li><a href="fisioterapia_deportiva">Fisioterapia Deportiva</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="programas">EMPRESARIAL</a>
                                        <ul class="dl-submenu">
                                            <li><a href="pausas_activas">Pausas Activas</a></li>
                                            <li><a href="masajes_corporativos">Masajes Corporativos</a></li>
                                            <li><a href="terapia_ocupacional_det">Terapia Ocupacional</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="servicios_fisioterapeuticos">FISIOTERAPÉUTICO</a>
                                        <ul class="dl-submenu">
                                            <li><a href="servicios_fisioterapeuticos">Medicina Física y Rehabilitación</a></li>
                                            <li><a href="servicios_fisioterapeuticos">Fisioterapia Virtual</a></li>
                                            <li><a href="servicios_fisioterapeuticos">Evaluaciones Fisioterapéuticas</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="sedes">SEDES</a>
                                <ul class="dl-submenu">
                                    <li><a href="trujillo">Trujillo</a></li>
                                    <li><a href="chimbote">Chimbote</a></li>
                                    <li><a href="huamachuco">Huamachuco</a></li>
                                </ul>
                            </li>
                            <li><a href="contacto">CONTACTO</a></li>
                            <li><a href="atencion_domicilio">ATENCIÓN A DOMICILIO</a></li>
                        </ul>
                    </div>
                    <!--DL Menu END-->
                </div>
                <div class="col-md-3">
                    <div class="city_top_form">
                        <a class="top_user navigation" href="https://wa.link/fxm9m8" target="_blank"
                            style="font-size: 20px;"><i class="fa fa-whatsapp"></i> Escríbenos</a>
                        <div class="city_top_search">

                            <!--<input type="text" placeholder="Search">
                            <a href="#"><i class="fa fa-search"></i></a>-->
                        </div>
                        <!--<<a class="top_user" href="login.html"><i class="fa fa-user"></i></a>>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--CITY TOP NAVIGATION END-->

    <!--CITY TOP NAVIGATION START-->
    <div class="city_top_navigation hide">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9">
                    <div class="navigation">
                        <ul>
                            <li><a href="../">HOME</a></li>
                            <li><a href="nosotros">NOSOTROS</a></li>
                            <li><a href="servicios">SERVICIOS</a>
                                <ul class="child">
                                    <li><a href="fisioterapia_traumatologica">FISIOTERAPIA TRAUMATOLÓGICA</a></li>
                                    <li><a href="fisioterapia_reumatologica">FISIOTERAPIA REUMATOLÓGICA</a></li>
                                    <li><a href="fisioterapia_neurologica">FISIOTERAPIA NEUROLÓGICA</a></li>
                                    <li><a href="fisioterapia_respiratoria">FISIOTERAPIA RESPIRATORIA</a></li>
                                    <li><a href="fisioterapia_pediatrica">FISIOTERAPIA PEDIÁTRICA </a></li>
                                    <li><a href="terapia_lenguaje">TERAPIA DE LENGUAJE</a></li>
                                    <li><a href="terapia_complementaria">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></li>
                                    <li><a href="terapia_ocupacional">TERAPIA OCUPACIONAL</a></li>
                                    <li><a href="fisioterapia_uroginecologica">FISIOTERAPIA UROGINECOLÓGICA</a></li>
                                </ul>
                            </li>
                            <li><a href="programas">PROGRAMAS</a>
                                <ul class="child">
                                    <li><a href="programas">INDIVIDUAL</a>
                                        <ul class="child">
                                            <li><a href="fisioterapia_domicilio">Fisioterapia a Domicilio</a></li>
                                            <li><a href="masajes_domicilio">Masajes a Domicilio</a></li>
                                            <li><a href="adulto_mayor">Adulto Mayor</a></li>
                                            <li><a href="fisioterapia_deportiva">Fisioterapia Deportiva</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="programas">EMPRESARIAL</a>
                                        <ul class="child">
                                            <li><a href="pausas_activas">Pausas Activas</a></li>
                                            <li><a href="masajes_corporativos">Masajes Corporativos</a></li>
                                            <li><a href="terapia_ocupacional_det">Terapia Ocupacional</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="servicios_fisioterapeuticos">FISIOTERAPÉUTICO</a>
                                        <ul class="child">
                                            <li><a href="servicios_fisioterapeuticos">Medicina Física y Rehabilitación</a></li>
                                            <li><a href="servicios_fisioterapeuticos">Fisioterapia Virtual</a></li>
                                            <li><a href="servicios_fisioterapeuticos">Evaluaciones Fisioterapéuticas</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="sedes">SEDES</a>
                                <ul class="child">
                                    <li><a href="trujillo">Trujillo</a></li>
                                    <li><a href="chimbote">Chimbote</a></li>
                                    <li><a href="huamachuco">Huamachuco</a></li>
                                </ul>
                            </li>
                            <li><a href="contacto">CONTACTO</a></li>
                            <li><a href="atencion_domicilio">ATENCIÓN A DOMICILIO</a></li>
                        </ul>
                    </div>
                    <div class=" dl-menuwrapper city_top_logo" style="width: 40%;">
                        <figure>
                            <h1><a href="#"><img src="images/widget-logo.png" alt="FisioHogar"></a></h1>
                        </figure>
                    </div>
                    <!--DL Menu Start-->
                    <div id="kode-responsive-navigation1" class="dl-menuwrapper">
                        <button class="dl-trigger">Open Menu</button>
                        <ul class="dl-menu">
                            <li><a class="active" href="../">Home</a></li>
                            <li class="menu-item kode-parent-menu"><a href="servicios">SERVICIOS</a>
                                <ul class="dl-submenu">
                                    <li><a href="fisioterapia_traumatologica">FISIOTERAPIA TRAUMATOLÓGICA</a></li>
                                    <li><a href="fisioterapia_reumatologica">FISIOTERAPIA REUMATOLÓGICA</a></li>
                                    <li><a href="fisioterapia_neurologica">FISIOTERAPIA NEUROLÓGICA</a></li>
                                    <li><a href="fisioterapia_respiratoria">FISIOTERAPIA RESPIRATORIA</a></li>
                                    <li><a href="fisioterapia_pediatrica">FISIOTERAPIA PEDIÁTRICA </a></li>
                                    <li><a href="terapia_lenguaje">TERAPIA DE LENGUAJE</a></li>
                                    <li><a href="terapia_complementaria">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></li>
                                    <li><a href="terapia_ocupacional">TERAPIA OCUPACIONAL</a></li>
                                    <li><a href="fisioterapia_uroginecologica">FISIOTERAPIA UROGINECOLÓGICA</a></li>
                                </ul>
                            </li>
                            <li><a href="nosotros">NOSOTROS</a></li>
                            <li class="menu-item kode-parent-menu"><a href="programas">PROGRAMAS</a>
                                <ul class="dl-submenu">
                                    <li><a href="programas">INDIVIDUAL</a>
                                        <ul class="dl-submenu">
                                            <li><a href="fisioterapia_domicilio">Fisioterapia a Domicilio</a></li>
                                            <li><a href="masajes_domicilio">Masajes a Domicilio</a></li>
                                            <li><a href="adulto_mayor">Adulto Mayor</a></li>
                                            <li><a href="fisioterapia_deportiva">Fisioterapia Deportiva</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="programas">EMPRESARIAL</a>
                                        <ul class="dl-submenu">
                                            <li><a href="pausas_activas">Pausas Activas</a></li>
                                            <li><a href="masajes_corporativos">Masajes Corporativos</a></li>
                                            <li><a href="terapia_ocupacional_det">Terapia Ocupacional</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="servicios_fisioterapeuticos">FISIOTERAPÉUTICO</a>
                                        <ul class="dl-submenu">
                                            <li><a href="servicios_fisioterapeuticos">Medicina Física y Rehabilitación</a></li>
                                            <li><a href="servicios_fisioterapeuticos">Fisioterapia Virtual</a></li>
                                            <li><a href="servicios_fisioterapeuticos">Evaluaciones Fisioterapéuticas</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="sedes">SEDES</a>
                                <ul class="dl-submenu">
                                    <li><a href="trujillo">Trujillo</a></li>
                                    <li><a href="chimbote">Chimbote</a></li>
                                    <li><a href="huamachuco">Huamachuco</a></li>
                                </ul>
                            </li>
                            <li><a href="contacto">CONTACTO</a></li>
                            <li><a href="atencion_domicilio">ATENCIÓN A DOMICILIO</a></li>
                        </ul>
                    </div>
                    <!--DL Menu END-->
                </div>
                <!--<div class="col-md-3">
                    <div class="city_top_form">
                        <div class="city_top_search">
                            <input type="text" placeholder="Search">
                            <a href="#"><i class="fa fa-search"></i></a>
                        </div>
                        <a class="top_user" href="login.html"><i class="fa fa-user"></i></a>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
    <!--CITY TOP NAVIGATION END-->
</header>