<!--CITY REQUEST WRAP START-->
<div class="city_requset_wrap" style="margin-top: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="city_request_list">
                    <div class="city_request_row">
                        <span><i class="fa icon-question"></i></span>
                        <div class="city_request_text">
                            <span>Nuevo</span>
                            <h4>Servicios</h4>
                        </div>
                    </div>
                    <div class="city_request_link">
                        <ul>
                            <li><a href="fisioterapia_traumatologica">FISIOTERAPIA TRAUMATOLÓGICA</a></li>
                            <li><a href="fisioterapia_reumatologica">FISIOTERAPIA REUMATOLÓGICA</a></li>
                            <li><a href="fisioterapia_neurologica">FISIOTERAPIA NEUROLÓGICA</a></li>
                            <li><a href="fisioterapia_respiratoria">FISIOTERAPIA RESPIRATORIA</a></li>
                            <li><a href="fisioterapia_pediatrica">FISIOTERAPIA PEDIÁTRICA </a></li>
                            <li><a href="terapia_lenguaje">TERAPIA DE LENGUAJE</a></li>
                            <li><a href="terapia_complementaria">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></li>
                            <li><a href="terapia_ocupacional">TERAPIA OCUPACIONAL</a></li>
                            <li><a href="fisioterapia_uroginecologica">FISIOTERAPIA UROGINECOLÓGICA</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="city_request_list">
                    <div class="city_request_row">
                        <span><i class="fa icon-shout"></i></span>
                        <div class="city_request_text">
                            <span>Nuevo</span>
                            <h4><a href="blog">Blog</a> </h4>
                        </div>
                    </div>
                    <div class="city_request_link">
                        <ul>
                            <li><a href="blog_contabilidad" target="_blank">CONOCE NUESTRA AREA DE CONTABILIDAD Y FINANZAS</a></li>
                            <li><a href="blog_rehabilitacion" target="_blank">REHABILITACIÓN EN PACIENTES POST COVID-19</a></li>
                            <!--li><a href="#">Building Violation</a></li>
                            <li><a href="#">Affordable Housing</a></li>
                            <li><a href="#">Graffiti Removal</a></li>
                            <li><a href="#">Civil Service Exams</a></li>
                            <li><a href="#">Rodent Baiting</a></li>
                            <li class="margin0"><a href="#">Cleaning</a></li>
                            <li class="margin0"><a href="#">Uncleared Sidewalk</a></li-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--CITY REQUEST WRAP END-->
<footer>
    <div class="widget_wrap overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="widget_list">
                        <h4 class="widget_title">Encuéntranos</h4>
                        <div class="widget_text">
                            <ul>

                                <!--li>
                                    <a class="side_news_text">
                                        <span>Trujillo</span>
                                        <p> Av. Nicolas de Pierola 872 Urb. Primavera, 4to y 5to piso, área de Terapias
                                            Integrales</p>
                                    </a>
                                </li>

                                <li>
                                    <a class="side_news_text">
                                        <span>Chimbote</span>
                                        <p> Av. Jose Balta 558, <br>4to piso, área de Medicina Física y Rehabilitación
                                        </p>
                                    </a>
                                </li>

                                <li>
                                    <a class="side_news_text">
                                        <span>Huamachuco</span>
                                        <p>Av. 10 de Julio N°104, <br>2do piso, área de Terapia Física</p>
                                    </a>
                                </li-->
                                <li>
                                    <p> Telefonos</p>
                                </li>
                                <li><a href="tel:+51934571849">+51 934 571 849 </a></li>
                                <li><a href="tel:+51949307984">+51 949 307 984</a></li>
                                <li><a href="#">Lunes a Sábado</a></li>
                                <li><a href="#">08 am - 08 pm</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="widget_list">
                                <h4 class="widget_title">Servicios</h4>
                                <div class="widget_service">
                                    <ul>
                                        <li><a href="fisioterapia_traumatologica">FISIOTERAPIA TRAUMATOLÓGICA</a></li>
                                        <li><a href="fisioterapia_reumatologica">FISIOTERAPIA REUMATOLÓGICA</a></li>
                                        <li><a href="fisioterapia_neurologica">FISIOTERAPIA NEUROLÓGICA</a></li>
                                        <li><a href="fisioterapia_respiratoria">FISIOTERAPIA RESPIRATORIA</a></li>
                                        <li><a href="fisioterapia_pediatrica.php">FISIOTERAPIA PEDIÁTRICA </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="widget_list">
                                <h4 class="widget_title">Servicios </h4>
                                <div class="widget_service">
                                    <ul>
                                        <li><a href="terapia_lenguaje">TERAPIA DE LENGUAJE</a></li>
                                        <li><a href="terapia_complementaria">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a>
                                        </li>
                                        <li><a href="terapia_ocupacional">TERAPIA OCUPACIONAL</a></li>
                                        <li><a href="fisioterapia_uroginecologica">FISIOTERAPIA UROGINECOLÓGICA</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="widget_list">
                                <h4 class="widget_title">Programas</h4>
                                <div class="widget_service">
                                    <ul>
                                        <li><a href="programas">INDIVIDUAL</a>
                                        <li><a href="programas">EMPRESARIAL</a>
                                        <li><a href="programas">FISIOTERAPÉUTICO</a>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget_list">
                        <h4 class="widget_title">Horarios</h4>
                        <div class="widget_text text2">
                            <ul>
                                <li><a href="#">Lunes<span>08 am - 08 pm</span></a></li>
                                <li><a href="#">Martes <span>08 am - 08 pm</span></a></li>
                                <li><a href="#">Miercoles<span>08 am - 08 pm</span></a></li>
                                <li><a href="#">Jueves<span>08 am - 08 pm</span></a></li>
                                <li><a href="#">Viernes <span>08 am - 08 pm</span></a></li>
                                <li><a href="#">Sábado<span>08 am - 08 pm</span></a></li>
                                <li><a href="#">Domingo<span>Cerrado</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="widget_copyright">
                    <div class="col-md-3 col-sm-6">
                        <div class="widget_logo">
                            <a href="#"><img src="images/widget-logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="copyright_text">
                            <p><span>Copyright © 2021 - 22 FisioHogar.</span> Designed and
                                Developed by SERTEC</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="city_top_social">
                            <ul>
                                <li><a href="https://www.facebook.com/FisioHogarOficial" target="_blank"><i
                                            class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/fisiohogarpe" target="_blank"><i
                                            class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/fisiohogarperu/" target="_blank"><i
                                            class="fa fa-instagram"></i></a></li>
                                <li><a href="https://www.youtube.com/channel/UCc1hR2v8mJ13lN6Rls3i1bw"
                                        target="_blank"><i class="fa fa-youtube"></i></a></li>
                                <li><a href="https://wa.link/fxm9m8" target="_blank"><i class="fa fa-whatsapp"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>