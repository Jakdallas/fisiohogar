function lockFields() {
    //$("#btnUnlock").removeAttr("disabled");
    $('#txtNombreSide').attr("disabled", "disabled");
    $('#txtCiudadSide').attr("disabled", "disabled");
    $('#txtDNISide').attr("disabled", "disabled");
    $('#txtEmailSide').attr("disabled", "disabled");
    $('#txtTelefonoSide').attr("disabled", "disabled");
    $('#txtTextSide').attr("disabled", "disabled");
    $('#btnConsultarSide').attr("disabled", "disabled");
    $('#txtNombreModal').attr("disabled", "disabled");
    $('#txtCiudadModal').attr("disabled", "disabled");
    $('#txtDNIModal').attr("disabled", "disabled");
    $('#txtEmailModal').attr("disabled", "disabled");
    $('#txtTelefonoModal').attr("disabled", "disabled");
    $('#txtTextModal').attr("disabled", "disabled");
    $('#btnConsultarModal').attr("disabled", "disabled");
}

function unlockFields() {
    //$('#btnUnlock').attr("disabled", "disabled");
    $("#txtNombreSide").removeAttr("disabled");
    $("#txtCiudadSide").removeAttr("disabled");
    $("#txtDNISide").removeAttr("disabled");
    $("#txtEmailSide").removeAttr("disabled");
    $("#txtTelefonoSide").removeAttr("disabled");
    $("#txtTextSide").removeAttr("disabled");
    $("#btnConsultarSide").removeAttr("disabled");
    $("#txtNombreModal").removeAttr("disabled");
    $("#txtCiudadModal").removeAttr("disabled");
    $("#txtDNIModal").removeAttr("disabled");
    $("#txtEmailModal").removeAttr("disabled");
    $("#txtTelefonoModal").removeAttr("disabled");
    $("#txtTextModal").removeAttr("disabled");
    $("#btnConsultarModal").removeAttr("disabled");
}

function alCargarDocumento() {
    $("#btnConsultarSide").click(function() {

        nombre = $("#txtNombreSide").val();
        ciudad = $("#txtCiudadSide").val();
        dni = $("#txtDNISide").val();
        email = $("#txtEmailSide").val();
        telefono = $("#txtTelefonoSide").val();
        texto = $("#txtTextSide").val();

        if (nombre != "") {
            var formData = new FormData();
            formData.append("Nombre", nombre);
            formData.append("Ciudad", ciudad);
            formData.append("DNI", dni);
            formData.append("Email", email);
            formData.append("Telefono", telefono);
            formData.append("Text", texto);
            formData.append("opcion", 'insertSolCita');
            $.ajax({
                type: 'POST',
                data: formData,
                url: 'admin/controlador/solicitud/cSolicitud.php',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    lockFields();
                },
                success: function(data) {
                    var obj = JSON.parse(data);
                    console.log(obj);
                    unlockFields();
                    if (obj == 1) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Solicitud Exitosa',
                            text: '¡Nos pondremos en contacto pronto!'
                        });
                        sonido.play();
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'No se pudo registrar la cita'
                        });
                    }
                },
                error: function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Error al registrar cita'
                    });
                    //listarConfig();
                    unlockFields();
                }
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Campos incompletos',
                text: 'Ingrese un nombre por favor.'
            });
        }
    });

    $("#btnConsultarModal").click(function() {

        nombre = $("#txtNombreModal").val();
        ciudad = $("#txtCiudadModal").val();
        dni = $("#txtDNIModal").val();
        email = $("#txtEmailModal").val();
        telefono = $("#txtTelefonoModal").val();
        texto = $("#txtTextModal").val();

        if (nombre != "") {
            var formData = new FormData();
            formData.append("Nombre", nombre);
            formData.append("Ciudad", ciudad);
            formData.append("DNI", dni);
            formData.append("Email", email);
            formData.append("Telefono", telefono);
            formData.append("Text", texto);
            formData.append("opcion", 'insertSolCita');
            $.ajax({
                type: 'POST',
                data: formData,
                url: 'admin/controlador/solicitud/cSolicitud.php',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    lockFields();
                },
                success: function(data) {
                    var obj = JSON.parse(data);
                    console.log(obj);
                    unlockFields();
                    if (obj == 1) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Solicitud Exitosa',
                            text: '¡Nos pondremos en contacto pronto!'
                        });
                        sonido.play();
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'No se pudo registrar la cita'
                        });
                    }
                },
                error: function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Error al registrar cita'
                    });
                    //listarConfig();
                    unlockFields();
                }
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Campos incompletos',
                text: 'Ingrese un nombre por favor.'
            });
        }
    });
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);