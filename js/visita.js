function alCargarDocumento() {
    const path = 'https://api.bigdatacloud.net/data/ip-geolocation-full?key=';
    const token = 'f9b2b8722e1147649ab4424cf692954c';
    console.log(path);
    console.log(token);
    $.ajax({
        type: 'GET',
        url: path + token,
        success: function(response) {
            let ipaddress = response.ip;
            let country = response.country.name;
            let department = response.location.isoPrincipalSubdivision.replace('department', '');
            let locality = response.location.localityName.replace('district', '');

            const data = new FormData()
            data.append('opcion', 'insertVisita');
            data.append('IP', ipaddress);
            data.append('Pais', country);
            data.append('Zona', department);
            data.append('Localidad', locality);

            $.ajax({
                type: 'POST',
                data: data,
                url: 'admin/controlador/configuration/cConfiguration.php',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                    var obj = JSON.parse(data);
                    console.log(obj);
                },
                error: function(data) {}
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);