function listarPasos() {

    var param_opcion = 'listStep';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: 'controlador/configuration/cConfiguration.php',
        success: function(data) {
            var steps = document.getElementById('steps');
            var obj = JSON.parse(data);
            var cont = 0;
            var step = "";
            console.log(obj);
            obj.forEach(function(e) {
                step = "";
                step = step + '<div class="col-lg-3 col-md-6 col-sm-6"><div class="services__item"><img src="';
                step = step + e.url;
                step = step + '" alt=""><h4>';
                step = step + e.titulo;
                step = step + '</h4><p>';
                step = step + e.descripcion;
                step = step + '</p></div></div>';
                steps.innerHTML += step;
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function listarConfiguracion() {

    var param_opcion = 'listConfiguration';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: 'controlador/configuration/cConfiguration.php',
        success: function(data) {
            var razonSocial = document.getElementById('Empresa');
            var obj = JSON.parse(data);
            console.log(obj);
            razonSocial.innerHTML = obj[0].razon;

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function listarConfig() {

    var param_opcion = 'listConfig';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/configuration/cConfiguration.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            //$('#txtUsuario').val(obj[0].admin);
            //$('#txtNombre').val(obj[0].nombre);
            //$('#txtTelefono').val(obj[0].telefono);
            //$('#txtCorreo').innerHTML(obj[0].email);
            var Empresa = document.getElementById('Empresa');
            Empresa.innerHTML = obj[0].empresa;
            var header = document.getElementById('title-header');
            header.innerHTML = obj[0].empresa;
            //$('#Empresa').innerHTML(obj[0].empresa);
            //$('#title-header').innerHTML(obj[0].empresa);
            $("#logo-loading").attr("src", obj[0].logo);
            $("#logo-header").attr("src", obj[0].logo);
            $("#logo-footer").attr("src", obj[0].logo);
            $("#imgVideo").attr("src", obj[0].url);
            // $('#txtLogo').val("disabled", "disabled");
            //$('#txtVideo').val("disabled", "disabled");

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function listClienteSession() {
    var param_opcion = 'listClienteBySession';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            var container = document.getElementById('containerSession');
            var clienteSession = "";
            if (obj != undefined && obj != null) {
                clienteSession += '<h4>' + obj[0].nombres + '</h4>';
            } else {
                clienteSession += '<input id="hdUsuario" type="hidden" value="">';
                clienteSession += '<a href="./editor.php" class="primary-btn">Probar Editor!</a>';
                clienteSession += '<h4 id="lblUsuario" class="hide"></h4>';
            }
            container.innerHTML = clienteSession;


        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });
}


function alCargarDocumento() {
    listarPasos();
    //listarConfiguracion();
    listClienteSession();
    listarConfig();
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);