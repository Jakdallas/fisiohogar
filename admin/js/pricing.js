olPricing = [];
olBeneficio = [];
var bandRegistro = 0;
var bandLogin = 0;
var idPrecio;
var gv_precio;
var gv_puntos;
var gv_plan;

const publicKey = "pk_live_bfc8e9101d0a2790";
const privateKey = "sk_live_b54c47c3a632be40";
const urlSecure = "https://secure.culqi.com/v2/tokens";
const urlApi = "https://api.culqi.com/v2/charges";

function listarPricing() {

    var param_opcion = 'listBeneficios';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/beneficios/cBeneficios.php',
        success: function(data) {
            olBeneficio = JSON.parse(data);
            console.log(olBeneficio);

            param_opcion = 'listPricing';
            $.ajax({
                type: 'POST',
                data: 'opcion=' + param_opcion,
                url: '../../controlador/tarifas/cTarifas.php',
                success: function(data) {

                    olPricing = JSON.parse(data);
                    console.log(olPricing);

                    param_opcion = 'listBeneficiosByPricing';
                    $.ajax({
                        type: 'POST',
                        data: 'opcion=' + param_opcion,
                        url: '../../controlador/beneficios/cBeneficios.php',
                        success: function(data) {
                            var olCont = JSON.parse(data);
                            console.log(olCont);
                            olCont.sort(function(a, b) {
                                if (a.cont > b.cont) {
                                    return 1;
                                }
                                if (a.cont < b.cont) {
                                    return -1;
                                }
                                // a must be equal to b
                                return 0;
                            });

                            var max = olCont.pop().cont;
                            console.log(max);
                            var container = document.getElementById('container-cards-price');
                            var pricing = "";

                            olPricing.forEach(function(e) {
                                pricing = pricing + '<div class="col-lg-4"><div class="card mb-5 mb-lg-0"><div class="card-body"><h5 class="card-title text-muted text-uppercase text-center">';
                                pricing = pricing + e.nombre;
                                pricing = pricing + '</h5><h6 class="card-price text-center">S/. ' + e.precio + '<span class="period">/ ' + e.puntos + ' Ptos</span></h6>';
                                pricing = pricing + '<hr><ul class="fa-ul">';
                                olBeneficioFilter = olBeneficio.filter(oBeneficio => oBeneficio.nombre == e.nombre);
                                for (let index = 0; index < max; index++) {

                                    if (olBeneficioFilter[index] != undefined) {
                                        pricing = pricing + '<li><span class="fa-li"><i class="icon_check"></i></span>';
                                        pricing = pricing + olBeneficioFilter[index].beneficio;
                                        pricing = pricing + '</li>';
                                    } else {
                                        pricing = pricing + '<li>';
                                        pricing = pricing + '</br>';
                                        pricing = pricing + '</li>';
                                    }
                                }
                                pricing = pricing + '</ul><a href="#" class="btn btn-block btn-dark text-uppercase" onclick="setDataPrice(' + e.idprecio + "," + "'" + e.nombre + "'" + "," + "'" + e.precio + "'" + "," + "'" + e.puntos + "'" + ')" >COMPRAR</a></div></div></div>';

                            });
                            container.innerHTML = pricing;
                        },
                        error: function(data) {
                            alert('Error al mostrar');
                        }
                    });
                },
                error: function(data) {
                    alert('Error al mostrar');
                }
            });
        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function setDataPrice(idprecio, nombre, precio, puntos) {
    var param_opcion = 'listClienteBySession';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            if (obj[0] != undefined && obj[0] != null) {
                $("#documentoCard").val(obj[0].documento);
                $("#titularCard").val(obj[0].nombres);
                $("#titularCard").keyup();
                $("#buyModal").modal("show");
            } else {
                $("#loginModal").modal("show");
                bandLogin = 1;
                //alert('Error al encontrar usuario');
            }


        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });
    idPrecio = idprecio;
    gv_precio = precio;
    gv_puntos = puntos;
    gv_plan = nombre;
    $("#Monto").val(precio);
    $("#Puntos").val(puntos);
}

function listClienteSession() {
    var param_opcion = 'listClienteBySession';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            var container = document.getElementById('containerSession');
            var clienteSession = "";
            if (obj[0] != undefined && obj[0] != null) {
                clienteSession += '<h4>' + obj[0].nombres + '</h4>';
            } else {
                clienteSession += '<input id="hdUsuario" type="hidden" value="">';
                clienteSession += '<a href="./editor.php" class="primary-btn">Probar Editor!</a>';
                clienteSession += '<h4 id="lblUsuario" class="hide"></h4>';
            }
            container.innerHTML = clienteSession;


        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });
}

function RegistrarUsuario() {
    //console.log("asedadad");
    //$("#buyModal").modal("hide");
    $("#loginModal").modal("hide");
    $("#registerModal").modal("show");

}

function login() {
    var Correo = $("#txtCorreoL").val();
    var Clave = $("#txtClaveL").val();
    var param_opcion = 'initSesion';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&Correo=' + Correo +
            '&Clave=' + Clave,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            if (obj.idcliente != undefined && obj.idcliente != null) {
                $("#buyModal").modal("show");
            } else {
                if (obj == 1) {
                    $("#loginModal").modal("hide");
                } else {
                    $("#loginModal").modal("show");
                }
            }


        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });
}

function registrarVenta() {
    //Code para mostrar loader
    $(".loader").fadeIn();
    $("#preloder").delay(200).fadeIn("slow");
    var card_number = $("#numberCard").val().replace(/ /g, "");
    var cvv = $("#ccvCard").val().trim();
    var email = $("#txtCorreoL").val();
    var expiration_month = $("#dateCard").val().split("/")[0].trim();
    var expiration_year = $("#dateCard").val().split("/")[1].trim();
    var amount = $("#Monto").val();
    var description = gv_plan;
    var Puntos = $("#Puntos").val();
    var param_opcion = 'processVenta';
    dataToken = JSON.stringify({
        "card_number": card_number,
        "cvv": cvv,
        "expiration_month": expiration_month,
        "expiration_year": expiration_year,
        "email": email
    });

    //Crear Token
    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: urlSecure,
        dataType: 'json',
        data: dataToken,
        // Añade un header:
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + publicKey
        },
        // El resto del código
        success: function(token) {
            console.log(token);
            var montoculqi = amount.replace(".", "")
            dataCargo = JSON.stringify({
                "amount": montoculqi,
                "currency_code": "PEN",
                "email": email,
                "source_id": token.id
            })
            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: urlApi,
                dataType: 'json',
                data: dataCargo,
                // Añade un header:
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + privateKey
                },
                // El resto del código
                success: function(json) {
                    console.log(json);
                    $.ajax({
                        type: 'POST',
                        data: 'opcion=' + param_opcion +
                            '&card_number=' + card_number +
                            '&cvv=' + cvv +
                            '&email=' + email +
                            '&expiration_month=' + expiration_month +
                            '&expiration_year=' + expiration_year +
                            '&amount=' + amount +
                            '&description=' + description +
                            '&Puntos=' + Puntos,
                        url: '../../controlador/venta/cVenta.php',
                        success: function(data) {
                            var obj = JSON.parse(data);
                            //Code para ocultar loader
                            $(".loader").fadeOut();
                            $("#preloder").delay(200).fadeOut("slow");
                            console.log(obj);
                            if (obj == 1) {
                                alert("Compra exitosa");
                                $("#buyModal").modal("hide");
                            }


                        },
                        error: function(data) {
                            //Code para ocultar loader
                            $(".loader").fadeOut();
                            $("#preloder").delay(200).fadeOut("slow");
                            alert('Error al registrar venta y adicionar puntos');
                        }
                    });
                },
                error: function(data) {
                    //Code para ocultar loader
                    $(".loader").fadeOut();
                    $("#preloder").delay(200).fadeOut("slow");
                    alert('Error al crear cargo');
                }
            });
        },
        error: function(data) {
            //Code para ocultar loader
            $(".loader").fadeOut();
            $("#preloder").delay(200).fadeOut("slow");
            alert('Error al crear token');
        }
    });
    /*$.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&card_number=' + card_number +
            '&cvv=' + cvv +
            '&email=' + email +
            '&expiration_month=' + expiration_month +
            '&expiration_year=' + expiration_year +
            '&amount=' + amount +
            '&description=' + description +
            '&Puntos=' + Puntos,
        url: '../../controlador/venta/cVenta.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);


        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });*/

}

function registrar() {
    var Documento = $("#txtDocumento").val();
    var Nombres = $("#txtNombres").val();
    var Cmp = $("#txtCmp").val();
    var Ciudad = $("#txtCiudad").val();
    var Modelo = $("#txtModelo").val();
    var Marca = $("#txtMarca").val();
    var Correo = $("#txtCorreo").val();
    var Clave = $("#txtClave").val();
    var ClaveReingreso = $("#txtClaveReingreso").val();
    var Telefono = $("#txtTelefono").val();

    if (Documento == "") {
        alert("Ingrese un nro de documento válido");
        return false;
    }
    if (Nombres == "") {
        alert("Ingrese un nombre válido");
        return false;
    }
    if (Cmp == "") {
        alert("Ingrese un CMP válido");
        return false;
    }
    if (Ciudad == "") {
        alert("Ingrese una Ciudad válida");
        return false;
    }
    if (Modelo == "") {
        alert("Ingrese un Modelo válido");
        return false;
    }
    if (Marca == "") {
        alert("Ingrese una Marca válida");
        return false;
    }
    if (Correo == "") {
        alert("Ingrese un correo válido");
        return false;
    }
    if (Clave == "") {
        alert("Ingrese una contraseña válida");
        return false;
    }
    if (ClaveReingreso == "") {
        alert("Ingrese 2 veces la contraseña");
        return false;
    }
    if (ClaveReingreso != Clave) {
        alert("Las contraseñas no coninciden");
        return false;
    }

    var param_opcion = 'insertCliente';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&Documento=' + Documento +
            '&Nombres=' + Nombres +
            '&Cmp=' + Cmp +
            '&Ciudad=' + Ciudad +
            '&Modelo=' + Modelo +
            '&Marca=' + Marca +
            '&Correo=' + Correo +
            '&Clave=' + Clave +
            '&Telefono=' + Telefono,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            if (obj.idcliente != undefined && obj.idcliente != null) {
                $("#buyModal").modal("show");
            } else {
                $("#loginModal").modal("show");
                //alert('Error al encontrar usuario');
            }


        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });
}

function eventoModal() {

    $("#loginModal").on("hidden.bs.modal", function() {
        // Aquí va el código a disparar en el evento
        if (bandRegistro == 1) {
            //$("#registerModal").modal("show");
            bandRegistro = 0;
        }
    });
}

function listarConfig() {

    var param_opcion = 'listConfig';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/configuration/cConfiguration.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            var header = document.getElementById('title-header');
            header.innerHTML = obj[0].empresa;
            console.log(obj[0].logo);
            $("#logo-loading").attr("src", obj[0].logo);
            $("#logo-header").attr("src", obj[0].logo);
            $("#logo-footer").attr("src", obj[0].logo);
            //$('#Empresa').innerHTML(obj[0].empresa);
            //$('#title-header').innerHTML(obj[0].empresa);
            //$('#txtUsuario').val(obj[0].admin);
            //$('#txtNombre').val(obj[0].nombre);
            //$('#txtTelefono').val(obj[0].telefono);
            //$('#txtCorreo').val(obj[0].email);
            //$('#txtEmpresa').val(obj[0].empresa);
            //$("#imgLogo").attr("src", obj[0].logo);
            //$("#imgVideo").attr("src", obj[0].url);
            // $('#txtLogo').val("disabled", "disabled");
            //$('#txtVideo').val("disabled", "disabled");

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function alCargarDocumento() {
    listarPricing();
    eventoModal();
    listarConfig();
    listClienteSession();
    $("#Monto").blur(function() {
        console.log($("#Monto").val());
        var puntos = $("#Monto").val();
        var monto = parseFloat($("#Monto").val());
        monto = monto.toFixed(2);
        gv_plan = "PUNTOS";
        $("#Monto").val(monto);
        var oPricing = olPricing.filter(oBeneficio => oBeneficio.precio == monto);
        if (oPricing.length > 0) {
            puntos = oPricing[0].puntos;
            gv_plan = oPricing[0].nombre;
        }
        $("#Puntos").val(puntos);
        console.log(oPricing);
    });
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);