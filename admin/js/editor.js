var grid;
var email;
var selected_element = ""
var jsPDF = window.jspdf.jsPDF;
var signature_obj = {
    selected_signature: false,
    has_footer_signature: false,
    has_active_preview: false,
}
var active_drag_and_drop_zone = ""

var collage_zone_data = {
    paper_align: "vertical",
    proportions: { vertical: 0, horizontal: 0 },
    collage_content_area: document.getElementById("collage_content_area"),
    collage: document.getElementById("canvas"),
    signature: document.querySelector("#signature_collage_zone"),
    border_color: "#000000",
    background_color: "#FFFFFF",
}

var olPricing;
const publicKey = "pk_live_bfc8e9101d0a2790";
const privateKey = "sk_live_b54c47c3a632be40";
const urlSecure = "https://secure.culqi.com/v2/tokens";
const urlApi = "https://api.culqi.com/v2/charges";
var navigation_bar = document.getElementById("navigation_bar")

var paper_align_buttons = document.getElementsByName("paper_align")
var DragnDropRegions = document.getElementsByClassName("ddzone");
var bandLogin = 0;

var imgData;
// Input File
var inputImage = document.querySelector('#image');
// Nodo donde estará el editor
var editor = document.querySelector('#editor');
// El canvas donde se mostrará la previa
var miCanvas = document.querySelector('#preview');
// Contexto del canvas
var contexto = miCanvas.getContext('2d');
var cropper;
// Ruta de la imagen seleccionada
let urlImage = undefined;
var bandEditor = "";
$(function() {
    $(document).contextmenu({
        delegate: ".hasmenu",
        autoFocus: true,
        preventContextMenuForPopup: true,
        preventSelect: true,
        taphold: true,
        menu: [
            { title: "----------", disabled: true },
            { title: "Recorte Cuadrado", cmd: "editCuadrado", uiIcon: "ui-icon-scissors" },
            { title: "----------", disabled: true },
            { title: "Recorte Circular", cmd: "editCirculo", uiIcon: "ui-icon-scissors" },
            { title: "----------", disabled: true },
            { title: "Eliminar ", cmd: "delete", uiIcon: "ui-icon-trash" },
        ],
        // Handle menu selection to implement a fake-clipboard
        select: function(event, ui) {
            var $target = ui.target;
            switch (ui.cmd) {
                case "editCuadrado":
                    //Code here
                    //console.log($target[0].src);
                    bandEditor = "CD";
                    imgData = $target[0].src;
                    $("#rngBordes").val(0);
                    $("#rngBordes").change();
                    editor.innerHTML = '';
                    cropprImg = document.createElement('img');
                    cropprImg.setAttribute('id', 'editImage');
                    editor.appendChild(cropprImg);
                    $("#editImage").attr("src", $target[0].src);

                    contexto.clearRect(0, 0, miCanvas.width, miCanvas.height);

                    //$("editorModal").modal("show");
                    $("#editorModal").modal("show");
                    showEditorCuadrado();
                    break;
                case "editCirculo":
                    //Code here
                    //console.log($target[0].src);
                    bandEditor = "CC";
                    imgData = $target[0].src;
                    $("#rngBordes").val(0);
                    $("#rngBordes").change();
                    editor.innerHTML = '';
                    cropprImg = document.createElement('img');
                    cropprImg.setAttribute('id', 'editImage');
                    editor.appendChild(cropprImg);
                    $("#editImage").attr("src", $target[0].src);
                    contexto.clearRect(0, 0, miCanvas.width, miCanvas.height);

                    //$("editorModal").modal("show");
                    $("#editorModal").modal("show");
                    showEditorCircular();
                    break;
                case "delete":
                    //Code here
                    let image_dom = $target[0];
                    if (image_dom.classList.contains("selectable_image")) {
                        remove_selected_image(image_dom)
                    }
                    if (image_dom.classList.contains("selectable_signature")) {
                        remove_selected_signature(image_dom)
                    }
                    break;
            }
            // Optionally return false, to prevent closing the menu now
        },
        // Implement the beforeOpen callback to dynamically change the entries
        beforeOpen: function(event, ui) {
            var $menu = ui.menu,
                $target = ui.target,
                extraData = ui.extraData; // passed when menu was opened by call to open()

            // console.log("beforeOpen", event, ui, event.originalEvent.type);

            $(document)
                //        .contextmenu("replaceMenu", [{title: "aaa"}, {title: "bbb"}])
                //        .contextmenu("replaceMenu", "#options2")
                //        .contextmenu("setEntry", "cut", {title: "Cuty", uiIcon: "ui-icon-heart", disabled: true})
                .contextmenu("setEntry", "edit", "Editar")
                .contextmenu("setEntry", "delete", "Eliminar")
                .contextmenu("enableEntry", "paste", );

            // Optionally return false, to prevent opening the menu now
        }
    });

});
(function($) {

    /*------------------
        Preloader
    --------------------*/
    $(window).on('load', function() {
        //Code para ocultar loader
        $(".loader").fadeOut();
        $("#preloder").delay(200).fadeOut("slow");
        //Code para mostrar loader
        //$(".loader").fadeIn();
        //$("#preloder").delay(200).fadeIn("slow");
    });

})(jQuery);

function preventDefault(e) {
    e.preventDefault();
    e.stopPropagation();
}

function handleDrop(e) {
    var dt = e.dataTransfer,
        files = dt.files;
    if (files.length) {
        handleFiles(files, this.unload_zone, this.unload_action);
    } else {
        var html = dt.getData('text/html'),
            match = html && /\bsrc="?([^"\s]+)"?\s*/.exec(html),
            url = match && match[1];
        if (url) {
            uploadImageFromURL(url);
            return;
        }
    }

    function uploadImageFromURL(url) {
        var img = new Image;
        var c = document.createElement("canvas");
        var ctx = c.getContext("2d");
        img.onload = function() {
            c.width = this.naturalWidth;
            //c.height = this.naturalHeight;
            ctx.drawImage(this, 0, 0);
            c.toBlob(function(blob) {
                handleFiles([blob], this.unload_zone, this.unload_action);

            }, "image/png");
        };
        img.onerror = function() {
            alert("Error in uploading");
        }
        img.crossOrigin = "";
        img.src = url;
    }
}

function handleFiles(files, unload_zone, unload_action) {
    for (var i = 0, len = files.length; i < len; i++) {
        if (validateImage(files[i]))
            addImage(files[i], unload_zone, unload_action);
    }
}

function init_drag_and_drop_zone(ddzone_collection) {
    for (var i = 0, len = ddzone_collection.length; i < len; i++) {
        let dropRegion = ddzone_collection[i]
        let fakeInput = document.createElement("input");
        dropRegion.addEventListener('click', function() {
            fakeInput.click();
        });

        dropRegion.unload_zone = dropRegion.getAttribute("unload_zone");
        fakeInput.type = "file";
        fakeInput.accept = "image/*";
        fakeInput.multiple = true;
        fakeInput.unload_zone = dropRegion.getAttribute("unload_zone");
        fakeInput.unload_action = dropRegion.getAttribute("unload_action");

        fakeInput.addEventListener("change", function() {
            //var files = fakeInput.files;
            var files = this.files;
            handleFiles(files, this.unload_zone, this.unload_action);
        });

        dropRegion.input = fakeInput

        dropRegion.addEventListener('dragenter', preventDefault, false)
        dropRegion.addEventListener('dragleave', preventDefault, false)
        dropRegion.addEventListener('dragover', preventDefault, false)
        dropRegion.addEventListener('drop', preventDefault, false)
        dropRegion.addEventListener('drop', handleDrop, false);

    }
}
/* ---------- Image function -------------- */

function validateImage(image) {
    var validTypes = ['image/jpeg', 'image/png', 'image/gif'];
    if (validTypes.indexOf(image.type) === -1) {
        alert("Invalid File Type");
        return false;
    }
    var maxSizeInBytes = 10e6;
    if (image.size > maxSizeInBytes) {
        alert("File too large");
        return false;
    }

    return true;

}

function addImage(image, unload_zone, unload_action, image_type = "File") {
    if (unload_action == "add_to_pool") {
        var UnloadRegion = document.getElementById(unload_zone)
        var imgView = document.createElement("div");
        imgView.className = "toolbar-content__grid-item";
        //imagePreviewRegion.appendChild(imgView);
        UnloadRegion.appendChild(imgView);

        var img = document.createElement("img");
        img.className = "toolbar-content__grid-image selectable_image hasmenu";
        imgView.appendChild(img);
        add_selectable_image_events_to_obj(img)
    } else if (unload_action == "set_as_src") {
        var img = document.getElementById(unload_zone);
        if (img == null) {
            document.getElementById("signature_percentage_scroll").value = 60;
            signature_distribution_change("preview")
            img = document.getElementById(unload_zone);
        }
    }
    if (image_type == "File") {
        var reader = new FileReader();
        reader.onload = function(e) {
            img.src = e.target.result;
        }
        reader.readAsDataURL(image);
    } else if (image_type == "b64") {
        img.src = image;
    }


}

function refresh_selectable_image_events() {
    let images = document.getElementsByClassName("selectable_image");
    for (var i = 0, len = images.length; i < len; i++) {
        let current_image = images[i]
        add_selectable_image_events_to_obj(current_image)
    }
}

function add_selectable_image_events_to_obj(element) {
    //element.addEventListener('ondrag', OnDragSelectableImage, false)
    element.addEventListener('ondragstart', OnStartDragSelectableImage, false)
    element.addEventListener('click', function() { select_image(element) }, false)

    /*
    let element_parent = element.parentNode;
    var div = document.createElement("div");
    div.className = "ui-icon custom-icon-x-shape handle-top-rigth";
    div.parent_widget = element_parent;
    div.addEventListener("click", remove_selected_image, false);
    element_parent.appendChild(div);
    element_parent.custom_elements = { close_handle: div }
    */
}

function remove_selected_image(image_dom) {
    if (image_dom.classList.contains("selected_image_item")) {
        select_image(image_dom)
    }
    image_dom.parentNode.remove()
}


/*
function OnDragSelectableImage(event){
    document.getElementById("demo").innerHTML = "The p element is being dragged";
}
*/
function OnStartDragSelectableImage(event) {
    event.dataTransfer.setData("Text", event.target.src);
}

function select_image(image_dom) {
    if (image_dom.classList.contains("selected_image_item")) {
        image_dom.classList.remove("selected_image_item")
        image_dom.parentNode.classList.remove("selected_image_item_container")
        selected_element = ""
    } else {
        let previous_selected_image = document.querySelector(".selected_image_item")
        if (previous_selected_image != null) {
            previous_selected_image.classList.remove("selected_image_item")
            previous_selected_image.parentNode.classList.remove("selected_image_item_container")
        }
        image_dom.classList.add("selected_image_item")
        image_dom.parentNode.classList.add("selected_image_item_container")
        selected_element = image_dom.src
    }
}


/* ---------- Grid Functions -------------- */
function init_grid() {
    grid = GridStack.init({
        marginUnit: 'px',
        disableDrag: false,
        minWidth: 0,
        margin: 0,
        staticGrid: false,
        float: true,
        column: 12,
        cellHeight: 10,
        cellHeightUnit: 'px',
        disableOneColumnMode: 'grid-stack-one-column-mode',
    });
}

function handle_widget_drop(event) {
    event.preventDefault();
    var data = event.dataTransfer.getData("Text");
    event.target.style.backgroundImage = "url(" + data + ")"
        //.appendChild(document.getElementById(data));
}

function refreshGridItemEvents() {
    /*
    $(".grid-stack-item-content").click(function(event) {
        if (selected_element != "") {
            this.style.backgroundImage = "url(" + selected_element + ")"
        }
    })
    */
    let grid_items = grid.engine.nodes;
    for (var i = 0, len = grid_items.length; i < len; i++) {
        let item = grid_items[i]
        let widget_content = item.el.querySelector(".grid-stack-item-content")

        var div = document.createElement("div");
        div.className = "ui-icon custom-icon-x-shape handle-top-rigth";
        div.parent_widget = item;
        div.addEventListener("click", remove_panel, false);
        div.style.display = "none";
        item.el.appendChild(div);
        item.el.custom_elements = { close_handle: div }

        widget_content.addEventListener('dragenter', preventDefault, false)
        widget_content.addEventListener('dragleave', preventDefault, false)
        widget_content.addEventListener('dragover', preventDefault, false)
        widget_content.addEventListener('drop', handle_widget_drop, false);
        item.el.addEventListener('mouseover', function() {
            item.el.custom_elements.close_handle.style.display = "block";
        });
        item.el.addEventListener('mouseout', function() {
            item.el.custom_elements.close_handle.style.display = "none";
        });
    }

}

function on_mouse_over_grid_element() {
    this.custom_elements.close_handle.style.display = "block";
}

function on_mouse_out_grid_element() {
    this.custom_elements.close_handle.style.display = "none";
}

function add_new_panel() {
    grid.addWidget('<div><div class="grid-stack-item-content"></div></div>', { width: 1, height: 4 });
    refreshGridItemEvents()
}

function remove_panel() {
    grid.removeWidget(this.parent_widget.el, true);
}

function load_grid_template(template_id) {
    let template_data = template_master.get_template_by_id(template_id)
    if (template_data) {} else {
        template_data = template_master.get_template_by_id("", true)
        alert("No se encontro la plantilla, se va a cargar la plantilla por defecto")
    }
    template_master.load_template(grid, template_data)
    refreshGridItemEvents()
}


async function make_pdf() {
    if (confirm("Desea imprimir el collage, se consumiran 5 Puntos (S/N)")) {
        var paper_data = {},
            collage_data = {},
            proportion_data = {},
            pdf
        const hexToRGB = hex => {
            let r = 0,
                g = 0,
                b = 0;
            // handling 3 digit hex
            if (hex.length == 4) {
                r = "0x" + hex[1] + hex[1];
                g = "0x" + hex[2] + hex[2];
                b = "0x" + hex[3] + hex[3];
                // handling 6 digit hex
            } else if (hex.length == 7) {

                r = "0x" + hex[1] + hex[2];
                g = "0x" + hex[3] + hex[4];
                b = "0x" + hex[5] + hex[6];
            };

            return {
                red: +r,
                green: +g,
                blue: +b
            };
        }

        function add_image_to_pdf(pdf, x, y, w, h, image_url, img_type = "JPEG") {
            if (image_url && image_url != "") {
                if (image_url.includes('url("')) {
                    image_url = image_url.slice(5, -2)
                }
                if (image_url.includes("data:image")) {
                    let image_type = ((image_url.split(";")[0]).split("/")[1]).toUpperCase()
                    pdf.addImage(image_url, image_type, x + 1, y + 1, w - 2, h - 2, null, "FAST");
                    console.log("Add" + image_url)
                } else {
                    pdf.addImage(image_url, img_type, x + 1, y + 1, w - 2, h - 2);
                    console.log("Add" + image_url)
                }
            } else {
                pdf.setFillColor(collage_data.background_color.red, collage_data.background_color.green, collage_data.background_color.blue);
                pdf.rect(x + 1, y + 1, w - 2, h - 2, 'F');
            }
        }

        function update_proportion_data() {
            proportion_data.x = paper_data.width / 12;
            proportion_data.document_height = (collage_data.height * paper_data.height) / (collage_data.height + collage_data.signature_heigth) // Alto del documento en mm
            if (collage_data.has_signature) {
                let aux = collage_data.height
                let margin_signature = parseInt(document.getElementById("signature_position_scroll").value)
                proportion_data.y = proportion_data.document_height / (aux / 10);
                proportion_data.sig_h = (collage_data.signature_heigth * paper_data.height) / (collage_data.height + collage_data.signature_heigth) //altura de firma en mm
                proportion_data.sig_x = ((collage_data.width * (margin_signature / 100)) * paper_data.width) / (collage_data.width), // inicio de firma en hoja a4
                    proportion_data.sig_w = (collage_data.signature_width * paper_data.width) / (collage_data.width) // Ancho de firma en mm
            } else {
                proportion_data.y = paper_data.height / (collage_data.height / 10);
            }

        }

        function init_paper_config() {
            paper_data = {
                width: 0,
                height: 0,
            }
            collage_zone_data.signature
            collage_data = {
                width: parseFloat(collage_zone_data.collage.style.width.replace("px", "")),
                height: parseFloat(collage_zone_data.collage.style.height.replace("px", "")),
                background_color: hexToRGB(collage_zone_data.background_color),
                border_color: hexToRGB(collage_zone_data.border_color),
                signature_heigth: collage_zone_data.signature.clientHeight,
                has_signature: false
            }
            if (signature_obj.has_footer_signature) {
                collage_data.has_signature = true;
                collage_data.signature_width = document.getElementById("signature_zone").clientWidth;
            }
            if (collage_zone_data.paper_align == "vertical") {
                paper_data.width = 210
                paper_data.height = 297
                pdf = new jsPDF('p', 'mm', [297, 210]);
            } else {
                paper_data.width = 297
                paper_data.height = 210
                pdf = new jsPDF('l', 'mm', [210, 297]);
            }
        }
        init_paper_config()
        update_proportion_data()
        let grid_items = grid.engine.nodes;
        // PDF draw
        pdf.setFillColor(collage_data.background_color.red, collage_data.background_color.green, collage_data.background_color.blue);
        pdf.rect(0, 0, collage_data.width, collage_data.height, 'F');
        for (var i = 0, len = grid_items.length; i < len; i++) {
            let item = grid_items[i]
            let x = item.x * proportion_data.x,
                y = item.y * proportion_data.y,
                w = item.width * proportion_data.x,
                h = item.height * proportion_data.y;
            pdf.setFillColor(collage_data.border_color.red, collage_data.border_color.green, collage_data.border_color.blue);
            pdf.rect(x, y, w, h, 'F');
            let image_url = item.el.querySelector(".grid-stack-item-content")
            image_url = image_url.style.backgroundImage
            add_image_to_pdf(pdf, x, y, w, h, image_url)
                //pdf.addImage(item.src, "PNG", item.x * proportion_data.x, item.y * proportion_data.y, item.width * proportion_data.x, item.height * proportion_data.y, null, "FAST");
        }
        pdf.setFillColor(collage_data.background_color.red, collage_data.background_color.green, collage_data.background_color.blue);
        pdf.rect(0, proportion_data.document_height, collage_data.width, collage_data.height, 'F');
        if (collage_data.has_signature) {

            let x = proportion_data.sig_x,
                y = proportion_data.document_height,
                w = proportion_data.sig_w,
                h = proportion_data.sig_h;
            let image_url = document.getElementById("signature_zone")
            image_url = image_url.src
            add_image_to_pdf(pdf, x, y, w, h, image_url)
        }
        //let file_get = pdf.output("dataurlstring")
        //console.log("dad")
        //pdf.autoPrint();
        //This is a key for printing
        //pdf.output('dataurlnewwindow');
        let new_pdf = "" //pdf.output('dataurlstring');
        var newWin = window.frames["printf"];
        newWin.document.write('<body onload="window.print()"><div id="embed_zone" style="width:100%;height:100%"> <embed class="pdfobject" src="' + new_pdf + '" type="application/pdf" style="overflow: auto; width: 100%; height: 100%;"</div></body>');
        newWin.document.close();
    }

}

//Evento de carga de pdf a imprimir
function print_collage_iframe() {
    console.log("Cargo iframe")
    window.frames["printf"].focus();
    window.frames["printf"].print();
}

async function print_collage() {
    let opcion = "deductPoints"
        //let opcion = "listCliente"
    var ptosValidation = 0;
    ptosValidation = $("#hdPtos").val();
    if (ptosValidation <= 0) {
        if (confirm("No tiene puntos disponibles, ¿Desea comprar puntos?")) {
            var param_opcion = 'listClienteBySession';
            $.ajax({
                type: 'POST',
                data: 'opcion=' + param_opcion,
                url: 'controlador/cliente/cCliente.php',
                success: function(data) {
                    var obj = JSON.parse(data);
                    console.log(obj);
                    if (obj[0] != undefined && obj[0] != null) {
                        email = obj[0].correo;
                        $("#documentoCard").val(obj[0].documento);
                        $("#titularCard").val(obj[0].nombres);
                        $("#titularCard").keyup();

                        param_opcion = 'listBeneficios';
                        $.ajax({
                            type: 'POST',
                            data: 'opcion=' + param_opcion,
                            url: 'controlador/beneficios/cBeneficios.php',
                            success: function(data) {
                                olBeneficio = JSON.parse(data);
                                console.log(olBeneficio);

                                param_opcion = 'listPricing';
                                $.ajax({
                                    type: 'POST',
                                    data: 'opcion=' + param_opcion,
                                    url: '../../controlador/tarifas/cTarifas.php',
                                    success: function(data) {
                                        olPricing = JSON.parse(data);
                                        $("#buyModal").modal("show");
                                    },
                                    error: function(data) {
                                        alert('Error al mostrar');
                                    }
                                });
                            },
                            error: function(data) {
                                alert('Error al mostrar');
                            }
                        });
                    } else {
                        $("#loginModal").modal("show");
                        bandLogin = 1;
                        //alert('Error al encontrar usuario');
                    }


                },
                error: function(data) {
                    alert('Error al encontrar cliente');
                }
            });
        }
        return false;
    }
    if (confirm("Desea imprimir el collage, se consumiran 3 Puntos (S/N)")) {

        var deduct_points = new Promise(async function(resolve, reject) {
            $.ajax({
                type: 'POST',
                data: 'opcion=' + opcion,
                url: '../../controlador/cliente/cCliente.php',
                success: async function(data) {
                    console.log(data);
                    var ptos = 0;
                    ptos = $("#hdPtos").val();
                    ptos = ptos - 3;
                    $("#hdPtos").val(ptos);
                    lblPtos = ptos + " Puntos"
                    $("#txtPtos").text(lblPtos);
                    resolve(true)
                },
                error: function(data) {
                    console.log(data);
                    alert('Error al deducir puntos');
                    reject(false)
                }
            });
        })
        let deduct_points_response = await deduct_points
        if (deduct_points_response) {
            let collage = document.querySelector("#collage_print_zone")
            domtoimage.toPng(collage)
                .then(function(collage_url) {
                    var paper_data = {},
                        collage_data = {},
                        pdf
                    const hexToRGB = hex => {
                        let r = 0,
                            g = 0,
                            b = 0;
                        // handling 3 digit hex
                        if (hex.length == 4) {
                            r = "0x" + hex[1] + hex[1];
                            g = "0x" + hex[2] + hex[2];
                            b = "0x" + hex[3] + hex[3];
                            // handling 6 digit hex
                        } else if (hex.length == 7) {

                            r = "0x" + hex[1] + hex[2];
                            g = "0x" + hex[3] + hex[4];
                            b = "0x" + hex[5] + hex[6];
                        };

                        return {
                            red: +r,
                            green: +g,
                            blue: +b
                        };
                    }

                    function init_paper_config() {
                        paper_data = {
                            width: 0,
                            height: 0,
                        }
                        collage_zone_data.signature
                        collage_data = {
                            width: parseFloat(collage_zone_data.collage.style.width.replace("px", "")),
                            height: parseFloat(collage_zone_data.collage.style.height.replace("px", "")),
                            //background_color: hexToRGB(collage_zone_data.background_color),
                            //border_color: hexToRGB(collage_zone_data.border_color),
                            //signature_heigth: collage_zone_data.signature.clientHeight,
                            //has_signature: false
                        }
                        if (signature_obj.has_footer_signature) {
                            collage_data.has_signature = true;
                            collage_data.signature_width = document.getElementById("signature_zone").clientWidth;
                        }
                        if (collage_zone_data.paper_align == "vertical") {
                            paper_data.width = 210
                            paper_data.height = 297
                            pdf = new jsPDF('p', 'mm', [297, 210]);
                        } else {
                            paper_data.width = 297
                            paper_data.height = 210
                            pdf = new jsPDF('l', 'mm', [210, 297]);
                        }
                    }
                    init_paper_config()
                    let image_type = ((collage_url.split(";")[0]).split("/")[1]).toUpperCase()

                    //Ajustar margenes
                    var paper_margin = { left: 8, rigth: 8, upper: 10, bottom: 10 }
                    pdf.addImage(collage_url, image_type,
                        0 + paper_margin.left, // Inicio izquierda
                        0 + paper_margin.upper, // Inicio arriba
                        paper_data.width - (2 * paper_margin.rigth), // fin derecha
                        paper_data.height - (2 * paper_margin.bottom), // Fin abajo
                        null, "NONE");
                    let pdf_blob = pdf.output('blob')
                    let pdf_blob_virtual_url = URL.createObjectURL(pdf_blob);
                    var newWin = document.getElementById("printf")
                    newWin.src = pdf_blob_virtual_url
                    console.log("Crear pdf")
                })
                .catch(function(error) {
                    console.error('No se pudo generar la imagen del collage', error);
                });
        }
    }
}

function change_collage_background_color(color) {
    let collage = document.querySelector("#canvas")
    collage.style.backgroundColor = color;
    collage_zone_data.background_color = color;
}

function change_collage_border_color(color) {
    collage_zone_data.border_color = color;
    let grid_items = grid.engine.nodes;
    for (var i = 0, len = grid_items.length; i < len; i++) {
        grid_items[i].el.style.borderColor = color;
    }
}

/* Canvas functions */
function change_page_align(value) {
    console.log("llamo funcion")
    collage_zone_data.paper_align = value
    if (value == "vertical") {
        collage_zone_data.proportions = { vertical: 1, horizontal: 0.7 }
    }
    if (value == "horizontal") {
        collage_zone_data.proportions = { vertical: 0.7, horizontal: 1 }
    }
    resize_canvas()
}


function resize_canvas() {
    let new_height, new_width = "0px"
    let vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
    let vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)

    let nav_bar_height = navigation_bar.clientHeight;
    let signature_zone_height = collage_zone_data.signature.clientHeight;

    let collage_zone_width = collage_zone_data.collage_content_area.clientWidth;

    let usable_width = collage_zone_width
    let usable_height = vh - nav_bar_height - signature_zone_height

    let proportions = collage_zone_data.proportions
    let tent_width = ((usable_height * 1.0) * proportions.horizontal) / proportions.vertical
    let tent_height = ((usable_width * 1.0) * proportions.vertical) / proportions.horizontal
    let base_value = 0
    if (tent_width <= usable_width) {
        new_width = tent_width.toString() + "px"
        new_height = usable_height.toString() + "px"
    } else if (tent_height <= usable_height) {
        new_width = usable_width.toString() + "px"
        new_height = tent_height.toString() + "px"
    }

    collage_zone_data.collage.style.height = new_height
    collage_zone_data.collage.style.width = new_width
}
/* Signature functions */

function load_client_premade_signatures() {
    var opcion = 'listSignatureByLoggedClient';
    var user_signature_zone = document.getElementById("user_signature");
    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion,
        url: '../../controlador/firma/cFirma.php',
        success: function(data) {
            console.log(data)
            var obj = JSON.parse(data);
            for (var i = 0; i < obj.length; i++) {
                let signature_record = obj[i]
                var imgView = document.createElement("div");
                imgView.className = "toolbar-content__grid-item";
                user_signature_zone.appendChild(imgView);
                var img = document.createElement("img");
                img.className = "toolbar-content__grid-image selectable_signature hasmenu";
                img.dataId = signature_record["idfirma"]
                img.src = signature_record["url"]
                if (img.addEventListener) {
                    img.addEventListener('click', function() {
                        select_signature(this, "img")
                    });
                } else if (img.attachEvent) {
                    img.attachEvent('onclick', function() {
                        select_signature(this, "img")
                    });
                }
                imgView.appendChild(img);
                imgView.dataId = signature_record["idfirma"]
                    /*
                    var div = document.createElement("div");
                    div.className = "ui-icon custom-icon-x-shape handle-top-rigth";
                    div.parent_widget = imgView;
                    div.addEventListener("click", remove_selected_signature, false);
                    imgView.appendChild(div);
                    imgView.custom_elements = { close_handle: div }
                    */

            }
        },
        error: function(data) {
            alert('Error al recuperar firmas del usuario');
        }
    });
}

function remove_selected_signature(image_dom) {
    var formData = new FormData();
    formData.append("idFirma", parseInt(image_dom.dataId));
    formData.append("opcion", 'eliminarFirma');
    $.ajax({
        url: "../../controlador/firma/cFirma.php",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data) {
            switch (data) {
                case "0":
                    console.log("No se pudo borrar la firma");
                    break;
                case "1":
                    if (image_dom.classList.contains("selected_signature_item")) {
                        select_signature(image_dom, "img")
                    }
                    image_dom.parentNode.remove();
                    break;
                case "2":
                    console.log("Firma no encontrada en base de datos");
                    break;
                case "4":
                    console.log("No se encontro la firma en servidor");
                    break;
            }

            console.log(data)
            image_dom.remove()
        },
        error: function(data) {
            alert('Error al recuperar firmas del usuario');
        }
    });
}

function select_signature(signature_img, data_type) {
    if (data_type == "img") {
        if (signature_img.classList.contains("selected_signature_item")) {
            signature_img.classList.remove("selected_signature_item")
            signature_img.parentNode.classList.remove("selected_signature_item_container")
            signature_obj.selected_signature = false
        } else {
            let previous_selected_signature = document.querySelector(".selected_signature_item")
            if (previous_selected_signature != null) {
                previous_selected_signature.classList.remove("selected_signature_item")
                previous_selected_signature.parentNode.classList.remove("selected_signature_item_container")
            }
            signature_img.classList.add("selected_signature_item")
            signature_img.parentNode.classList.add("selected_signature_item_container")
            signature_obj.selected_signature = signature_img
        }
    } else {
        if (data_type == "set_signature") {
            //<img src="" alt="" id="signature_zone" class="signature_collage_container">
            let has_signature = collage_zone_data.signature.querySelector("#signature_zone")
            if (!has_signature) {
                var img = document.createElement("img");
                img.id = "signature_zone";
                img.class = "signature_collage_container";
                collage_zone_data.signature.appendChild(img);
                signature_width_change('collage')
                signature_position_change()
            }
            let signature_zone = document.getElementById("signature_zone")
            signature_zone.src = signature_obj.selected_signature.src
            signature_obj.has_footer_signature = true
        } else if (data_type == "remove_signature") {
            collage_zone_data.signature.innerHTML = ""
            signature_obj.has_footer_signature = false
        }
        resize_canvas()
    }
    reload_selected_signature_options()
}

function reload_selected_signature_options() {
    let selected_signature = signature_obj.selected_signature

    let signature_button = document.getElementsByClassName("btn_select_signature_for_collage")
    let signature_footer_config = document.getElementsByClassName("footer_signature_config_section")
    let signature_preview_config = document.querySelectorAll("[marker='preview_signature_config_section']");
    //document.getElementsByClassName("preview_signature_config_section")
    if (selected_signature) {
        for (let i = 0; i < signature_button.length; i++) {
            signature_button[i].style.display = "block"
        }
    } else {
        for (let i = 0; i < signature_button.length; i++) {
            signature_button[i].style.display = "none"
        }
    }

    if (signature_obj.has_footer_signature) {
        for (let i = 0; i < signature_footer_config.length; i++) {
            signature_footer_config[i].style.display = "block"
        }
    } else {
        for (let i = 0; i < signature_footer_config.length; i++) {
            signature_footer_config[i].style.display = "none"
        }
    }

    if (signature_obj.has_active_preview) {
        for (let i = 0; i < signature_preview_config.length; i++) {
            signature_preview_config[i].style.display = "flex"
        }
    } else {
        for (let i = 0; i < signature_preview_config.length; i++) {
            signature_preview_config[i].style.display = "none"
        }
    }
}

function save_signature(signature_img, data_type) {
    let signature_image_region = document.getElementById("user_signature")
    if (data_type == "b64") {
        preview_id = "#" + signature_img
        let preview = document.querySelector(preview_id);
        domtoimage.toPng(preview)
            .then(function(dataUrl) {
                var file = dataURLtoFile(dataUrl, 'temp.png');
                var formData = new FormData();
                formData.append("File", file);
                formData.append("opcion", 'insertarFirma');
                $.ajax({
                    url: "../../controlador/firma/cFirma.php",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        signature_image_region.innerHTML = ""
                        load_client_premade_signatures()
                    },
                    error: function(data) {
                        alert('Error al recuperar firmas del usuario');
                    }

                });
            })
            .catch(function(error) {
                console.error('No se pudo generar la firma', error);
            });
    }
}

function reset_signature_preview() {
    let text_area = document.getElementById("t_dedicatoria")
    text_area.value = ""
    let signature_percentage_scroll = document.getElementById("signature_percentage_scroll")
    signature_percentage_scroll.value = 100
    console.log("xd")
    signature_distribution_change()
    reload_preview()
}

function dataURLtoFile(dataurl, filename) {

    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);

    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
}



function reload_preview() {
    let preview = document.getElementById("signature_preview_container")
    let text_area = document.getElementById("t_dedicatoria")
    let text_preview = document.getElementById("text_preview")
    let text_value = text_area.value
    text_value = text_value.replace(/(?:\r\n|\r|\n)/g, '<br>');
    text_preview.innerHTML = text_value
    text_preview.style.fontFamily = text_area.style.fontFamily
    if (text_area.value == "") {
        signature_obj.has_active_preview = false
    } else {
        signature_obj.has_active_preview = true
    }
    reload_selected_signature_options()
}

function signature_width_change(type) {
    if (type == "preview") {
        let signature_preview = document.getElementById("signature_preview_container")
        let signature_width_value = document.getElementById("signature_width_value")
        let signature_width_scroll = document.getElementById("signature_width_scroll")

        signature_width_value.innerHTML = signature_width_scroll.value
        signature_preview.style.width = signature_width_scroll.value.toString() + "px";
    } else {
        let signature_img = document.getElementById("signature_zone")
        let signature_width_scroll = document.getElementById("signature_width_scroll_collage")
        let signature_width_value = document.getElementById("signature_width_value_collage")

        let signature_position_scroll = document.getElementById("signature_position_scroll")
        let aux = (parseInt(signature_width_scroll.value) + parseInt(signature_position_scroll.value))
        if (aux > 100) {
            signature_width_scroll.value = 100 - signature_position_scroll.value
        }

        signature_width_value.innerHTML = signature_width_scroll.value
        signature_img.style.width = signature_width_scroll.value.toString() + "%";
        resize_canvas()
    }

}

function signature_position_change() {
    let signature_img = document.getElementById("signature_zone")
    let signature_position_scroll = document.getElementById("signature_position_scroll")
    let signature_position_value = document.getElementById("signature_position_value")

    let signature_width_scroll = document.getElementById("signature_width_scroll_collage")
    let aux = (parseInt(signature_width_scroll.value) + parseInt(signature_position_scroll.value))
    if (aux > 100) {
        signature_position_scroll.value = 100 - signature_width_scroll.value
    }

    signature_position_value.innerHTML = signature_position_scroll.value
    signature_img.style.marginLeft = signature_position_scroll.value.toString() + "%";
}

function signature_distribution_change() {
    let preview = document.getElementById("signature_preview_container")
    let signature_text_part = document.getElementById("text_preview")
    let signature_image_part = document.getElementById("signature_img")

    let signature_percentage_value = document.getElementById("signature_percentage_value")
    let signature_percentage_scroll = document.getElementById("signature_percentage_scroll")


    signature_percentage_value.innerHTML = signature_percentage_scroll.value

    if (signature_percentage_scroll.value == 100 && signature_image_part != null) {
        signature_image_part.outerHTML = "";

    } else if (signature_image_part == null) {
        var img = document.createElement("img");
        img.className = "editor_signature_img";
        img.id = "signature_img"
        preview.appendChild(img)
        signature_image_part = img
    }

    signature_text_part.style.width = signature_percentage_scroll.value.toString() + "%";
    signature_image_part.style.width = (100 - signature_percentage_scroll.value).toString() + "%";
}

function signature_heigth_change() {
    if (type == "preview") {
        //let signature_preview_wrapper = document.getElementById("signature_preview_wrapper")
        let signature_preview = document.getElementById("signature_preview_container")
        let signature_heigth_value = document.getElementById("signature_heigth_value")
        let signature_heigth_scroll = document.getElementById("signature_heigth_scroll")

        signature_heigth_value.innerHTML = signature_heigth_scroll.value
        signature_preview.style.height = signature_heigth_scroll.value.toString() + "px";
        //signature_preview_wrapper.style.height = signature_heigth_scroll.value.toString() + "px";
    } else {
        let signature_img = document.getElementById("signature_zone")
        let signature_scroll = document.getElementById("signature_heigth_scroll_collage")
        let signature_value = document.getElementById("signature_heigth_value_collage")
        signature_value.innerHTML = signature_scroll.value
        signature_img.style.width = signature_scroll.value.toString() + "px";
    }
}


function validateUsuario() {
    var param_opcion = 'listClienteBySession';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            if (obj != undefined && obj != null && obj != 0) {
                if (obj[0].idcliente != undefined && obj[0].idcliente != null) {
                    $("#hdPtos").val(obj[0].puntos);
                    lblPtos = obj[0].puntos + " Puntos"
                    $("#txtPtos").text(lblPtos);
                } else {
                    $("#loginModal").modal("show");
                }
            } else {
                $("#loginModal").modal("show");
            }



        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });
}

function RegistrarUsuario() {
    //console.log("asedadad");
    //$("#buyModal").modal("hide");
    bandLogin = 1;
    $("#loginModal").modal("hide");
    $("#registerModal").modal("show");

}

function login() {
    var Correo = $("#txtCorreoL").val();
    var Clave = $("#txtClaveL").val();
    var param_opcion = 'initSesion';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&Correo=' + Correo +
            '&Clave=' + Clave,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            if (obj != undefined && obj != null && obj != 1) {
                if (obj[0].idcliente != undefined && obj[0].idcliente != null) {
                    //$("#buyModal").modal("show");
                } else {
                    if (obj == 1) {
                        $("#loginModal").modal("hide");
                    } else {
                        $("#loginModal").modal("show");

                    }
                    //alert('Error al encontrar usuario');
                }
            } else {
                if (obj == 1) {
                    $("#loginModal").modal("hide");
                } else {
                    $("#loginModal").modal("show");

                }
                //alert('Error al encontrar usuario');
            }


        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });
}

function registrar() {
    var Documento = $("#txtDocumento").val();
    var Nombres = $("#txtNombres").val();
    var Cmp = $("#txtCmp").val();
    var Ciudad = $("#txtCiudad").val();
    var Modelo = $("#txtModelo").val();
    var Marca = $("#txtMarca").val();
    var Correo = $("#txtCorreo").val();
    var Clave = $("#txtClave").val();
    var ClaveReingreso = $("#txtClaveReingreso").val();
    var Telefono = $("#txtTelefono").val();

    if (Documento == "") {
        alert("Ingrese un nro de documento válido");
        return false;
    }
    if (Nombres == "") {
        alert("Ingrese un nombre válido");
        return false;
    }
    if (Cmp == "") {
        alert("Ingrese un CMP válido");
        return false;
    }
    if (Ciudad == "") {
        alert("Ingrese una Ciudad válida");
        return false;
    }
    if (Modelo == "") {
        alert("Ingrese un Modelo válido");
        return false;
    }
    if (Marca == "") {
        alert("Ingrese una Marca válida");
        return false;
    }
    if (Correo == "") {
        alert("Ingrese un correo válido");
        return false;
    }
    if (Clave == "") {
        alert("Ingrese una contraseña válida");
        return false;
    }
    if (ClaveReingreso == "") {
        alert("Ingrese 2 veces la contraseña");
        return false;
    }
    if (ClaveReingreso != Clave) {
        alert("Las contraseñas no coninciden");
        return false;
    }

    var param_opcion = 'insertCliente';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&Documento=' + Documento +
            '&Nombres=' + Nombres +
            '&Cmp=' + Cmp +
            '&Ciudad=' + Ciudad +
            '&Modelo=' + Modelo +
            '&Marca=' + Marca +
            '&Correo=' + Correo +
            '&Clave=' + Clave +
            '&Telefono=' + Telefono,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            if (obj.idcliente != undefined && obj.idcliente != null) {
                $("#buyModal").modal("show");
            } else {
                $("#registerModal").modal("hide");
                $("#loginModal").modal("show");
                //alert('Error al encontrar usuario');
            }


        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });
}

function registrarVenta() {

    //Code para mostrar loader
    $(".loader").fadeIn();
    $("#preloder").delay(200).fadeIn("slow");
    var card_number = $("#numberCard").val().replace(/ /g, "");
    var cvv = $("#ccvCard").val().trim();
    var email = $("#txtCorreoL").val();
    var expiration_month = $("#dateCard").val().split("/")[0].trim();
    var expiration_year = $("#dateCard").val().split("/")[1].trim();
    var amount = $("#Monto").val();
    var description = gv_plan;
    var Puntos = $("#Puntos").val();
    var param_opcion = 'processVenta';
    dataToken = JSON.stringify({
        "card_number": card_number,
        "cvv": cvv,
        "expiration_month": expiration_month,
        "expiration_year": expiration_year,
        "email": email
    });

    //Crear Token
    $.ajax({
        type: 'POST',
        contentType: "application/json",
        url: urlSecure,
        dataType: 'json',
        data: dataToken,
        // Añade un header:
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + publicKey
        },
        // El resto del código
        success: function(token) {
            console.log(token);
            var montoculqi = amount.replace(".", "")
            dataCargo = JSON.stringify({
                "amount": montoculqi,
                "currency_code": "PEN",
                "email": email,
                "source_id": token.id
            })
            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: urlApi,
                dataType: 'json',
                data: dataCargo,
                // Añade un header:
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + privateKey
                },
                // El resto del código
                success: function(json) {
                    console.log(json);
                    $.ajax({
                        type: 'POST',
                        data: 'opcion=' + param_opcion +
                            '&card_number=' + card_number +
                            '&cvv=' + cvv +
                            '&email=' + email +
                            '&expiration_month=' + expiration_month +
                            '&expiration_year=' + expiration_year +
                            '&amount=' + amount +
                            '&description=' + description +
                            '&Puntos=' + Puntos,
                        url: '../../controlador/venta/cVenta.php',
                        success: function(data) {
                            var obj = JSON.parse(data);
                            console.log(obj);
                            //Code para ocultar loader
                            $(".loader").fadeOut();
                            $("#preloder").delay(200).fadeOut("slow");
                            if (obj == 1) {
                                var ptos = 0;
                                ptos = $("#hdPtos").val();
                                ptos = parseInt(ptos) + parseInt(Puntos);
                                $("#hdPtos").val(ptos);
                                lblPtos = ptos + " Puntos"
                                $("#txtPtos").text(lblPtos);
                                $("#buyModal").modal("hide");
                                alert("Compra exitosa");
                            }


                        },
                        error: function(data) {
                            //Code para ocultar loader
                            $(".loader").fadeOut();
                            $("#preloder").delay(200).fadeOut("slow");
                            alert('Error al registrar venta y adicionar puntos');
                        }
                    });
                },
                error: function(data) {
                    //Code para ocultar loader
                    $(".loader").fadeOut();
                    $("#preloder").delay(200).fadeOut("slow");
                    alert('Error al crear cargo');
                }
            });
        },
        error: function(data) {
            //Code para ocultar loader
            $(".loader").fadeOut();
            $("#preloder").delay(200).fadeOut("slow");
            alert('Error al crear token');
        }
    });

    /* $.ajax({
         type: 'POST',
         data: 'opcion=' + param_opcion +
             '&card_number=' + card_number +
             '&cvv=' + cvv +
             '&email=' + email +
             '&expiration_month=' + expiration_month +
             '&expiration_year=' + expiration_year +
             '&amount=' + amount +
             '&description=' + description +
             '&Puntos=' + Puntos,
         url: '../../controlador/venta/cVenta.php',
         success: function(data) {
             var obj = JSON.parse(data);
             console.log(obj);


         },
         error: function(data) {
             alert('Error al encontrar cliente');
         }
     });*/
}

function eventoModal() {

    $("#loginModal").on("hidden.bs.modal", function() {
        // Aquí va el código a disparar en el evento
        if (bandLogin == 0) {
            validateUsuario();
        }
    });
    $("#registerModal").on("hidden.bs.modal", function() {
        // Aquí va el código a disparar en el evento
        bandLogin = 0;
        validateUsuario();
    });
}

function select_font(value) {
    let inscription = document.querySelector("#t_dedicatoria");
    inscription.style.fontFamily = value;
    reload_preview()
}

function listarConfig() {

    var param_opcion = 'listConfig';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/configuration/cConfiguration.php',
        success: function(data) {
            var obj = JSON.parse(data);
            $("#logo-header").attr("src", obj[0].logo);

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function buy_puntos() {
    // $("#buyModal").modal("show");
    var param_opcion = 'listClienteBySession';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: 'controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            if (obj[0] != undefined && obj[0] != null) {
                email = obj[0].correo;
                $("#documentoCard").val(obj[0].documento);
                $("#titularCard").val(obj[0].nombres);
                $("#titularCard").keyup();

                param_opcion = 'listBeneficios';
                $.ajax({
                    type: 'POST',
                    data: 'opcion=' + param_opcion,
                    url: 'controlador/beneficios/cBeneficios.php',
                    success: function(data) {
                        olBeneficio = JSON.parse(data);
                        console.log(olBeneficio);

                        param_opcion = 'listPricing';
                        $.ajax({
                            type: 'POST',
                            data: 'opcion=' + param_opcion,
                            url: '../../controlador/tarifas/cTarifas.php',
                            success: function(data) {
                                olPricing = JSON.parse(data);
                                $("#buyModal").modal("show");
                            },
                            error: function(data) {
                                alert('Error al mostrar');
                            }
                        });
                    },
                    error: function(data) {
                        alert('Error al mostrar');
                    }
                });
            } else {
                $("#loginModal").modal("show");
                bandLogin = 1;
                //alert('Error al encontrar usuario');
            }


        },
        error: function(data) {
            alert('Error al encontrar cliente');
        }
    });


}

function show_modal_crear() {
    $("#t_dedicatoria").val("");
    $("#firmaModal").modal("show");
}

function borderRadious() {
    var principal = document.getElementById('preview');
    var br = document.getElementById('rngBordes');
    var value = br.value + "%";
    principal.style.borderRadius = value;
}

function saveEditImage() {
    if (bandEditor == "CC") {
        var roundedImage = document.getElementById("imgCircular");
        imagenEn64 = roundedImage.src;
    }
    if (bandEditor == "CD") {
        imagenEn64 = miCanvas.toDataURL("image/jpeg");
    }
    addImage(imagenEn64, "user_image_container", "add_to_pool", "b64");
}

function getRoundedCanvas(sourceCanvas) {
    var canvas = document.createElement("canvas");
    var context = canvas.getContext("2d");
    var width = sourceCanvas.width;
    var height = sourceCanvas.height;

    canvas.width = width;
    canvas.height = height;
    context.imageSmoothingEnabled = true;
    context.drawImage(sourceCanvas, 0, 0, width, height);
    context.globalCompositeOperation = "destination-in";
    context.beginPath();
    context.arc(
        width / 2,
        height / 2,
        Math.min(width, height) / 2,
        0,
        2 * Math.PI,
        true
    );
    context.fill();
    return canvas;
}

function showEditorCircular() {
    var image = document.getElementById("editImage");
    var button = document.getElementById("image");
    var result = document.getElementById("preview__content");
    var imgCircular = document.getElementById("imgCircular");
    if (imgCircular != undefined) {
        imgCircular.setAttribute('src', '');
    }
    var croppable = false;
    console.log(image.width + "-" + image.height);
    cropper = new Cropper(image, {
        aspectRatio: 1,
        preview: '#preview',
        ready: function() {
            croppable = true;
        },
        crop: function() {
            var croppedCanvas;
            var roundedCanvas;
            var roundedImage;

            if (!croppable) {
                return;
            }

            // Crop
            croppedCanvas = cropper.getCroppedCanvas();

            // Round
            roundedCanvas = getRoundedCanvas(croppedCanvas);

            // Show
            roundedImage = document.createElement("img");
            roundedImage.setAttribute('id', 'imgCircular');
            roundedImage.src = roundedCanvas.toDataURL();
            result.innerHTML = "";
            result.appendChild(roundedImage);
            //miCanvas.src = roundedCanvas.toDataURL();
        }
    });
}

function showEditorCuadrado() {
    var result = document.getElementById("preview__content");
    var roundedImage = document.createElement("canvas");
    roundedImage.setAttribute('id', 'preview');

    result.innerHTML = "";
    result.appendChild(roundedImage);
    miCanvas = document.querySelector('#preview');
    // Obtiene la imagen
    urlImage = imgData; // URL.createObjectURL(e.target.files[0]);

    // Borra editor en caso que existiera una imagen previa
    editor.innerHTML = '';
    let cropprImg = document.createElement('img');
    cropprImg.setAttribute('id', 'croppr');
    editor.appendChild(cropprImg);

    // Limpia la previa en caso que existiera algún elemento previo
    contexto.clearRect(0, 0, miCanvas.width, miCanvas.height);

    // Envia la imagen al editor para su recorte
    document.querySelector('#croppr').setAttribute('src', urlImage);

    // Crea el editor
    cropper = new Croppr('#croppr', {
        startSize: [50, 50],
        onCropEnd: recortarImagenCuadrado
    });
}

/**
 * Método que recorta la imagen con las coordenadas proporcionadas con croppr.js
 */
function recortarImagenCuadrado(data) {
    miCanvas = document.querySelector('#preview');
    contexto = miCanvas.getContext('2d');
    // Variables
    const inicioX = data.x;
    const inicioY = data.y;
    const nuevoAncho = data.width;
    const nuevaAltura = data.height;
    const zoom = 1;
    let imagenEn64 = '';
    // La imprimo
    miCanvas.width = nuevoAncho;
    miCanvas.height = nuevaAltura;
    // La declaro
    let miNuevaImagenTemp = new Image();
    // Cuando la imagen se carge se procederá al recorte
    miNuevaImagenTemp.onload = function() {
            // Se recorta
            contexto.drawImage(miNuevaImagenTemp, inicioX, inicioY, nuevoAncho * zoom, nuevaAltura * zoom, 0, 0, nuevoAncho, nuevaAltura);
            // Se transforma a base64
            imagenEn64 = miCanvas.toDataURL("image/jpeg");
            // Mostramos el código generado
            //document.querySelector('#base64').textContent = imagenEn64;
        }
        // Proporciona la imagen cruda, sin editarla por ahora
        //  Setea imagen en preview
    miNuevaImagenTemp.src = urlImage;
}

function listarPasos() {

    $(".loader").fadeIn();
    $("#preloder").delay(200).fadeIn("slow");
    var param_opcion = 'listStep';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: 'controlador/configuration/cConfiguration.php',
        success: function(data) {
            var steps = document.getElementById('steps');
            var obj = JSON.parse(data);
            var cont = 0;
            var step = "";
            console.log(obj);
            obj.forEach(function(e) {
                step = "";
                step = step + '<div class="col-lg-3 col-md-6 col-sm-6"><div class="services__item"><img src="';
                step = step + e.url;
                step = step + '" alt=""><h4>';
                step = step + e.titulo;
                step = step + '</h4><p>';
                step = step + e.descripcion;
                step = step + '</p></div></div>';
                steps.innerHTML += step;
            });
            $(".loader").fadeOut();
            $("#preloder").delay(200).fadeOut("slow");

        },
        error: function(data) {
            alert('Error al mostrar');
            $(".loader").fadeOut();
            $("#preloder").delay(200).fadeOut("slow");
        }
    });

}

$(document).ready(function() {
    // 	v-if only on portrait - landscape is fine without js!!!

    // 	TODO:
    // http://stackoverflow.com/questions/13655919/how-to-bind-both-mousedown-and-touchstart-but-not-respond-to-both-android-jqu
    eventoModal();
    listarConfig();
    listarPasos();
    $("#stepsModal").modal("show");
    validateUsuario();
    let isOpenToolbar = false;
    $("#close-toolbar").on("click", function(e) {
        // console.log(e.type);
        $("#collage-toolbar").removeClass("open");
        $(this).toggleClass("closed");
        isOpenToolbar = false;
    });

    $("#Monto").blur(function() {
        console.log($("#Monto").val());
        var puntos = $("#Monto").val();
        var monto = parseFloat($("#Monto").val());
        monto = monto.toFixed(2);
        gv_plan = "PUNTOS";
        $("#Monto").val(monto);
        var oPricing = olPricing.filter(oBeneficio => oBeneficio.precio == monto);
        if (oPricing.length > 0) {
            puntos = oPricing[0].puntos;
            gv_plan = oPricing[0].nombre;
        }
        $("#Puntos").val(puntos);
        console.log(oPricing);
    });
    $("#btnComprar").on("click", function(e) {
        var param_opcion = 'listClienteBySession';
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion,
            url: 'controlador/cliente/cCliente.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(obj);
                if (obj[0] != undefined && obj[0] != null) {
                    email = obj[0].correo;
                    $("#documentoCard").val(obj[0].documento);
                    $("#titularCard").val(obj[0].nombres);
                    $("#titularCard").keyup();

                    param_opcion = 'listBeneficios';
                    $.ajax({
                        type: 'POST',
                        data: 'opcion=' + param_opcion,
                        url: 'controlador/beneficios/cBeneficios.php',
                        success: function(data) {
                            olBeneficio = JSON.parse(data);
                            console.log(olBeneficio);

                            param_opcion = 'listPricing';
                            $.ajax({
                                type: 'POST',
                                data: 'opcion=' + param_opcion,
                                url: '../../controlador/tarifas/cTarifas.php',
                                success: function(data) {
                                    olPricing = JSON.parse(data);
                                    $("#buyModal").modal("show");
                                },
                                error: function(data) {
                                    alert('Error al mostrar');
                                }
                            });
                        },
                        error: function(data) {
                            alert('Error al mostrar');
                        }
                    });
                } else {
                    $("#loginModal").modal("show");
                    bandLogin = 1;
                    //alert('Error al encontrar usuario');
                }


            },
            error: function(data) {
                alert('Error al encontrar cliente');
            }
        });

    });
    $("#btnComprarSide").on("click", function(e) {
        var param_opcion = 'listClienteBySession';
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion,
            url: 'controlador/cliente/cCliente.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(obj);
                if (obj[0] != undefined && obj[0] != null) {
                    email = obj[0].correo;
                    $("#documentoCard").val(obj[0].documento);
                    $("#titularCard").val(obj[0].nombres);
                    $("#titularCard").keyup();

                    param_opcion = 'listBeneficios';
                    $.ajax({
                        type: 'POST',
                        data: 'opcion=' + param_opcion,
                        url: 'controlador/beneficios/cBeneficios.php',
                        success: function(data) {
                            olBeneficio = JSON.parse(data);
                            console.log(olBeneficio);

                            param_opcion = 'listPricing';
                            $.ajax({
                                type: 'POST',
                                data: 'opcion=' + param_opcion,
                                url: '../../controlador/tarifas/cTarifas.php',
                                success: function(data) {
                                    olPricing = JSON.parse(data);
                                    $("#buyModal").modal("show");
                                },
                                error: function(data) {
                                    alert('Error al mostrar');
                                }
                            });
                        },
                        error: function(data) {
                            alert('Error al mostrar');
                        }
                    });
                } else {
                    $("#loginModal").modal("show");
                    bandLogin = 1;
                    //alert('Error al encontrar usuario');
                }


            },
            error: function(data) {
                alert('Error al encontrar cliente');
            }
        });
    });

    $("#collage_content_area").addClass("collage-content-center");
    $("#signature_img").on("error", function(event) {
        $(event.target).css("display", "none");
    });
    $(".toolbar-content__font").on("click", function(e) {
        // console.log(e.type);
        console.log(this.attributes.font.value)
        if (!this.classList.contains("active")) {
            let previous_selected_font = $(".toolbar-content__font")
            previous_selected_font.removeClass("active")
            this.classList.add("active")
            let inscription = document.querySelector("#t_dedicatoria");
            inscription.style.fontFamily = this.attributes.font.value;
        }
        reload_preview()

    });

    // 	TODO: trigger über js-class auslösen! (funktion, nicht styling)
    $(".toolbar-button").on("mousedown", function(e) {
        let isChecked = $("input[id='" + $(this).attr("for") + "']").prop("checked");

        if (isChecked && isOpenToolbar) {
            $("#collage-toolbar").removeClass("open");
            $("#close-toolbar").addClass("closed");
            isOpenToolbar = false;
        } else {
            $("#collage-toolbar").addClass("open");
            $("#close-toolbar").removeClass("closed");
            // real click event gets lost, so simulate it
            $("input[id='" + $(this).attr("for") + "']").prop("checked", true);
            isOpenToolbar = true;
        }
        return true;
    });


    window.onresize = function(event) {
        resize_canvas()
    };

    for (var i = 0; i < paper_align_buttons.length; i++) {
        paper_align_buttons[i].onclick = function() {
            change_page_align(this.value)
        };
    }

    reload_selected_signature_options()

    $("#firmaModal").on('hidden.bs.modal', function() {
        reset_signature_preview()
    });
});



init_drag_and_drop_zone(DragnDropRegions)
load_client_premade_signatures()
init_grid()
change_page_align("horizontal")
load_grid_template('template_4')
signature_distribution_change("preview")
    //refresh_selectable_image_events()