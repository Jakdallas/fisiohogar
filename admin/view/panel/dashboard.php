<?php 
    session_start();  
    if(!isset($_SESSION['S_Cargo']) )
    {
      if($_SESSION['S_Cargo']!='Admin'){
        header("Location:../../index.php");   
      }
    }
    else
    {
      date_default_timezone_set('America/Lima');
    }
 ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SISTEMA | Ventas</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../../assets/morris.js/morris.css">
    <!-- AdminLTE Skins -->
    <link rel="stylesheet" href="../../assets/css/_all-skins.min.css">
    <!-- Sweetalert2 -->
    <link rel="stylesheet" href="../../assets/css/sweetalert2.css">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- top menu -->
            <?php 
      require('../menus/topNavigation.php');
     ?>
            <!-- /top menu -->
        </header>

        <!-- Columna vertical -->
        <aside class="main-sidebar">
            <section class="sidebar">

                <!-- top menu -->
                <?php 
        require('../menus/topMenu.php');
      ?>
                <!-- /top menu -->

                <!-- sidebar menu -->
                <?php 
        require('../menus/sideMenu.php');
      ?>
                <!-- /sidebar menu -->
            </section>
        </aside>

        <!-- ***** Contenido de la página *****-->
        <div class="content-wrapper" style="margin-top: 50px; padding-top: 0;">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <h1>
                    Dashboard
                    <small>Gráficos de visitas.</small>
                </h1>
                <ol class="breadcrumb">

                    <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
                </ol>
                </br>
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">

                            <div class="form-group">

                                <label for="txtFechaInicio" class="col-sm-1 control-label">Del:</label>
                                <div class="col-sm-4 col-md-3 col-lg-2">
                                    <input type="date" class="form-control" id="txtFechaInicio"
                                        onchange="handler(event);">
                                </div>

                                <label for="txtFechaFin" class="col-sm-1 control-label">Al:</label>
                                <div class="col-sm-4 col-md-3 col-lg-2">
                                    <input type="date" class="form-control" id="txtFechaFin" onchange="handler(event);">
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
                <!-- /.Left col -->
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-lg-4 connectedSortable">

                        <div class="row">
                            <div class=" col-md-12">

                                <div class="box box-default">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Solicitud de citas (Estado)</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                                    class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="chart-responsive">
                                                    <canvas id="pieChart" height="150"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="chart-legend clearfix" id="legend">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-pills nav-stacked" id="estado">
                                        </ul>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>
                    <div class="col-lg-6 connectedSortable">

                        <div class="row">
                            <div class=" col-md-12">
                                <div class="box box-default">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Visitas (Contador de visitas)</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                                    class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="chart-responsive">
                                                    <div class="chart tab-pane active" id="revenue-chart"
                                                        style="position: relative; height: 300px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-pills nav-stacked" id="meses">
                                            </li>)-->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>
            </section>


            <!-- /.content -->
        </div>
        <!-- ***** Fin del contenido de la página *****-->

        <!-- footer content -->
        <?php 
    require('../menus/footerContent.php');
  ?>
        <!-- /footer content -->

        <!-- Loading (remove the following to stop the loading)-->
        <div class="overlay" id="overlay__loading">
            <div style="padding-left: calc(50% - 40px);padding-top: 25%;">
                <i class="fa fa-refresh fa-spin" style="color: white; font-size:80px"></i>
            </div>
        </div>
        <!-- end loading -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../../assets/jquery-ui/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="../../assets/raphael/raphael.min.js"></script>
    <script src="../../assets/morris.js/morris.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../assets/js/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../assets/js/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../assets/js/adminlte.min.js"></script>
    <!-- ChartJS -->
    <script src="../../assets/js/chart.js/Chart.js"></script>
    <!-- Sweetalert2 -->
    <script src="../../assets/js/sweetalert2.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../js/panel/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../assets/js/demo.js"></script>

</body>

</html>