<?php 
    session_start();  
    if(!isset($_SESSION['S_Cargo']) )
    {
      if($_SESSION['S_Cargo']!='Admin'){
        header("Location:../../index.php");   
      }
    }
    else
    {
      date_default_timezone_set('America/Lima');
    }
 ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SISTEMA | Ventas</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../../assets/morris.js/morris.css">
    <!-- AdminLTE Skins -->
    <link rel="stylesheet" href="../../assets/css/_all-skins.min.css">
    <!-- Sweetalert2 -->
    <link rel="stylesheet" href="../../assets/css/sweetalert2.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../../assets/css/select2.min.css">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- top menu -->
            <?php 
      require('../menus/topNavigation.php');
     ?>
            <!-- /top menu -->
        </header>

        <!-- Columna vertical -->
        <aside class="main-sidebar">
            <section class="sidebar">

                <!-- top menu -->
                <?php 
        require('../menus/topMenu.php');
      ?>
                <!-- /top menu -->

                <!-- sidebar menu -->
                <?php 
        require('../menus/sideMenu.php');
      ?>
                <!-- /sidebar menu -->
            </section>
        </aside>

        <!-- ***** Contenido de la página *****-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Pacientes
                    <small>Mantenedor</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a href="#">Mantenedor</a></li>
                    <li class="active">Pacientes</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">

                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">Buscar</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Antecedentes</a></li>
                                <li><a href="#tab_3" data-toggle="tab">Cita</a></li>
                                <li><a href="#tab_4" data-toggle="tab">Sesiones</a></li>
                                <li><a href="#tab_5" data-toggle="tab">Historico</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="txtDocumento" class="col-sm-2 control-label">DNI:</label>
                                            <div class="col-sm-3">
                                                <input type="number" onKeyPress="return check(event,value)"
                                                    onInput="checkLength(8,this)" class="form-control" id="txtDocumento"
                                                    placeholder="DNI">
                                            </div>
                                            <div class="col-sm-3">
                                                <button class="btn btn-primary" type="button" id="btnVerify"><i
                                                        class="fa fa-search"></i> <span>Comprobar DNI</span></button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtNombres" class="col-sm-2 control-label">Nombres:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="txtNombres"
                                                    placeholder="Nombres">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtApellidoPaterno" class="col-sm-2 control-label">Apellido
                                                Paterno:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="txtApellidoPaterno"
                                                    placeholder="Apellido Paterno">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtApellidoMaterno" class="col-sm-2 control-label">Apellido
                                                Materno:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="txtApellidoMaterno"
                                                    placeholder="Apellido Materno">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtDireccion" class="col-sm-2 control-label">Dirección:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="txtDireccion"
                                                    placeholder="Dirección">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtCorreo" class="col-sm-2 control-label">Correo:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="txtCorreo"
                                                    placeholder="Correo">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtTelefono" class="col-sm-2 control-label">Telefóno:</label>
                                            <div class="col-sm-3">
                                                <input type="number" onKeyPress="return check(event,value)"
                                                    onInput="checkLength(9,this)" class="form-control" id="txtTelefono"
                                                    placeholder="Telefono">
                                            </div>
                                        </div>
                                        <div class="box-footer" id="ftEdicion">
                                            <button type="button" class="btn btn-warning btn-md"
                                                id="btnEditarBasicInfo">Editar</button>
                                            <!--<button type="button"  class="btn btn-success btn-md" id="btnEditar">Guardar cambios</button>-->
                                            <button type="button" class="btn btn-info btn-md"
                                                id="btnGuardarBasicInfo">Guardar</button>
                                            <button type="button" class="btn btn-danger btn-md"
                                                id="btnCancelarBasicInfo">Cancelar</button>
                                        </div>

                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="txtPatologicas" class="col-sm-3 control-label">Personales
                                                    Patologicas:</label>
                                                <div class="col-sm-9">
                                                    <textarea type="textarea" maxlength="1000" rows="8"
                                                        class="form-control" id="txtPatologicas"
                                                        placeholder="Antecedentes Personales Patologicas"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="txtQuirurgicas" class="col-sm-3 control-label">Personales
                                                    Quirurgicas:</label>
                                                <div class="col-sm-9">
                                                    <textarea type="textarea" maxlength="1000" rows="8"
                                                        class="form-control" id="txtQuirurgicas"
                                                        placeholder="Antecedentes Personales Quirurgicas"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="txtAlergicas" class="col-sm-3 control-label">Personales
                                                    Alergicas:</label>
                                                <div class="col-sm-9">
                                                    <textarea type="textarea" maxlength="1000" rows="8"
                                                        class="form-control" id="txtAlergicas"
                                                        placeholder="Antecedentes Personales Alergicas"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="txtFamiliares" class="col-sm-3 control-label">Antecedentes
                                                    Familiares:</label>
                                                <div class="col-sm-9">
                                                    <textarea type="textarea" maxlength="1000" rows="8"
                                                        class="form-control" id="txtFamiliares"
                                                        placeholder="Antecedentes Familiares"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="txtEpidemiologicos"
                                                    class="col-sm-3 control-label">Epidemiologicos:</label>
                                                <div class="col-sm-9">
                                                    <textarea type="textarea" maxlength="1000" rows="8"
                                                        class="form-control" id="txtEpidemiologicos"
                                                        placeholder="Antecedentes Epidemiologicos"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="txtOtros" class="col-sm-3 control-label">Otros
                                                    Antecedentes:</label>
                                                <div class="col-sm-9">
                                                    <textarea type="textarea" maxlength="1000" rows="8"
                                                        class="form-control" id="txtOtros"
                                                        placeholder="Otros Antecedentes"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="txtExamenes" class="col-sm-3 control-label">Ex.
                                                    Auxiliares:</label>
                                                <div class="col-sm-9">
                                                    <textarea type="textarea" maxlength="1000" rows="8"
                                                        class="form-control" id="txtExamenes"
                                                        placeholder="Examenes Auxiliares"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="box-footer" id="ftEdicionAnt">
                                            <button type="button" class="btn btn-warning btn-md"
                                                id="btnEditarAntecedentes">Editar</button>
                                            <!--<button type="button"  class="btn btn-success btn-md" id="btnEditar">Guardar cambios</button>-->
                                            <button type="button" class="btn btn-info btn-md"
                                                id="btnGuardarAntecedentes">Guardar</button>
                                            <button type="button" class="btn btn-danger btn-md"
                                                id="btnCancelarAntecedentes">Cancelar</button>
                                        </div>

                                    </form>


                                </div>
                                <!-- /.tab-pane -->
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_3">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Servicio: </label>
                                            <div class="col-sm-8">
                                                <select id="cboServicio" class="form-control select2"
                                                    style="width:100%;">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtDiagnostico"
                                                class="col-sm-2 control-label">Diagnostico:</label>
                                            <div class="col-sm-8">
                                                <textarea type="textarea" maxlength="1000" class="form-control" rows="8"
                                                    id="txtDiagnostico" placeholder="Diagnostico"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtTratamiento"
                                                class="col-sm-2 control-label">Tratamiento:</label>
                                            <div class="col-sm-8">
                                                <textarea type="textarea" maxlength="1000" class="form-control" rows="8"
                                                    id="txtTratamiento" placeholder="Tratamiento"></textarea>
                                            </div>
                                        </div>
                                        <div class="box-footer" id="ftRegistro">
                                            <button type="button" class="btn btn-info btn-md"
                                                id="btnRegistrarCita">Guardar</button>
                                            <!--<button type="button"  class="btn btn-success btn-md" id="btnEditar">Guardar cambios</button>-->
                                            <button type="button" class="btn btn-danger btn-md"
                                                id="btnCancelarRegistro">Cancelar</button>
                                        </div>

                                    </form>


                                </div>
                                <!-- /.tab-pane -->
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_4">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="txtDiagnostico" class="col-sm-2 control-label">Sesion:</label>
                                            <div class="col-sm-8">
                                                <select id="cboSesion" class="form-control select2"
                                                    style="width:100%;">
                                                    <option value="0">Sesion de Terapia</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtSesion" class="col-sm-2 control-label">Detalle de
                                                Sesion:</label>
                                            <div class="col-sm-8">
                                                <textarea type="textarea" maxlength="1000" class="form-control" rows="8"
                                                    id="txtSesion" placeholder="Detalle de Sesion"></textarea>
                                            </div>
                                        </div>
                                        <div class="box-footer" id="ftSesion">
                                            <button type="button" class="btn btn-info btn-md"
                                                id="btnRegistrarSesion">Guardar</button>
                                            <!--<button type="button"  class="btn btn-success btn-md" id="btnEditar">Guardar cambios</button>-->
                                            <button type="button" class="btn btn-danger btn-md"
                                                id="btnCancelarSesion">Cancelar</button>
                                        </div>

                                    </form>


                                </div>
                                <!-- /.tab-pane -->
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_5">
                                    <div class="row" id="result">
                                        <div style="    margin-left: 49%; margin-top:50px;">
                                            <i class="fa fa-reddit" style="font-size:100px"></i>
                                            <p style="padding: 2px;"></p>
                                            <p style="min-width: 100px;"><b>Sin resultados</b></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="form-horizontal">

                                                <div class="form-group hide">

                                                    <label for="txtFechaInicio"
                                                        class="col-sm-1 control-label">Del:</label>
                                                    <div class="col-sm-4 col-md-3 col-lg-2">
                                                        <input type="date" class="form-control" id="txtFechaInicio"
                                                            onchange="handler(event);">
                                                    </div>

                                                    <label for="txtFechaFin" class="col-sm-1 control-label">Al:</label>
                                                    <div class="col-sm-4 col-md-3 col-lg-2">
                                                        <input type="date" class="form-control" id="txtFechaFin"
                                                            onchange="handler(event);">
                                                    </div>

                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- The time line -->
                                            <ul class="timeline" id="UlTimeLine">

                                            </ul>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>
                    <!-- /.col -->
                </div>

            </section>

            <!-- /.content -->
        </div>
        <!-- ***** Fin del contenido de la página *****-->
        <!-- Modal -->
        <div class="modal fade" id="puntosModalCenter" tabindex="-1" role="dialog" aria-labelledby="puntosModalCenter"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Aumentar Puntos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="txtPtosActuales" class="col-sm-4 control-label">Puntos actuales:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="txtPtosActuales"
                                    placeholder="Puntos actuales" disabled>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="txtCantidad" class="col-sm-4 control-label">Cantidad a aumentar:</label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" id="txtCantidad"
                                    placeholder="Cantidad a aumentar">
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnAumentar">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer content -->
        <?php 
    require('../menus/footerContent.php');
  ?>
        <!-- /footer content -->
        <!-- Loading (remove the following to stop the loading)-->
        <div class="overlay" id="overlay__loading">
            <div style="padding-left: calc(50% - 40px);padding-top: 25%;">
                <i class="fa fa-refresh fa-spin" style="color: white; font-size:80px"></i>
            </div>
        </div>
        <!-- end loading -->

    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../assets/js/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../assets/js/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../assets/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../assets/js/demo.js"></script>
    <!-- DataTables -->
    <script src="../../assets/js/jquery.dataTables.min.js"></script>
    <script src="../../assets/js/dataTables.bootstrap.min.js"></script>
    <!-- Sweetalert2 -->
    <script src="../../assets/js/sweetalert2.js"></script>
    <!-- Select2 -->
    <script src="../../assets/js/select2.full.min.js"></script>
    <!-- Capa JS -->
    <script src="../js/paciente/paciente.js"></script>


</body>

</html>