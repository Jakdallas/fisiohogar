<?php 
    session_start();  
    if(!isset($_SESSION['S_Cargo']) )
    {
      if($_SESSION['S_Cargo']!='Admin'){
        header("Location:../../index.php");   
      }
    }
    else
    {
      date_default_timezone_set('America/Lima');
    }
 ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SISTEMA | Mantenedor</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
    <!-- AdminLTE Skins -->
    <link rel="stylesheet" href="../../assets/css/_all-skins.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.min.css">
    <!-- Main style -->
    <link rel="stylesheet" href="../../assets/css/min.css">
    <!-- Sweetalert2 -->
    <link rel="stylesheet" href="../../assets/css/sweetalert2.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../../assets/css/select2.min.css">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <!-- Loading (remove the following to stop the loading)-->
        <div class="overlay" id="overlay__loading">
            <div style="padding-left: calc(50% - 40px);padding-top: 25%;">
                <i class="fa fa-refresh fa-spin" style="color: white; font-size:80px"></i>
            </div>
        </div>
        <!-- end loading -->
        <header class="main-header">
            <!-- top menu -->
            <?php 
      require('../menus/topNavigation.php');
     ?>
            <!-- /top menu -->
        </header>

        <!-- Columna vertical -->
        <aside class="main-sidebar">
            <section class="sidebar">

                <!-- top menu -->
                <?php 
        require('../menus/topMenu.php');
      ?>
                <!-- /top menu -->

                <!-- sidebar menu -->
                <?php 
        require('../menus/sideMenu.php');
      ?>
                <!-- /sidebar menu -->
            </section>
        </aside>

        <!-- ***** Contenido de la página *****-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Usuarios
                    <small>Mantenedor</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a href="#">Mantenedor</a></li>
                    <li class="active">Usuarios</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">

                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">Consultar</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Nuevo</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <table id="datatable-Usuario" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nº</th>
                                                <th>Usuario</th>
                                                <th>Documento</th>
                                                <th>Empleado</th>
                                                <th>Cargo</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodytable-Usuario">

                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="Usuario" class="col-sm-2 control-label">Usuario:</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="Usuario"
                                                    placeholder="usuario">
                                            </div>
                                            <label class="col-sm-2 control-label">Empleado</label>
                                            <div class="col-sm-3">
                                                <select id="cboCargo" class="form-control select2" style="width:100%;">

                                                </select>
                                            </div>
                                        </div>

                                        <div id="" class="form-group">
                                            <label for="Contra" class="col-sm-2 control-label">Contraseña:</label>
                                            <div class="col-sm-3">
                                                <input type="password" class="form-control" id="Contra"
                                                    placeholder="Contraseña">
                                            </div>
                                            <label for="txtNombres" class="col-sm-2 control-label">Nombres:</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="txtNombres"
                                                    placeholder="Nombres" disabled>
                                            </div>
                                        </div>

                                        <div id="showContra" class="form-group">
                                            <label for="ContraVerifica" class="col-sm-2 control-label">Repetir
                                                Contraseña:</label>
                                            <div class="col-sm-3">
                                                <input type="password" class="form-control" id="ContraVerifica"
                                                    placeholder="Repita Contraseña">
                                            </div>
                                            <label for="txtDNI" class="col-sm-2 control-label">DNI:</label>
                                            <div class="col-sm-3">
                                                <input type="number" class="form-control" id="txtDNI" placeholder="DNI"
                                                    disabled>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <button type="button" class="btn btn-info btn-md"
                                                id="btnRegistrar">Registrar</button>
                                            <button type="button" class="btn btn-success btn-md" id="btnEditar">Guardar
                                                cambios</button>
                                            <button type="button" class="btn btn-danger btn-md"
                                                id="btnCancelar">Cancelar</button>
                                            <p style="float: right; color: gray;">La contraseña por defecto es: FisioHog@r2022</p>
                                        </div>
                                    </form>

                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </section>

            <!-- /.content -->
        </div>
        <!-- ***** Fin del contenido de la página *****-->
        <!-- Modal -->
        <div class="modal fade" id="puntosModalCenter" tabindex="-1" role="dialog" aria-labelledby="puntosModalCenter"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Actualizar Credenciales de Usuario</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="txtNombres" class="col-sm-4 control-label">Empleado:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="txtNombresUp" placeholder="Nombres" disabled>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="UsuarioUp" class="col-sm-4 control-label">Usuario:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="UsuarioUp" placeholder="Usuario">
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnAumentar">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <!-- footer content -->
        <?php 
    require('../menus/footerContent.php');
  ?>
        <!-- /footer content -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../../assets/jquery-ui/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../assets/js/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../assets/js/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../assets/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../assets/js/demo.js"></script>
    <!-- DataTables -->
    <script src="../../assets/js/jquery.dataTables.min.js"></script>
    <script src="../../assets/js/dataTables.bootstrap.min.js"></script>
    <!-- Sweetalert2 -->
    <script src="../../assets/js/sweetalert2.js"></script>
    <!-- Select2 -->
    <script src="../../assets/js/select2.full.min.js"></script>
    <!-- Capa JS -->
    <script src="../js/seguridad/usuario.js"></script>


</body>

</html>