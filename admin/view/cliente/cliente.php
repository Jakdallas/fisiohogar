<?php 
    session_start();  
    if(!isset($_SESSION['S_Cargo']) )
    {
      if($_SESSION['S_Cargo']!='Admin'){
        header("Location:../../index.php");   
      }
    }
    else
    {
      date_default_timezone_set('America/Lima');
    }
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISTEMA | Mantenedor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../assets/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
  <!-- AdminLTE Skins -->
  <link rel="stylesheet" href="../../assets/css/_all-skins.min.css">
   <!-- DataTables -->
   <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.min.css">
   <!-- Main style -->
  <link rel="stylesheet" href="../../assets/css/min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../assets/css/select2.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- top menu -->
     <?php 
      require('../menus/topNavigation.php');
     ?>
    <!-- /top menu -->
  </header>

  <!-- Columna vertical -->
  <aside class="main-sidebar">
    <section class="sidebar">

      <!-- top menu -->
      <?php 
        require('../menus/topMenu.php');
      ?>
      <!-- /top menu -->
      
      <!-- sidebar menu -->
      <?php 
        require('../menus/sideMenu.php');
      ?>
      <!-- /sidebar menu -->
    </section>
  </aside>

  <!-- ***** Contenido de la página *****-->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Clientes
        <small>Mantenedor</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Mantenedor</a></li>
        <li class="active">Clientes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    
<div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Consultar</a></li>
              <!--<li><a href="#tab_2" data-toggle="tab">Nuevo</a></li>-->
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
              <table id="datatable-Cliente" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Documento</th>
                  <th>Nombres</th>
                  <th>CMP</th>
                  <th>Modelo Ecografo</th>
                  <th>Marca Ecografo</th>
                  <th>Ciudad</th>
                  <th>Correo</th>
                  <th>Telefóno</th>
                  <th>Ptos</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody id="bodytable-Cliente">
                 
                </tbody>
              </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2" >
              <form class="form-horizontal">  
                <div class="form-group">
                  <label for="txtRazonSocial" class="col-sm-2 control-label">Razon Social:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="txtRazonSocial" placeholder="Razon Social">
                  </div>
                </div>
                <div class="form-group">
                  <label for="txtRUC" class="col-sm-2 control-label">RUC:</label>
                  <div class="col-sm-3">
                    <input type="number" onKeyPress="return check(event,value)" onInput="checkLength(11,this)" class="form-control" id="txtRUC" placeholder="RUC" >
                  </div>
                  <label class="col-sm-2 control-label">Zona</label>
                  <div class="col-sm-3">
                     <select id="cboZona" class="form-control select2" style="width:100%;">
                 
                  </select>
                  </div> 
                </div>
                <div class="form-group">
                  <label for="txtResponsable" class="col-sm-2 control-label">Responsable:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="txtResponsable" placeholder="Responsable">
                  </div>  
                </div>
                <div class="form-group">
                  <label for="txtDNIResponsable" class="col-sm-2 control-label">DNI:</label>
                  <div class="col-sm-3">
                    <input type="number" onKeyPress="return check(event,value)" onInput="checkLength(8,this)" class="form-control" id="txtDNIResponsable" placeholder="DNI">
                  </div> 
                </div>
                <div class="box-footer">
                <button type="button"  class="btn btn-info btn-md" id="btnRegistrar">Registrar</button>
                <button type="button"  class="btn btn-success btn-md" id="btnEditar">Guardar cambios</button>
                <button type="button"  class="btn btn-danger btn-md" id="btnCancelar">Cancelar</button>
                </div> 
                 
            </form>
            
            
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
</div>

    </section>
    
    <!-- /.content -->
  </div>
  <!-- ***** Fin del contenido de la página *****-->
<!-- Modal -->
<div class="modal fade" id="puntosModalCenter" tabindex="-1" role="dialog" aria-labelledby="puntosModalCenter" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Aumentar Puntos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <div class="form-group">
                  <label for="txtPtosActuales" class="col-sm-4 control-label">Puntos actuales:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="txtPtosActuales" placeholder="Puntos actuales" disabled>
                  </div>
                </div>
                <br>
                <br>
                <div class="form-group">
                  <label for="txtCantidad" class="col-sm-4 control-label">Cantidad a aumentar:</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" id="txtCantidad" placeholder="Cantidad a aumentar">
                  </div>
                </div>
                <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btnAumentar" >Guardar</button>
      </div>
    </div>
  </div>
</div>
  <!-- footer content -->
  <?php 
    require('../menus/footerContent.php');
  ?>
  <!-- /footer content -->

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../assets/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../assets/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../assets/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../assets/js/demo.js"></script>
<!-- DataTables -->
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../../assets/js/select2.full.min.js"></script>
<!-- Capa JS -->
<script src="../js/cliente/cliente.js"></script>


</body>
</html>
