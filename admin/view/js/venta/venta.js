//var a espanish datatable
$("#datatable-Tarifas").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});


//VARIABLES 
var txtNombre = document.getElementById('txtNombre');
var txtPrecio = document.getElementById('txtPrecio');
var txtPuntos = document.getElementById('txtPuntos');
var txtOrden = document.getElementById('txtOrden');

var txtNombreUp = document.getElementById('txtNombreUp');
var txtPrecioUp = document.getElementById('txtPrecioUp');
var txtPuntosUp = document.getElementById('txtPuntosUp');
var txtOrdenUp = document.getElementById('txtOrdenUp');

var btnRegistrar = document.getElementById('btnRegistrar');
var btnCancelar = document.getElementById('btnCancelar');
var btnActualizar = document.getElementById('btnActualizar');
var idTarifa;

//funciones


function obtenerID(id, nombre, precio, puntos, orden) {
    idTarifa = id;
    txtNombreUp.value = nombre;
    txtPrecioUp.value = precio;
    txtPuntosUp.value = puntos;
    txtOrdenUp.value = orden;
    //$('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function listarPricing() {

    var param_opcion = 'listVenta';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/venta/cVenta.php',
        success: function(data) {
            var table = $("#datatable-Tarifas").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(cont);
                array.push(e.nombres);
                array.push(e.cmp);
                array.push(e.ecografo_modelo);
                array.push(e.ecografo_marca);
                array.push(e.monto);
                array.push(e.puntos);
                array.push(e.fecha);
                array.push("<button data-toggle='modal' data-target='#updateModalCenter' class='btn btn-xs btn-warning' onclick='obtenerID(" + e.idventa + "," + '"' + e.nombres + '"' + "," + '"' + e.monto + '"' + "," + '"' + e.puntos + '"' + "," + '"' + e.fecha + '"' + ")'>Editar</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminarTarifa(" + e.idventa + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function editar() {

    var opcion = 'updatePricing';
    var Nombre = txtNombreUp.value;
    var Precio = txtPrecioUp.value;
    var Puntos = txtPuntosUp.value;
    var Orden = txtOrdenUp.value;
    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion +
            '&idPrecio=' + idTarifa +
            '&Nombre=' + Nombre +
            '&Precio=' + Precio +
            '&Puntos=' + Puntos +
            '&Orden=' + Orden,
        url: '../../controlador/tarifas/cTarifas.php',
        success: function(data) {
            console.log(data);
            var obj = JSON.parse(data);
            listarClientes();
            if (obj == 1) {
                alert('Plan actualizado con exito.');
                cancelar();
                $("#updateModalCenter").modal("hide");
            } else {
                alert('No se realizaron modificaciones.');
            }
        },
        error: function(data) {
            alert('Error al editar');
        }
    });
}

function registrarTarifa() {

    var param_opcion = 'insertPricing';
    var Nombre = txtNombre.value;
    var Precio = txtPrecio.value;
    var Puntos = txtPuntos.value;
    var Orden = txtOrden.value;
    $("btnRegistrar").prop('disabled', true);
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&Nombre=' + Nombre +
            '&Precio=' + Precio +
            '&Puntos=' + Puntos +
            '&Orden=' + Orden,
        url: '../../controlador/tarifas/cTarifas.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj == 1) {
                listarClientes();
                cancelar();
                alert("Registro exitoso");
            } else {
                alert('Error al agregar puntos');
            }
            $("btnRegistrar").prop('disabled', false);
        },
        error: function(data) {
            alert('Error al agregar puntos');
            $("btnRegistrar").prop('disabled', false);
        }
    });


}

function eliminarTarifa(idprecio) {

    var param_opcion = 'deletePricing';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idPrecio=' + idprecio,
            url: '../../controlador/tarifas/cTarifas.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                if (obj == 1) {

                    listarClientes();

                } else {
                    alert('Error al eliminar');
                }

            },
            error: function(data) {
                alert('Error al eliminar');
            }
        });
    }

}

function cancelar() {
    txtNombre.value = "";
    txtPrecio.value = "";
    txtPuntos.value = "";
    txtOrden.value = "";

}

function alCargarDocumento() {
    listarPricing();
    btnRegistrar.addEventListener("click", registrarTarifa);
    btnActualizar.addEventListener("click", editar);
    btnCancelar.addEventListener("click", cancelar);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);