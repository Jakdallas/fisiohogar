//var a espanish datatable
$("#datatable-Cliente").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});


//VARIABLES 
var txtCantidad = document.getElementById('txtCantidad');
var btnAumentar = document.getElementById('btnAumentar');
var idCliente;
var idClienteAumentar;


var txtPtosActuales = document.getElementById('txtPtosActuales');
//funciones



function obtenerID(id, puntos) {
    idCliente = id;
    idClienteAumentar = id;
    txtPtosActuales.value = puntos;
    //$('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function listarClientes() {

    var param_opcion = 'listCliente';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var table = $("#datatable-Cliente").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(e.documento);
                array.push(e.nombres);
                array.push(e.cmp);
                array.push(e.ecografo_modelo);
                array.push(e.ecografo_marca);
                array.push(e.ciudad);
                array.push(e.correo);
                array.push(e.telefono);
                array.push(e.puntos);
                array.push("<button data-toggle='modal' data-target='#puntosModalCenter' class='btn btn-xs btn-warning' onclick='obtenerID(" + e.idCliente + "," + '"' + e.puntos + '"' + ")'>Puntos</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminarCliente(" + e.idCliente + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function editar() {

    var opcion = 'updateCliente';
    var RazonSocial = txtRazonSocial.value;
    var Responsable = txtResponsable.value;
    var RUC = txtRUC.value;
    var DNIResponsable = txtDNIResponsable.value;
    var idZona = cboZona.value;
    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion +
            '&idCliente=' + idCliente +
            '&RazonSocial=' + RazonSocial +
            '&Responsable=' + Responsable +
            '&RUC=' + RUC +
            '&DNIResponsable=' + DNIResponsable +
            '&idZona=' + idZona,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            console.log(data);
            var obj = JSON.parse(data);
            listarClientes();
            if (obj == 1) {
                alert('Cliente editado con exito.');
                cancelar();

            } else if (obj == 4) {
                alert('RUC ya existe.');
            } else {
                alert('No se realizaron modificaciones.');
            }
        },
        error: function(data) {
            alert('Error al editar');
        }
    });
}

function addPuntos() {
    var puntos = txtCantidad.value;
    var param_opcion = 'addPuntos';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&idClienteM=' + idClienteAumentar +
            '&Puntos=' + puntos,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj == 1) {

                listarClientes();
                txtCantidad.value = "";
            } else {
                alert('Error al agregar puntos');
            }
            $("#puntosModalCenter").modal("hide");
        },
        error: function(data) {
            alert('Error al agregar puntos');
        }
    });
}

function eliminarCliente(idCliente) {

    var param_opcion = 'deleteCliente';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idClienteM=' + idCliente,
            url: '../../controlador/cliente/cCliente.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                if (obj == 1) {

                    listarClientes();

                } else {
                    alert('Error al eliminar');
                }

            },
            error: function(data) {
                alert('Error al eliminar');
            }
        });
    }

}

function check(e, value) {
    //Check Charater
    var unicode = e.charCode ? e.charCode : e.keyCode;
    if (value.indexOf(".") != -1)
        if (unicode == 46) return false;
    if (unicode != 8)
        if ((unicode < 48 || unicode > 57) && unicode != 46) return false;
}

function checkLength(len, ele) {
    var fieldLength = ele.value.length;
    if (fieldLength <= len) {
        return true;
    } else {
        var str = ele.value;
        str = str.substring(0, str.length - 1);
        ele.value = str;
    }
}

function llenarcboServicios() {

    var param_opcion = 'listSteps';
    $("#cboServicio").empty();
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/steps/cSteps.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            $("#cboServicio").html('<option value="0" id="cboFirst" >Seleccionar Servicio...</option>');
            obj.forEach(function(e) {
                $("#cboServicio").append('<option value="' + e.idservicio + '">' + e.descripcion + '</option>');
            });
            $("#cboServicio").select2();
        },
        error: function(data) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Error al cargar servicios!'
            });
        }
    });
}

function lockFieldsPaciente() {
    $("#btnEditarBasicInfo").removeAttr("disabled");
    $('#txtNombres').attr("disabled", "disabled");
    $('#txtApellidoPaterno').attr("disabled", "disabled");
    $('#txtApellidoMaterno').attr("disabled", "disabled");
    $('#txtDireccion').attr("disabled", "disabled");
    $('#txtCorreo').attr("disabled", "disabled");
    $('#txtTelefono').attr("disabled", "disabled");
    $('#btnGuardarBasicInfo').attr("disabled", "disabled");
    $('#btnCancelarBasicInfo').attr("disabled", "disabled");
}

function unlockFieldsPaciente() {
    $('#btnEditarBasicInfo').attr("disabled", "disabled");
    //$("#txtNombres").removeAttr("disabled");
    //$("#txtApellidoPaterno").removeAttr("disabled");
    //$("#txtApellidoMaterno").removeAttr("disabled");
    $("#txtDireccion").removeAttr("disabled");
    $("#txtCorreo").removeAttr("disabled");
    $("#txtTelefono").removeAttr("disabled");
    $("#btnGuardarBasicInfo").removeAttr("disabled");
    $("#btnCancelarBasicInfo").removeAttr("disabled");
}

function lockFieldsAntecedentes() {
    $("#btnEditarAntecedentes").removeAttr("disabled");
    $('#txtPatologicas').attr("disabled", "disabled");
    $('#txtQuirurgicas').attr("disabled", "disabled");
    $('#txtAlergicas').attr("disabled", "disabled");
    $('#txtFamiliares').attr("disabled", "disabled");
    $('#txtEpidemiologicos').attr("disabled", "disabled");
    $('#txtOtros').attr("disabled", "disabled");
    $('#txtExamenes').attr("disabled", "disabled");
    $('#btnGuardarAntecedentes').attr("disabled", "disabled");
    $('#btnCancelarAntecedentes').attr("disabled", "disabled");
}

function unlockFieldsAntecedentes() {
    $('#btnEditarAntecedentes').attr("disabled", "disabled");
    $("#txtPatologicas").removeAttr("disabled");
    $("#txtQuirurgicas").removeAttr("disabled");
    $("#txtAlergicas").removeAttr("disabled");
    $("#txtFamiliares").removeAttr("disabled");
    $("#txtEpidemiologicos").removeAttr("disabled");
    $("#txtOtros").removeAttr("disabled");
    $("#txtExamenes").removeAttr("disabled");
    $("#btnGuardarAntecedentes").removeAttr("disabled");
    $("#btnCancelarAntecedentes").removeAttr("disabled");
}

function clearFields() {
    $("#txtPatologicas").val("");
    $("#txtQuirurgicas").val("");
    $("#txtAlergicas").val("");
    $("#txtFamiliares").val("");
    $("#txtEpidemiologicos").val("");
    $("#txtOtros").val("");
    $("#txtDireccion").val("");
    $("#txtCorreo").val("");
    $("#txtTelefono").val("");
    $("#txtNombres").val("");
    $("#txtApellidoPaterno").val("");
    $("#txtApellidoMaterno").val("");
}

function alCargarDocumento() {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var dni = "";
    if (dd < 10) { dd = '0' + dd; }
    if (mm < 10) { mm = '0' + mm; }

    today = yyyy + '-' + mm + '-' + dd;
    beginYear = yyyy + '-' + '01' + '-' + '01';

    $('#txtFechaInicio').val(beginYear);
    $('#txtFechaFin').val(today);
    //handler();
    //$('#txtFechaInicio').change(function() { handler(); });
    //$('#txtFechaFin').change(function() { handler(); });
    llenarcboServicios();
    lockFieldsPaciente();
    lockFieldsAntecedentes();
    //listarClientes();
    $("#btnEditarBasicInfo").click(function() {
        unlockFieldsPaciente();
    });
    $("#btnCancelarBasicInfo").click(function() {
        lockFieldsPaciente();
    });
    $("#btnEditarAntecedentes").click(function() {
        unlockFieldsAntecedentes();
    });
    $("#btnCancelarAntecedentes").click(function() {
        lockFieldsAntecedentes();
    });
    $("#btnVerify").click(function() {
        let Documento = $("#txtDocumento").val();
        dni = Documento;
        let param_opcion = 'searchDNI';
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&Documento=' + Documento,
            url: '../../controlador/paciente/cPaciente.php',
            beforeSend: function() {
                $("#overlay__loading").show();
                clearFields();
            },
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(obj);
                $('#txtNombres').val(obj.nombres);
                $('#txtApellidoPaterno').val(obj.apellidoPaterno);
                $('#txtApellidoMaterno').val(obj.apellidoMaterno);
                var formData = new FormData();
                formData.append("Documento", Documento);
                formData.append("Nombres", obj.nombres);
                formData.append("apellidoPaterno", obj.apellidoPaterno);
                formData.append("apellidoMaterno", obj.apellidoMaterno);
                formData.append("opcion", 'getCliente');
                $.ajax({
                    type: 'POST',
                    data: formData,
                    url: '../../controlador/paciente/cPaciente.php',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        //$("#overlay__loading").show();
                    },
                    success: function(data) {
                        var olbj = JSON.parse(data);
                        lockFieldsAntecedentes();
                        console.log(obj);
                        olbj.forEach(function(obj) {
                            if (obj.patologicas != null)
                                $('#txtPatologicas').val(obj.patologicas);
                            if (obj.quirurgicas != null)
                                $('#txtQuirurgicas').val(obj.quirurgicas);
                            if (obj.alergicas != null)
                                $('#txtAlergicas').val(obj.alergicas);
                            if (obj.familiares != null)
                                $('#txtFamiliares').val(obj.familiares);
                            if (obj.epidemiologicos != null)
                                $('#txtEpidemiologicos').val(obj.epidemiologicos);
                            if (obj.otros != null)
                                $('#txtOtros').val(obj.otros);
                            if (obj.examenes != null)
                                $('#txtExamenes').val(obj.examenes);
                            if (obj.correo != null)
                                $('#txtCorreo').val(obj.correo);
                            if (obj.telefono != null)
                                $('#txtTelefono').val(obj.telefono);
                            if (obj.email != null)
                                $('#txtCorreo').val(obj.email);
                            if (obj.telefono2 != null)
                                $('#txtTelefono').val(obj.telefono2);
                            if (obj.direccion != null)
                                $('#txtDireccion').val(obj.direccion);
                        });

                        var formData1 = new FormData();
                        formData1.append("Documento", Documento);
                        formData1.append("opcion", 'listCita');
                        $.ajax({
                            type: 'POST',
                            data: formData1,
                            url: '../../controlador/paciente/cPaciente.php',
                            contentType: false,
                            cache: false,
                            processData: false,
                            beforeSend: function() {
                                //$('#fupForm').css("opacity", ".5");
                                $("#result").show();
                            },
                            success: function(data) {
                                $("#UlTimeLine").empty();
                                var obj = JSON.parse(data);
                                var olMeses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
                                console.log(obj);
                                if (obj.length > 0)
                                    $("#result").hide();

                                obj.forEach(function(element) {
                                    let body = "";
                                    if (element.idservicio == "0") {
                                        element.descripcion = "Sesión de Terapia";
                                        body = `<div class="timeline-body">
                                                    <b>Detalle de Sesión: </b> ${element.diagnostico}
                                                </div>`;
                                    } else {
                                        body = `<div class="timeline-body">
                                                    <b>Diagnostico: </b> ${element.diagnostico}
                                                </div>
                                                <div class="timeline-body">
                                                    <b>Tratamiento: </b>${element.tratamiento}
                                                </div>`;
                                    }
                                    let time = element.fecha_inicio.split(" ");
                                    let label = `<li class="time-label">
                                                    <span class="bg-aqua ">
                                                        ${element.DAY} ${olMeses[element.MONTH]} ${element.YEAR}
                                                    </span>
                                                </li>`;
                                    $("#UlTimeLine").append(label);
                                    let content = `<li>
                                                        <i class="fa fa-user bg-blue"></i>

                                                        <div class="timeline-item">
                                                            <span class="time"><i class="fa fa-clock-o"></i>  ${time[1]}</span>

                                                            <h3 class="timeline-header"><a href="#">${element.descripcion}</a> </h3>

                                                            ${body}
                                                            <!--<div class="timeline-footer">
                                                                <a class="btn btn-primary btn-xs">Ver más</a>
                                                            </div>-->
                                                        </div>
                                                    </li>`;
                                    $("#UlTimeLine").append(content);
                                });
                                let last = `<li>
                                    <i class="fa fa-clock-o bg-gray"></i>
                                </li>`;

                                $("#UlTimeLine").append(last);

                                $("#overlay__loading").hide();
                                //$("#puntosModalCenter").modal("hide");
                                $("#btnRegistrar").removeAttr("disabled");
                            },
                            error: function(data) {
                                $("#overlay__loading").hide();
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error',
                                    text: 'Error al registrar!'
                                });
                            }
                        });
                    },
                    error: function(data) {
                        $("#overlay__loading").hide();
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Error!'
                        });
                    }
                });
                //$("#overlay__loading").hide();
            },
            error: function(data) {
                $("#overlay__loading").hide();
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error al cargar información!'
                });
            }
        });
    });
    $("#btnRegistrarCita").click(function() {
        var Tratamiento = $("#txtTratamiento").val();
        var Diagnostico = $("#txtDiagnostico").val();
        var Servicio = $("#cboServicio").val();
        var Documento = $("#txtDocumento").val();
        if (Documento == null || Documento == undefined || Documento == "" || Documento.length != 8) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Ingrese un DNI válido!'
            });
            return
        }
        var formData = new FormData();
        formData.append("Documento", Documento);
        formData.append("Servicio", Servicio);
        formData.append("Diagnostico", Diagnostico);
        formData.append("Tratamiento", Tratamiento);
        formData.append("opcion", 'insertCita');
        $.ajax({
            type: 'POST',
            data: formData,
            url: '../../controlador/paciente/cPaciente.php',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $("#overlay__loading").show();
            },
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                $("#overlay__loading").hide();
                if (obj == 1) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Ok',
                        text: 'Registro exitoso!'
                    });
                    sonido.play();
                }
                var formData1 = new FormData();
                formData1.append("Documento", Documento);
                formData1.append("opcion", 'listCita');
                $.ajax({
                    type: 'POST',
                    data: formData1,
                    url: '../../controlador/paciente/cPaciente.php',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        //$('#fupForm').css("opacity", ".5");
                        $("#result").show();
                    },
                    success: function(data) {
                        $("#UlTimeLine").empty();
                        var obj = JSON.parse(data);
                        var olMeses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
                        console.log(obj);
                        if (obj.length > 0)
                            $("#result").hide();

                        $("#txtTratamiento").val("");
                        $("#txtDiagnostico").val("");

                        obj.forEach(function(element) {
                            let body = "";
                            if (element.idservicio == "0") {
                                element.descripcion = "Sesión de Terapia";
                                body = `<div class="timeline-body">
                                                    <b>Detalle de Sesión: </b> ${element.diagnostico}
                                                </div>`;
                            } else {
                                body = `<div class="timeline-body">
                                                    <b>Diagnostico: </b> ${element.diagnostico}
                                                </div>
                                                <div class="timeline-body">
                                                    <b>Tratamiento: </b>${element.tratamiento}
                                                </div>`;
                            }
                            let time = element.fecha_inicio.split(" ");
                            let label = `<li class="time-label">
                                                    <span class="bg-aqua ">
                                                        ${element.DAY} ${olMeses[element.MONTH]} ${element.YEAR}
                                                    </span>
                                                </li>`;
                            $("#UlTimeLine").append(label);
                            let content = `<li>
                                                        <i class="fa fa-user bg-blue"></i>

                                                        <div class="timeline-item">
                                                            <span class="time"><i class="fa fa-clock-o"></i>  ${time[1]}</span>

                                                            <h3 class="timeline-header"><a href="#">${element.descripcion}</a> </h3>

                                                            ${body}
                                                            <!--<div class="timeline-footer">
                                                                <a class="btn btn-primary btn-xs">Ver más</a>
                                                            </div>-->
                                                        </div>
                                                    </li>`;
                            $("#UlTimeLine").append(content);
                        });
                        let last = `<li>
                                    <i class="fa fa-clock-o bg-gray"></i>
                                </li>`;

                        $("#UlTimeLine").append(last);

                        $("#overlay__loading").hide();
                        //$("#puntosModalCenter").modal("hide");
                        $("#btnRegistrar").removeAttr("disabled");
                    },
                    error: function(data) {
                        $("#overlay__loading").hide();
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Error al registrar!'
                        });
                    }
                });
            },
            error: function(data) {
                $("#overlay__loading").hide();
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error!'
                });
            }
        });
    });
    $("#btnGuardarAntecedentes").click(function() {
        var patologicas = $("#txtPatologicas").val();
        var quirurgicas = $("#txtQuirurgicas").val();
        var alergicas = $("#txtAlergicas").val();
        var familiares = $("#txtFamiliares").val();
        var epidemiologicos = $("#txtEpidemiologicos").val();
        var otros = $("#txtOtros").val();
        var examenes = $("#txtExamenes").val();
        var Documento = dni; //$("#txtDocumento").val();
        if (Documento == null || Documento == undefined || Documento == "" || Documento.length != 8) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Ingrese un DNI válido!'
            });
            return
        }
        var formData = new FormData();
        formData.append("Patologicas", patologicas);
        formData.append("Quirurgicas", quirurgicas);
        formData.append("Alergicas", alergicas);
        formData.append("Familiares", familiares);
        formData.append("Epidemiologicos", epidemiologicos);
        formData.append("Otros", otros);
        formData.append("Examenes", examenes);
        formData.append("Documento", Documento);
        formData.append("opcion", 'updateAntecedentes');
        $.ajax({
            type: 'POST',
            data: formData,
            url: '../../controlador/paciente/cPaciente.php',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $("#overlay__loading").show();
            },
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                lockFieldsAntecedentes();
                $("#overlay__loading").hide();
                if (obj == 1) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Ok',
                        text: 'Registro exitoso!'
                    });
                    sonido.play();
                }
            },
            error: function(data) {
                $("#overlay__loading").hide();
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error!'
                });
            }
        });
    });

    $("#btnGuardarBasicInfo").click(function() {
        var Direccion = $("#txtDireccion").val();
        var Correo = $("#txtCorreo").val();
        var Telefono = $("#txtTelefono").val();
        var Documento = dni; //$("#txtDocumento").val();
        if (Documento == null || Documento == undefined || Documento == "" || Documento.length != 8) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Ingrese un DNI válido!'
            });
            return
        }
        var formData = new FormData();
        formData.append("Direccion", Direccion);
        formData.append("Correo", Correo);
        formData.append("Telefono", Telefono);
        formData.append("Documento", Documento);
        formData.append("opcion", 'updateCliente');
        $.ajax({
            type: 'POST',
            data: formData,
            url: '../../controlador/paciente/cPaciente.php',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $("#overlay__loading").show();
            },
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                lockFieldsAntecedentes();
                $("#overlay__loading").hide();
                if (obj == 1) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Ok',
                        text: 'Registro exitoso!'
                    });
                    sonido.play();
                }
            },
            error: function(data) {
                $("#overlay__loading").hide();
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error!'
                });
            }
        });
    });

    $("#btnRegistrarSesion").click(function() {
        var Sesion = $("#txtSesion").val();
        var Servicio = $("#cboSesion").val();
        var Documento = $("#txtDocumento").val();
        if (Documento == null || Documento == undefined || Documento == "" || Documento.length != 8) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Ingrese un DNI válido!'
            });
            return
        }
        var formData = new FormData();
        formData.append("Documento", Documento);
        formData.append("Servicio", Servicio);
        formData.append("Diagnostico", Sesion);
        formData.append("opcion", 'insertCita');
        $.ajax({
            type: 'POST',
            data: formData,
            url: '../../controlador/paciente/cPaciente.php',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $("#overlay__loading").show();
            },
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                $("#overlay__loading").hide();
                if (obj == 1) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Ok',
                        text: 'Registro exitoso!'
                    });
                    sonido.play();
                }
                var formData1 = new FormData();
                formData1.append("Documento", Documento);
                formData1.append("opcion", 'listCita');
                $.ajax({
                    type: 'POST',
                    data: formData1,
                    url: '../../controlador/paciente/cPaciente.php',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        //$('#fupForm').css("opacity", ".5");
                        $("#result").show();
                    },
                    success: function(data) {
                        $("#UlTimeLine").empty();
                        var obj = JSON.parse(data);
                        var olMeses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
                        console.log(obj);
                        if (obj.length > 0)
                            $("#result").hide();

                        $("#txtSesion").val("");

                        obj.forEach(function(element) {
                            let body = "";
                            if (element.idservicio == "0") {
                                element.descripcion = "Sesión de Terapia";
                                body = `<div class="timeline-body">
                                                    <b>Detalle de Sesión: </b> ${element.diagnostico}
                                                </div>`;
                            } else {
                                body = `<div class="timeline-body">
                                                    <b>Diagnostico: </b> ${element.diagnostico}
                                                </div>
                                                <div class="timeline-body">
                                                    <b>Tratamiento: </b>${element.tratamiento}
                                                </div>`;
                            }
                            let time = element.fecha_inicio.split(" ");
                            let label = `<li class="time-label">
                                                    <span class="bg-aqua ">
                                                        ${element.DAY} ${olMeses[element.MONTH]} ${element.YEAR}
                                                    </span>
                                                </li>`;
                            $("#UlTimeLine").append(label);
                            let content = `<li>
                                                        <i class="fa fa-user bg-blue"></i>

                                                        <div class="timeline-item">
                                                            <span class="time"><i class="fa fa-clock-o"></i>  ${time[1]}</span>

                                                            <h3 class="timeline-header"><a href="#">${element.descripcion}</a> </h3>

                                                            ${body}
                                                            <!--<div class="timeline-footer">
                                                                <a class="btn btn-primary btn-xs">Ver más</a>
                                                            </div>-->
                                                        </div>
                                                    </li>`;
                            $("#UlTimeLine").append(content);
                        });
                        let last = `<li>
                                    <i class="fa fa-clock-o bg-gray"></i>
                                </li>`;

                        $("#UlTimeLine").append(last);

                        $("#overlay__loading").hide();
                        //$("#puntosModalCenter").modal("hide");
                        $("#btnRegistrar").removeAttr("disabled");
                    },
                    error: function(data) {
                        $("#overlay__loading").hide();
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Error al registrar!'
                        });
                    }
                });
            },
            error: function(data) {
                $("#overlay__loading").hide();
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error!'
                });
            }
        });
    });
    $("#cboServicio").change(function() {
        $('#cboFirst').attr("disabled", "disabled");
    });

    $("#overlay__loading").hide();
    //btnAumentar.addEventListener("click", addPuntos);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);

// uwu gamin 1 estudio
// yisus
// hacking slashing
// team pedacitos