//var a espanish datatable
$("#datatable-Steps").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});


//VARIABLES 
var txtTitulo = document.getElementById('txtTitulo');
var txtDescripcion = document.getElementById('txtDescripcion');
var txtOrden = document.getElementById('txtOrden');
var txtTituloUp = document.getElementById('txtTituloUp');
var txtDescripcionUp = document.getElementById('txtDescripcionUp');
var txtOrdenUp = document.getElementById('txtOrdenUp');
var btnRegistrar = document.getElementById('btnRegistrar');
var btnCancelar = document.getElementById('btnCancelar');
var btnActualizar = document.getElementById('btnActualizar');
var FileUpd;
var FileReg;
var idStep;



var txtPtosActuales = document.getElementById('txtPtosActuales');
//funciones



function obtenerID(id, descripcion) {
    idStep = id;
    txtDescripcionUp.value = descripcion;
    //$('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function cancelar() {
    txtDescripcion.value = "";
}

function listarSteps() {

    var param_opcion = 'listSteps';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/steps/cSteps.php',
        success: function(data) {
            var table = $("#datatable-Steps").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(cont);
                array.push(e.descripcion);
                array.push("<button data-toggle='modal' data-target='#updateModalCenter' class='btn btn-xs btn-warning' onclick='obtenerID(" + e.idservicio + "," + '"' + e.descripcion + '"' + ")'>Editar</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminarSteps(" + e.idservicio + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Error!'
            });
        }
    });

}


function editar() {

    var Descripcion = txtDescripcionUp.value;

    var formData = new FormData();
    formData.append("idStep", idStep);
    formData.append("Descripcion", Descripcion);
    formData.append("opcion", 'updateSteps');
    $.ajax({
        type: 'POST',
        data: formData,
        url: '../../controlador/steps/cSteps.php',
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            $('#btnActualizar').attr("disabled", "disabled");
            //$('#fupForm').css("opacity", ".5");
        },
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj == 1) {
                listarSteps();
                cancelar();
                Swal.fire({
                    icon: 'success',
                    title: 'Ok',
                    text: 'Actualización exitosa!'
                });
                sonido.play();
                $("#updateModalCenter").modal("hide");
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error al actualizar!'
                });
            }
            //$("#puntosModalCenter").modal("hide");
            $("#btnActualizar").removeAttr("disabled");
        },
        error: function(data) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Error!'
            });
        }
    });
}

function registrar() {
    var Descripcion = txtDescripcion.value;

    var formData = new FormData();
    formData.append("Descripcion", Descripcion);
    formData.append("opcion", 'insertSteps');
    $.ajax({
        type: 'POST',
        data: formData,
        url: '../../controlador/steps/cSteps.php',
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            $("#overlay__loading").show();
            //$('#fupForm').css("opacity", ".5");
        },
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj == 1) {
                listarSteps();
                cancelar();
                Swal.fire({
                    icon: 'success',
                    title: 'Ok',
                    text: 'Registro exitoso!'
                });
                sonido.play();
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error al registrar!'
                });
            }
            $("#overlay__loading").hide();
            //$("#puntosModalCenter").modal("hide");
            $("#btnRegistrar").removeAttr("disabled");
        },
        error: function(data) {
            $("#overlay__loading").hide();
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Error al registrar!'
            });
        }
    });


}

function eliminarSteps(idStep) {

    var param_opcion = 'deleteSteps';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idStep=' + idStep,
            url: '../../controlador/steps/cSteps.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                if (obj == 1) {

                    listarSteps();
                    alert('Elemento eliminado');

                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Error al eliminar!'
                    });
                }

            },
            error: function(data) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error al eliminar!'
                });
            }
        });
    }

}


function alCargarDocumento() {
    listarSteps();
    btnRegistrar.addEventListener("click", registrar);
    btnActualizar.addEventListener("click", editar);
    btnCancelar.addEventListener("click", cancelar);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);