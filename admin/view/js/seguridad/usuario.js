//var a espanish datatable
$("#datatable-Usuario").DataTable({
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});

//VARIABLES 
var cboCargo = document.getElementById('cboCargo');
var Usuario = document.getElementById('Usuario');
var Contra = document.getElementById('Contra');
var ContraVerifica = document.getElementById('ContraVerifica');
var btnGuardar = document.getElementById('btnRegistrar');
var btnCancelar = document.getElementById('btnCancelar');
var btnEditar = document.getElementById('btnEditar');
var idUsuario;
var olPersonal = [];
//funciones
function obtenerID(idUsu, User, id) {
    idUsuario = idUsu;
    $('#UsuarioUp').val(User);
    console.log(olPersonal);
    oPersonal = olPersonal.filter(personal => personal.id_personal == id);
    $('#txtNombresUp').val(oPersonal[0].nombre + ' ' + oPersonal[0].apaterno + ' ' + oPersonal[0].amaterno);

    //$('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function listarUsuario() {
    var param_opcion = 'listUsuario';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/personal/cPersonal.php',
        success: function(data) {
            var table = $("#datatable-Usuario").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(cont);
                array.push(e.user);
                array.push(e.DNI);
                array.push(e.nombre + ' ' + e.apellido_p + ' ' + e.apellido_m);
                array.push(e.cargo);
                array.push("<button data-toggle='modal' data-target='#puntosModalCenter' class='btn btn-xs btn-warning' onclick='obtenerID(" + e.idusuario + "," + '"' + e.user + '"' + "," + '"' + e.id_personal + '"' + ")'>Editar</button> <button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-success' onclick='resetPassword(" + '"' + e.idusuario + '"' + ")'>Resetear Contraseña</button> <button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminar(" + e.idusuario + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });
        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });
}

function resetPassword(user) {
    Swal.fire({
        title: '¿Quieres resetear la contraseña?',
        text: 'La contraseña por defecto es FisioHog@r2022',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Resetear',
        denyButtonText: `Cancelar`,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            var opcion = 'resetContra';
            if (user != "") {
                console.log(user);
                $.ajax({
                    type: 'POST',
                    data: 'opcion=' + opcion +
                        '&idUsuario=' + user,
                    url: '../../controlador/seguridad/cUsuario.php',
                    success: function(data) {
                        var obj = JSON.parse(data);

                        listarUsuario();
                        if (obj == 1) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Ok',
                                text: 'Usuario editado con exito.'
                            });
                            sonido.play();
                        } else if (obj == 4) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: 'Usuario no existe.'
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: 'No se realizaron modificaciones.'
                            });
                        }
                    },
                    error: function(data) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Error al editar!'
                        });
                    }
                });
            } else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Error',
                    text: 'Campos no válidos!'
                });
            }
            Swal.fire('Saved!', '', 'success')
        } else if (result.isDenied) {
            /*Swal.fire('Changes are not saved', '', 'info')*/
        }
    });
}

function guardar() {
    var opcion = 'insert';
    var usuari = Usuario.value;
    var carg = cboCargo.value;
    var contr;
    if (Contra.value != ContraVerifica.value) { contr = "0"; } else if (ContraVerifica.value == Contra.value) { contr = Contra.value; }
    if (carg != '0' && usuari != "" && contr != "0") {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + opcion +
                '&Usuario=' + usuari + '&Clave=' + contr +
                '&Cargo=' + carg,
            url: '../../controlador/seguridad/cUsuario.php',
            success: function(data) {
                var obj = JSON.parse(data);
                listarUsuario();
                if (obj == 1) {
                    alert('Usuario registrado con exito.');
                    idUsuario = 0;
                    cboCargo.value = 0;
                    Usuario.value = "";
                    Contra.value = "";
                    ContraVerifica.value = "";
                } else if (obj == 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Error al registrar.'
                    });
                }
            },
            error: function(data) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error al registrar.'
                });
            }
        });
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Campos no válidos.'
        });
    }
}

function editar() {
    var opcion = 'editar';
    var usuari = $('#UsuarioUp').val();
    if (usuari != "") {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + opcion +
                '&idUsuario=' + idUsuario +
                '&Usuario=' + usuari,
            url: '../../controlador/seguridad/cUsuario.php',
            success: function(data) {
                var obj = JSON.parse(data);
                listarUsuario();
                if (obj == 1) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Ok',
                        text: 'Usuario editado con exito.'
                    });
                    sonido.play();
                } else if (obj == 4) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Usuario no existe.'
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'No se realizaron modificaciones.'
                    });
                }
            },
            error: function(data) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error al editar!'
                });
            }
        });
    } else {
        Swal.fire({
            icon: 'warning',
            title: 'Error',
            text: 'Campos no válidos!'
        });
    }
}

function eliminar(idusu) {
    Swal.fire({
        title: '¿Quieres eliminar al usuario?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Resetear',
        denyButtonText: `Cancelar`,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            var param_opcion = 'delete';
            $.ajax({
                type: 'POST',
                data: 'opcion=' + param_opcion +
                    '&idUsuario=' + idusu,
                url: '../../controlador/seguridad/cUsuario.php',
                success: function(data) {
                    var obj = JSON.parse(data);
                    if (obj == 1) {
                        listarUsuario();
                    } else {
                        alert('Error al eliminar');
                    }
                },
                error: function(data) {
                    alert('Error al eliminar');
                }
            });
        } else if (result.isDenied) {
            /*Swal.fire('Changes are not saved', '', 'info')*/
        }
    })

}

function cancelar() {}

function llenarcboCargo() {
    $("#cboCargo").empty();
    $("#cboCargo").html('<option value="0">Seleccionar Personal...</option>');
    var param_opcion = 'listPersonal';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/personal/cPersonal.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            olPersonal = obj;
            obj.forEach(function(e) {
                $("#cboCargo").append(`<option value = "${e.id_personal}" > ${e.nombre + ' ' + e.apaterno + ' ' + e.amaterno}</option>`);
            });
            $("#cboCargo").select2();
        },
        error: function(data) {
            console.log(data);
            alert('Error al mostrar');
        }
    });
}

function alCargarDocumento() {
    llenarcboCargo();
    $("#overlay__loading").hide();
    $('#Contra').val('');
    $('#ContraVerifica').val('');
    $('#cboCargo').change(function() {
        let id_personal = $('#cboCargo').val();
        let oPersonal = olPersonal.filter(personal => personal.id_personal == id_personal);
        console.log(oPersonal);
        let olNombres = oPersonal[0].nombre.split(" ");
        let nombres = oPersonal[0].nombre + ' ' + oPersonal[0].apaterno + ' ' + oPersonal[0].amaterno;
        $('#Usuario').val(olNombres[0] + '.' + oPersonal[0].apaterno);
        $('#Contra').val('FisioHog@r2022');
        $('#ContraVerifica').val('FisioHog@r2022');
        $('#txtNombres').val(nombres);
        $('#txtDNI').val(oPersonal[0].dni);

    });
    $('#btnAumentar').click(function() {
        editar();
    });
    btnEditar.style.display = 'none';
    btnCancelar.style.display = 'none';
    btnCancelar.addEventListener("click", cancelar);
    btnGuardar.addEventListener("click", guardar);
    btnEditar.addEventListener("click", editar);
    listarUsuario();
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);