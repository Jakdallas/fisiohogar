loading(1);
//var a espanish datatable
$("#datatable-Personal").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [5, 25, 50, 75, 100]
});


//BOTONES
var txtCantidad = document.getElementById('txtCantidad');
var btnAumentar = document.getElementById('btnAumentar');
var btnRegistrar = document.getElementById('btnRegistrar');
var btnCancelar = document.getElementById('btnCancelar');
var btnCloseUp = document.getElementById('btnCloseUp');
var btnSave = document.getElementById('btnSave');
//BOTONES
//CAJAS DE TEXTO
var txtNombres = document.getElementById('txtNombres');
var txtApePaterno = document.getElementById('txtApePaterno');
var txtApeMaterno = document.getElementById('txtApeMaterno');
var txtDNI = document.getElementById('txtDNI');
var txtTelefono = document.getElementById('txtTelefono');
var txtCargo = document.getElementById('txtCargo');
var txtNombresUp = document.getElementById('txtNombresUp');
var txtApePaternoUp = document.getElementById('txtApePaternoUp');
var txtApeMaternoUp = document.getElementById('txtApeMaternoUp');
var txtDNIUp = document.getElementById('txtDNIUp');
var txtTelefonoUp = document.getElementById('txtTelefonoUp');
var txtCargoUp = document.getElementById('txtCargoUp');
//CAJAS DE TEXTO
//VARIABLES
var idPersonal;
var olPersonal = [];
//VARIABLES

//HTMLElements
var txtPtosActuales = document.getElementById('txtPtosActuales');
//funciones
function loading(option) {
    if (option == 1) {
        $("#overlay__loading").show("fade", { direction: "up" }, "slow");
    } else {
        $("#overlay__loading").hide("fade", { direction: "down" }, "slow");
    }
}

function obtenerID(id) {
    idPersonal = id;
    oPersonal = olPersonal.filter(personal => personal.id_personal == id);
    txtNombresUp.value = oPersonal.nombre;
    txtApePaternoUp.value = oPersonal.apaterno;
    txtApeMaternoUp.value = oPersonal.amaterno;
    txtDNIUp.value = oPersonal.dni;
    txtTelefonoUp.value = oPersonal.telefono;
    txtCargoUp.value = oPersonal.cargo;
}

function limpiarVariables() {
    idPersonal = null;
}

function limpiartxt() {
    txtNombres.value = "";
    txtApePaterno.value = "";
    txtApeMaterno.value = "";
    txtDNI.value = "";
    txtTelefono.value = "";
    txtCargo.value = "";
    txtNombresUp.value = "";
    txtApePaternoUp.value = "";
    txtApeMaternoUp.value = "";
    txtDNIUp.value = "";
    txtTelefonoUp.value = "";
    txtCargoUp.value = "";
}

function listarPersonal() {

    var param_opcion = 'listPersonal';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/personal/cPersonal.php',
        success: function(data) {
            var table = $("#datatable-Personal").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            olPersonal = obj;
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(cont);
                array.push(e.dni);
                array.push(e.nombre + ' ' + e.apaterno + ' ' + e.amaterno);
                /*array.push(e.correo);*/
                array.push(e.telefono);
                array.push(e.cargo);
                array.push("<button data-toggle='modal' data-target='#updateModalCenter' class='btn btn-xs btn-warning pr-2' onclick='cargarPersonal(" + e.id_personal + ")'>Editar</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminarPersonal(" + e.id_personal + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });
        },
        error: function(data) {
            console.log(data);
            alert('Error al mostrar');
        }
    });

}

function cargarPersonal(idPersonal) {
    obtenerID(idPersonal);
    cargarDatos();
}

function cargarDatos() {
    if (idPersonal != null) {
        let param_opcion = 'obtenerPersonal';
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idPersonal=' + idPersonal,
            url: '../../controlador/personal/cPersonal.php',
            success: function(data) {
                var obj = JSON.parse(data);
                txtNombresUp.value = obj.nombre;
                txtApePaternoUp.value = obj.apaterno;
                txtApeMaternoUp.value = obj.amaterno;
                txtDNIUp.value = obj.dni;
                txtTelefonoUp.value = obj.telefono;
                txtCargoUp.value = obj.cargo;
            },
            error: function(data) {
                console.log(data);
                alert('Error al mostrar');
            }
        });
    }
}

function create() {

    let opcion = 'createPersonal';
    let dni = txtDNI.value;
    let Nombres = txtNombres.value;
    let APaterno = txtApePaterno.value;
    let AMaterno = txtApeMaterno.value;
    let Telefono = txtTelefono.value;
    let Cargo = txtCargo.value;

    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion +
            '&dni=' + dni +
            '&Nombres=' + Nombres +
            '&APaterno=' + APaterno +
            '&AMaterno=' + AMaterno +
            '&Telefono=' + Telefono +
            '&Cargo=' + Cargo,
        url: '../../controlador/personal/cPersonal.php',
        success: function(data) {
            console.log(data);
            var obj = JSON.parse(data);
            if (obj == 1) {
                alert('Personal ingresado con exito.');
                loading(1);
                listarPersonal();
                limpiartxt();
                loading(0);
            } else {
                alert('No se ha realizado inserción.');
            }
        },
        error: function(data) {
            console.log(data);
            alert('Error al registrar');
        }
    });
}

function editar() {
    if (idPersonal != null) {
        let opcion = 'updatePersonal';
        let dni = txtDNIUp.value;
        let Nombres = txtNombresUp.value;
        let APaterno = txtApePaternoUp.value;
        let AMaterno = txtApeMaternoUp.value;
        let Telefono = txtTelefonoUp.value;
        let Cargo = txtCargoUp.value;
        if (dni != null &&
            Nombres != null &&
            APaterno != null &&
            AMaterno != null &&
            Cargo != null) {
            $.ajax({
                type: 'POST',
                data: 'opcion=' + opcion +
                    '&idPersonal=' + idPersonal +
                    '&dni=' + dni +
                    '&Nombres=' + Nombres +
                    '&APaterno=' + APaterno +
                    '&AMaterno=' + AMaterno +
                    '&Telefono=' + Telefono +
                    '&Cargo=' + Cargo,
                url: '../../controlador/personal/cPersonal.php',
                success: function(data) {
                    var obj = JSON.parse(data);
                    if (obj == 1) {
                        alert('Personal editado con exito.');
                        idPersonal = null;
                        btnCloseUp.click();
                        loading(1);
                        listarPersonal();
                        loading(0);
                    } else {
                        alert('No se realizaron modificaciones.');
                    }
                },
                error: function(data) {
                    alert('Error al editar');
                }
            });
        }
    }

}

function eliminarPersonal(idPersonal) {

    var param_opcion = 'deletePersonal';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idPersonal=' + idPersonal,
            url: '../../controlador/personal/cPersonal.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                if (obj == 1) {

                    listarPersonal();

                } else {
                    alert('Error al eliminar');
                }

            },
            error: function(data) {
                alert('Error al eliminar');
            }
        });
    }

}

function alCargarDocumento() {
    listarPersonal();
    btnRegistrar.addEventListener("click", create);
    btnCancelar.addEventListener("click", limpiartxt);
    btnSave.addEventListener("click", editar);
    $('#updateModalCenter').on('hidden.bs.modal', function(e) {
        limpiarVariables();
        limpiartxt();
    });
    loading(0);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);