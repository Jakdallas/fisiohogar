//VARIABLES 
var txtFechaInicio = document.getElementById('txtFechaInicio');
var txtFechaFin = document.getElementById('txtFechaFin');


var pieOptions = {
    // Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    // String - The colour of each segment stroke
    segmentStrokeColor: '#fff',
    // Number - The width of each segment stroke
    segmentStrokeWidth: 1,
    // Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    // Number - Amount of animation steps
    animationSteps: 100,
    // String - Animation easing effect
    animationEasing: 'easeOutBounce',
    // Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    // Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    // Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: false,
    // String - A legend template
    legendTemplate: '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
    // String - A tooltip template
    tooltipTemplate: '<%=value %> <%=label%> Doc'
};

/*[
                            { y: '2011-01', item1: 2666 },
                            { y: '2011-02', item1: 2500 },
                            { y: '2011-03', item1: 5000 },
                            { y: '2011-04', item1: 2666 },
                            { y: '2011-05', item1: 2666 },
                            { y: '2011-06', item1: 2666 },
                            { y: '2011-07', item1: 2666 },
                            { y: '2011-08', item1: 2666 },
                            { y: '2011-09', item1: 2666 },
                            { y: '2011-10', item1: 2666 },
                            { y: '2011-11', item1: 2666 },
                        ]*/
var olMeses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
var olColor = ['#f56954', '#00c0ef', '#f39c12'];
var olColorL = ['red', 'aqua', 'orange'];

function handler(e) {
    if (txtFechaFin.value != "" & txtFechaInicio.value != "") {
        if (txtFechaFin.value > txtFechaInicio.value || txtFechaFin.value == txtFechaInicio.value) {

            var param_opcion = 'listVisita';

            $.ajax({
                type: 'POST',
                data: 'opcion=' + param_opcion +
                    '&FechaInicio=' + txtFechaInicio.value +
                    '&FechaFin=' + txtFechaFin.value,
                url: '../../controlador/panel/cDashBoard.php',
                success: function(data) {
                    var obj = JSON.parse(data);
                    console.log(obj);
                    let olArea = []
                    $("#meses").empty();
                    obj.forEach(function(e) {
                        let areadata = { y: e.FECHA, item1: parseInt(e.IP) }
                        let labelDay = "";
                        if (e.DAY != undefined) {
                            labelDay = `- ${e.DAY}`
                        }
                        $("#meses").append(`<li><a href="#">${olMeses[e.MONTH-1]} ${labelDay}
                          <span class="pull-right text-yellow"><i class="fa fa-user"></i> ${parseInt(e.IP) }</span></a>
                        </li>`)
                        olArea.push(areadata);
                    });
                    /* Morris.js Charts */
                    // Sales chart
                    var area = new Morris.Area({
                        element: 'revenue-chart',
                        resize: true,
                        data: olArea,
                        xkey: 'y',
                        ykeys: ['item1'],
                        labels: ['Visitas'],
                        lineColors: ['#a0d0e0'],
                        hideHover: 'auto'
                    });
                    area.redraw();
                },
                error: function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Error!'
                    })
                }
            });

            param_opcion = 'listCita';
            $.ajax({
                type: 'POST',
                data: 'opcion=' + param_opcion +
                    '&FechaInicio=' + txtFechaInicio.value +
                    '&FechaFin=' + txtFechaFin.value,
                url: '../../controlador/panel/cDashBoard.php',
                success: function(data) {
                    var obj = JSON.parse(data);
                    let olPie = []
                    let cont = 0;
                    $("#estado").empty();
                    $("#legend").empty();
                    obj.forEach(function(e) {

                        let piedata = {
                            value: parseInt(e.cont),
                            color: olColor[cont],
                            highlight: olColor[cont],
                            label: e.descripcion
                        }
                        $("#estado").append(`<li><a href="#">${e.descripcion}
                                                <span class="pull-right text-${olColorL[cont]}"><i class="fa fa-user"></i> ${parseInt(e.cont) }</span></a>
                                              </li>`)
                        $("#legend").append(`<li>
                                              <i class="fa fa-circle-o text-${olColorL[cont]}"></i> ${e.descripcion}
                                            </li>`)
                        olPie.push(piedata);
                        cont++;
                    });
                    // -------------
                    // - PIE CHART -
                    // -------------
                    // Get context with jQuery - using jQuery's .get() method.
                    var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
                    var pieChart = new Chart(pieChartCanvas);
                    /*var PieData = [{
                            value: 5,
                            color: '#f56954',
                            highlight: '#f56954',
                            label: 'Pendiente'
                        },

                        {
                            value: 3,
                            color: '#00c0ef',
                            highlight: '#00c0ef',
                            label: 'Canjeado'
                        },

                        {
                            value: 4,
                            color: '#f39c12',
                            highlight: '#f39c12',
                            label: 'Anulado'
                        }
                    ];*/

                    // Create pie or douhnut chart
                    // You can switch between pie and douhnut using the method below.
                    pieChart.Doughnut(olPie, pieOptions);
                },
                error: function(data) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Error!'
                    })
                }
            });





        } else {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Fecha de inicio es mayor a fecha fin!'
            })
        }
    }
}

function alCargarDocumento() {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) { dd = '0' + dd; }
    if (mm < 10) { mm = '0' + mm; }

    today = yyyy + '-' + mm + '-' + dd;
    beginYear = yyyy + '-' + '01' + '-' + '01';

    $('#txtFechaInicio').val(beginYear);
    $('#txtFechaFin').val(today);
    handler();
    $('#txtFechaInicio').change(function() { handler(); });
    $('#txtFechaFin').change(function() { handler(); });



    // Fix for charts under tabs
    $('.box ul.nav a').on('shown.bs.tab', function() {
        area.redraw();
        line.redraw();
    });

    $("#overlay__loading").hide();

}
//EVENTOS
window.addEventListener("load", alCargarDocumento);