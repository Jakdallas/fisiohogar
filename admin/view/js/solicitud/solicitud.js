//var a espanish datatable
$("#datatable-Cliente").DataTable({
    //dom: 'lfrtip',
    //"responsive": true,
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});


//VARIABLES 
var txtCantidad = document.getElementById('txtCantidad');
var btnAumentar = document.getElementById('btnAumentar');
var olCitas = [];
var olEstados = [];
var idCita;
var idCliente;
var idClienteAumentar;


var txtPtosActuales = document.getElementById('txtPtosActuales');
//funciones



function obtenerID(id) {
    idCita = id;
    let oCita = olCitas.find(e => e.idcita == id);
    $("#txtDNI").val(oCita.DNI);
    $("#txtNombre").val(oCita.nombre);
    $("#txtCiudad").val(oCita.ciudad);
    $("#txtTelefono").val(oCita.telefono);
    $("#txtEmail").val(oCita.email);
    $("#txtFecha").val(oCita.fecha_registro);
    $("#txtTexto").text(oCita.mensaje);

    $("#cboEstado").empty();
    olEstados.forEach(function(oEstado) {
        $("#cboEstado").append('<option value="' + oEstado.idestado + '">' + oEstado.descripcion + '</option>');
    });

    $("#cboEstado").val(oCita.idestado);
    $("#cboEstado").change();
    //$('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function listarSolicitudes() {

    var param_opcion = 'listSolicitud';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/solicitud/cSolicitud.php',
        success: function(data) {
            var table = $("#datatable-Cliente").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            olCitas = obj;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(cont);
                array.push(e.DNI);
                array.push(e.nombre);
                array.push(e.ciudad);
                array.push(e.email);
                array.push(e.telefono);
                array.push(e.descripcion);
                array.push(e.fecha_registro);
                array.push("<button data-toggle='modal' data-target='#verSolicitud' class='btn btn-xs btn-warning' onclick='obtenerID(" + e.idcita + ")'>Ver detalle</button>");
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function llenarcboEstado() {

    var param_opcion = 'listEstado';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/solicitud/cSolicitud.php',
        success: function(data) {
            var obj = JSON.parse(data);
            olEstados = obj;
            console.log(obj);
        },
        error: function(data) {

        }
    });
}

function editar() {

    var opcion = 'editSolicitud';
    var idestado = $("#cboEstado").val();
    var idcita = idCita;
    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion +
            '&idEstado=' + idestado +
            '&idCita=' + idcita,
        url: '../../controlador/solicitud/cSolicitud.php',
        success: function(data) {
            console.log(data);
            var obj = JSON.parse(data);
            listarSolicitudes();
            if (obj == 1) {
                Swal.fire({
                    icon: 'success',
                    title: 'Actualización exitosa',
                    text: 'Solicitud actualizada con exito!'
                });
                sonido.play();

            } else if (obj == 4) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Campos incorrectos!'
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Campos incorrectos!'
                })
            }
        },
        error: function(data) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Error al editar!'
            })
        }
    });
}

function alCargarDocumento() {
    listarSolicitudes();
    llenarcboEstado()
    btnAumentar.addEventListener("click", editar);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);