//VARIABLES 
var idLetra;
var olDocumento = [];
var txtGirador = document.getElementById('txtGirador');
var txtLugar = document.getElementById('txtLugar');
var txtFechaGiro = document.getElementById('txtFechaGiro');
var txtPlazo = document.getElementById('txtPlazo');
var txtFechaVencimiento = document.getElementById('txtFechaVencimiento');
var txtLetra = document.getElementById('txtLetra');
//VARIABLES PAGO LETRA
var txtLetraPago = document.getElementById('txtLetraPago');
var txtFechaPago = document.getElementById('txtFechaPago');
var txtObservacionesPago = document.getElementById('txtObservacionesPago');
//VARIABLES EXTORNO
var txtLetraExtorno = document.getElementById('txtLetraExtorno');
var txtFechaExtorno = document.getElementById('txtFechaExtorno');
var txtObservacionesExtorno = document.getElementById('txtObservacionesExtorno');
//VARIABLES BÚSQUEDA CON PARAMETROS
var txtFechaInicio = document.getElementById('txtFechaInicio');
var txtFechaFin = document.getElementById('txtFechaFin');
var cboEstado = document.getElementById('cboEstado');
var cboCliente = document.getElementById('cboCliente');

var btnBuscar = document.getElementById('btnBuscar');
var btnGuardar = document.getElementById('btnRegistrar');
var btnRegistrarExtorno = document.getElementById('btnRegistrarExtorno');
var btnRegistrarPago = document.getElementById('btnRegistrarPago');
//var a espanish datatable
function initTables() {
    $("#datatable-Letra").DataTable({
        //dom: 'lfrtip',
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        columnDefs: [{
            "targets": [0],
            "visible": false
        }],
        "lengthMenu": [10, 25, 50, 75, 100]
    });

    //var a espanish datatable
    $("#datatable-Documento").DataTable({
        //dom: 'lfrtip',
        select: {
            style: 'multi'
        },

        columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            },
            {
                "targets": [5],
                "visible": false
            }
        ],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [
            [1, 'asc']
        ]
    });
}

function listarLetras() {

    var param_opcion = 'listLetra';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/letra/cLetra.php',
        success: function(data) {
            var table = $("#datatable-Letra").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(e.idLetra);
                array.push(e.NroLetra);
                array.push(e.Plazo);
                array.push(e.FechaEmision);
                array.push(e.FechaVencim);
                array.push(e.usuario);
                if (e.Estado == 0) {
                    array.push("PENDIENTE");
                } else if (e.Estado == 1) {
                    array.push("PAGADO");
                } else if (e.Estado == 2) {
                    array.push("EXTORNADO");
                } else {
                    array.push("");
                }
                if (e.FechaPago == null && e.Fechaextorno == null) {
                    array.push('<button data-toggle="modal" href="#mdlPago" id="btnPago" onclick="obtenerIdLetra(' + e.idLetra + ",'" + e.NroLetra + "'" + ')" class="btn btn-xs btn-primary">Pago</button>');
                } else {
                    array.push('<button data-toggle="modal" href="#mdlPago" id="btnPago" onclick="showPago(' + "'" + e.NroLetra + "','" + e.FechaPago + "','" + e.ObservPago + "','" + e.Fechaextorno + "'" + ')" class="btn btn-xs btn-primary">Pago</button>');
                }
                if (e.Fechaextorno == null) {
                    array.push('<button data-toggle="modal" href="#mdlExtorno" id="btnExtorno" onclick="obtenerIdLetra(' + e.idLetra + ",'" + e.NroLetra + "'" + ')" class="btn btn-xs btn-warning">Extorno</button>');
                } else {
                    array.push('<button data-toggle="modal" href="#mdlExtorno" id="btnExtorno" onclick="showExtorno(' + "'" + e.NroLetra + "','" + e.Fechaextorno + "','" + e.Observacion + "'" + ')" class="btn btn-xs btn-warning">Extorno</button>');
                }
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });
}

function buscarLetras() {
    fechaInicial = txtFechaInicio.value;
    fechaFinal = txtFechaFin.value;
    idCliente = cboCliente.value;
    Estado = cboEstado.value;
    if (idCliente == 0) {
        alert("Escoja un cliente");
        return;
    }
    if (fechaInicial == "" || fechaInicial == null || fechaInicial == undefined) {
        alert("Ingrese un párametro para fecha inicial válido");
        return;
    }
    if (fechaFinal == "" || fechaFinal == null || fechaFinal == undefined) {
        alert("Ingrese un párametro para fecha final válido");
        return;
    }
    cadFecIni = fechaInicial.split('-');
    cadFecFin = fechaFinal.split('-');
    DateIni = new Date(cadFecIni[0], cadFecIni[1], cadFecIni[2]);
    DateFin = new Date(cadFecFin[0], cadFecFin[1], cadFecFin[2]);
    console.log()
    if (DateIni > DateFin) {
        alert("Ingrese un rango de fechas válido");
        return;
    }
    var param_opcion = 'searchByParameters';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&fechaInicial=' + fechaInicial +
            '&fechaFinal=' + fechaFinal +
            '&idCliente=' + idCliente +
            '&Estado=' + Estado,
        url: '../../controlador/letra/cLetra.php',
        success: function(data) {
            var table = $("#datatable-Letra").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(e.idLetra);
                array.push(e.NroLetra);
                array.push(e.Plazo);
                array.push(e.FechaEmision);
                array.push(e.FechaVencim);
                array.push(e.usuario);
                if (e.Estado == 0) {
                    array.push("PENDIENTE");
                } else if (e.Estado == 1) {
                    array.push("PAGADO");
                } else if (e.Estado == 2) {
                    array.push("EXTORNADO");
                } else {
                    array.push("");
                }
                if (e.FechaPago == null && e.Fechaextorno == null) {
                    array.push('<button data-toggle="modal" href="#mdlPago" id="btnPago" onclick="obtenerIdLetra(' + e.idLetra + ",'" + e.NroLetra + "'" + ')" class="btn btn-xs btn-primary">Pago</button>');
                } else {
                    array.push('<button data-toggle="modal" href="#mdlPago" id="btnPago" onclick="showPago(' + "'" + e.NroLetra + "','" + e.FechaPago + "','" + e.ObservPago + "','" + e.Fechaextorno + "'" + ')" class="btn btn-xs btn-primary">Pago</button>');
                }
                if (e.Fechaextorno == null) {
                    array.push('<button data-toggle="modal" href="#mdlExtorno" id="btnExtorno" onclick="obtenerIdLetra(' + e.idLetra + ",'" + e.NroLetra + "'" + ')" class="btn btn-xs btn-warning">Extorno</button>');
                } else {
                    array.push('<button data-toggle="modal" href="#mdlExtorno" id="btnExtorno" onclick="showExtorno(' + "'" + e.NroLetra + "','" + e.Fechaextorno + "','" + e.Observacion + "'" + ')" class="btn btn-xs btn-warning">Extorno</button>');
                }
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function listarDocumentos() {

    var param_opcion = 'listDocumentoPendiente';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/documento/cDocumento.php',
        success: function(data) {
            var table = $("#datatable-Documento").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push("");
                array.push(e.TipoPago);
                array.push(e.Serie);
                array.push(e.Numero);
                array.push(e.Monto);
                array.push(e.idDocumento);
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function listarDocumentosByCliente() {
    idCliente = cboCliente.value;
    if (idCliente == 0 || idCliente == null || idCliente == undefined) {
        var table = $("#datatable-Documento").DataTable();
        table.clear().draw();
    } else {
        var param_opcion = 'listDocumentoPendienteByClient';
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idCliente=' + idCliente,
            url: '../../controlador/documento/cDocumento.php',
            success: function(data) {
                var table = $("#datatable-Documento").DataTable();
                table.clear().draw();
                var obj = JSON.parse(data);
                var cont = 0;
                obj.forEach(function(e) {
                    cont++;
                    array = [];
                    array.push("");
                    array.push(e.TipoPago);
                    array.push(e.Serie);
                    array.push(e.Numero);
                    array.push(e.Monto);
                    array.push(e.idDocumento);
                    table.row.add(array).draw().node();
                });

            },
            error: function(data) {
                alert('Error al mostrar');
            }
        });

    }

}


function eventSelectTable() {
    var table = $('#datatable-Documento').DataTable();

    $('#datatable-Documento tbody').on('click', 'tr', function() {
        $(this).toggleClass('selected');
        var rows = table.rows('.selected').data().length;
        olDocumento = [];
        var montoTotal = 0;
        for (let index = 0; index < rows; index++) {
            montoTotal = montoTotal + parseFloat(table.rows('.selected').data()[index][4]);
            olDocumento.push(table.rows('.selected').data()[index][5]);
        }
        $("#totalLetra").val(montoTotal);

    });

}

function llenarcboClientes() {

    var param_opcion = 'listCliente';
    $("#cboCliente").empty();
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            $("#cboCliente").html('<option value="0">Seleccionar Cliente...</option>');
            obj.forEach(function(e) {
                $("#cboCliente").append('<option value="' + e.idCliente + '">' + e.RazonSocial + '</option>');
            });
            $("#cboCliente").select2();
        },
        error: function(data) {
            alert("Error al cargar clientes");
        }
    });
}

function llenarcboEstado() {

    $("#cboEstado").empty();
    $("#cboEstado").append('<option value="' + 0 + '">' + "PENDIENTE" + '</option>');
    $("#cboEstado").append('<option value="' + 1 + '">' + "PAGADO" + '</option>');
    $("#cboEstado").append('<option value="' + 2 + '">' + "EXTORNADO" + '</option>');

}

function registrarPago() {
    if (confirm("Seguro de realizar el pago (S/N)")) {
        FechaPago = txtFechaPago.value;
        Observacion = txtObservacionesPago.value;
        if (FechaPago == "" || FechaPago == null || FechaPago == undefined) {
            alert("Ingrese un párametro para fecha pago válido");
            return;
        }
        var param_opcion = 'insertPago';
        console.log(idLetra);
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&FechaPago=' + FechaPago +
                '&idLetra=' + idLetra +
                '&Observacion=' + Observacion,
            url: '../../controlador/letra/cLetra.php',
            success: function(data) {
                var obj = JSON.parse(data);
                listarDocumentos();
                listarLetras();
                if (obj != 0) {
                    alert("Se registro correctamente");
                } else {
                    alert("Error al procesar pago");
                }
            },
            error: function(data) {
                alert("Error al procesar pago");
            }
        });
    } else {
        $("#mdlPago").show();
    }
}


function registrarExtorno() {
    if (confirm("Seguro de realizar el extorno (S/N)")) {
        FechaExtorno = txtFechaExtorno.value;
        Observacion = txtObservacionesExtorno.value;
        if (FechaExtorno == "" || FechaExtorno == null || FechaExtorno == undefined) {
            alert("Ingrese un párametro para fecha extorno válido");
            return;
        }
        var param_opcion = 'insertExtorno';
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&FechaExtorno=' + FechaExtorno +
                '&idLetra=' + idLetra +
                '&Observacion=' + Observacion,
            url: '../../controlador/letra/cLetra.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(obj);
                listarDocumentos();
                listarLetras();
                if (obj != 0) {
                    alert("Se registro correctamente");
                } else {
                    alert("Error al procesar extorno");
                }
            },
            error: function(data) {
                alert("Error al procesar extorno");
            }
        });
    } else {
        $("#mdlExtorno").show();
    }
}

function guardar() {
    NroLetra = txtLetra.value;
    FechaEmision = txtFechaGiro.value;
    Plazo = txtPlazo.value;
    FechaVencim = txtFechaVencimiento.value;
    Lugar = txtLugar.value;
    idCliente = cboCliente.value;
    Girador = txtGirador.value;
    var param_opcion = 'insertLetra';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&NroLetra=' + NroLetra +
            '&FechaEmision=' + FechaEmision +
            '&Plazo=' + Plazo +
            '&Girador=' + Girador +
            '&Lugar=' + Lugar +
            '&FechaVencim=' + FechaVencim +
            '&olDocumento=' + olDocumento,
        url: '../../controlador/letra/cLetra.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            buscarLetras();
            listarDocumentosByCliente();
            if (obj == 1) {
                alert("Se registro correctamente");
                txtLetra.value = "";
                txtFechaGiro.value = "";
                txtFechaVencimiento.value = "";
                txtPlazo.value = "";
                txtLugar.value = "";
                txtGirador.value = "";
                setInitialDate();
                if (idCliente == 0 || idCliente == null || idCliente == undefined) {
                    var table = $("#datatable-Letra").DataTable();
                    table.clear().draw();
                } else {
                    buscarLetras();
                }
            } else {
                alert("Datos duplicados");
            }
        },
        error: function(data) {
            alert("Error al insertar");
        }
    });

}

function obtenerIdLetra(idLetraParam, LetraParam) {
    txtLetraPago.value = "";
    txtFechaPago.value = "";
    txtObservacionesPago.value = "";
    txtLetraExtorno.value = "";
    txtFechaExtorno.value = "";
    txtObservacionesExtorno.value = "";

    idLetra = idLetraParam;
    txtLetraPago.value = LetraParam;
    txtLetraExtorno.value = LetraParam;

    $("#txtFechaPago").prop('disabled', false);
    $("#txtObservacionesPago").prop('disabled', false);
    $("#txtFechaExtorno").prop('disabled', false);
    $("#txtObservacionesExtorno").prop('disabled', false);
    $("#btnRegistrarExtorno").prop('disabled', false);
    $("#btnRegistrarPago").prop('disabled', false);

}

function showPago(LetraParam, FechaParam, ObservacionParam, FechaExtornoParam) {
    txtLetraPago.value = "";
    txtFechaPago.value = "";
    txtObservacionesPago.value = "";

    txtLetraPago.value = LetraParam;
    txtFechaPago.value = FechaParam;
    txtObservacionesPago.value = ObservacionParam;

    if (FechaExtornoParam != null) {
        document.getElementById("tltPago").innerHTML = "EXTORNADO";
    } else {
        document.getElementById("tltPago").innerHTML = "Registrar Pago";
    }

    $("#txtFechaPago").prop('disabled', true);
    $("#txtObservacionesPago").prop('disabled', true);
    $("#btnRegistrarPago").prop('disabled', true);
}

function showExtorno(LetraParam, FechaParam, ObservacionParam) {
    txtLetraExtorno.value = "";
    txtFechaExtorno.value = "";
    txtObservacionesExtorno.value = "";

    txtLetraExtorno.value = LetraParam;
    txtFechaExtorno.value = FechaParam;
    txtObservacionesExtorno.value = ObservacionParam;

    $("#txtFechaExtorno").prop('disabled', true);
    $("#txtObservacionesExtorno").prop('disabled', true);
    $("#btnRegistrarExtorno").prop('disabled', true);
}

function addEventFecha() {
    $("#txtPlazo").blur(function() {
        FechaGiro = txtFechaGiro.value;
        Plazo = txtPlazo.value;
        if (FechaGiro != "" || FechaGiro != null || FechaGiro != undefined) {

            DateGir = new Date(FechaGiro);
            DateGir.setDate((DateGir.getDate() + parseInt(Plazo)));

            var dd = DateGir.getDate();
            var mm = DateGir.getMonth() + 1;
            var yyyy = DateGir.getFullYear();

            if (dd < 10) { dd = '0' + dd; }
            if (mm < 10) { mm = '0' + mm; }

            DateGir = yyyy + '-' + mm + '-' + dd;
            txtFechaVencimiento.value = DateGir;
        }
    });

}

function setInitialDate() {
    var actualDate = new Date();
    var startDate = new Date(actualDate.getFullYear(), 0, 1);
    var dd = startDate.getDate();
    var mm = startDate.getMonth() + 1;
    var yyyy = startDate.getFullYear();

    if (dd < 10) { dd = '0' + dd; }
    if (mm < 10) { mm = '0' + mm; }

    startDate = yyyy + '-' + mm + '-' + dd;
    txtFechaInicio.value = startDate;

    var endDate = new Date(actualDate.getFullYear(), 12, 0);
    var dd = endDate.getDate();
    var mm = endDate.getMonth() + 1;
    var yyyy = endDate.getFullYear();

    if (dd < 10) { dd = '0' + dd; }
    if (mm < 10) { mm = '0' + mm; }

    endDate = yyyy + '-' + mm + '-' + dd;
    txtFechaFin.value = endDate;

}

function eventSelectClient() {
    $("#cboCliente").change(function() {
        listarDocumentosByCliente();
    });
}

function alCargarDocumento() {
    //listarDocumentos();
    listarLetras();
    initTables();
    setInitialDate();
    eventSelectTable();
    eventSelectClient();
    llenarcboClientes();
    llenarcboEstado();
    addEventFecha();
    btnBuscar.addEventListener("click", buscarLetras);
    btnGuardar.addEventListener("click", guardar);
    btnRegistrarPago.addEventListener("click", registrarPago);
    btnRegistrarExtorno.addEventListener("click", registrarExtorno);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);