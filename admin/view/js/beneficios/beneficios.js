//var a espanish datatable
$("#datatable-Beneficios").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});


//VARIABLES 
var cboPlan = document.getElementById('cboPlan');
var txtBeneficio = document.getElementById('txtBeneficio');

var cboPlanUp = document.getElementById('cboPlanUp');
var txtBeneficioUp = document.getElementById('txtBeneficioUp');

var btnRegistrar = document.getElementById('btnRegistrar');
var btnCancelar = document.getElementById('btnCancelar');
var btnActualizar = document.getElementById('btnActualizar');
var idBeneficio;



var txtPtosActuales = document.getElementById('txtPtosActuales');
//funciones



function obtenerID(idbeneficio, nombre, idprecio) {

    llenarcboPlanActualizar(idprecio);
    //$("#cboPlanUp").select2();
    idBeneficio = idbeneficio;
    txtBeneficioUp.value = nombre;
    //$('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function listarBeneficios() {

    var param_opcion = 'listBeneficios';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/beneficios/cBeneficios.php',
        success: function(data) {
            var table = $("#datatable-Beneficios").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(cont);
                array.push(e.beneficio);
                array.push(e.nombre);
                array.push("<button data-toggle='modal' data-target='#updateModalCenter' class='btn btn-xs btn-warning' onclick='obtenerID(" + e.idbeneficio + "," + '"' + e.beneficio + '"' + "," + '"' + e.idprecio + '"' + ")'>Editar</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminarBeneficio(" + e.idbeneficio + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function editar() {

    var idPrecio = cboPlanUp.value;
    var Beneficio = txtBeneficioUp.value;

    var opcion = 'updateBeneficios';

    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion +
            '&idPrecio=' + idPrecio +
            '&Beneficio=' + Beneficio +
            '&idBeneficio=' + idBeneficio,
        url: '../../controlador/beneficios/cBeneficios.php',
        success: function(data) {
            console.log(data);
            var obj = JSON.parse(data);
            listarBeneficios();
            if (obj == 1) {
                alert('Beneficio actualizado con exito.');
                cancelar();
                $("#updateModalCenter").modal("hide");

            } else {
                alert('No se realizaron modificaciones.');
            }
        },
        error: function(data) {
            alert('Error al editar');
        }
    });
}

function registrarBeneficio() {

    var idPrecio = cboPlan.value;
    var Beneficio = txtBeneficio.value;

    var param_opcion = 'insertBeneficios';
    $("btnRegistrar").prop('disabled', true);
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&idPrecio=' + idPrecio +
            '&Beneficio=' + Beneficio,
        url: '../../controlador/beneficios/cBeneficios.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj == 1) {

                listarBeneficios();
                alert("Registro exitoso");
                cancelar();
            } else {
                alert('Error al agregar puntos');
            }
            $("btnRegistrar").prop('disabled', false);
        },
        error: function(data) {
            alert('Error al agregar puntos');
            $("btnRegistrar").prop('disabled', false);
        }
    });


}

function eliminarBeneficio(idBeneficio) {

    var param_opcion = 'deleteBeneficios';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idBeneficio=' + idBeneficio,
            url: '../../controlador/beneficios/cBeneficios.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                if (obj == 1) {

                    listarBeneficios();

                } else {
                    alert('Error al eliminar');
                }

            },
            error: function(data) {
                alert('Error al eliminar');
            }
        });
    }

}

function llenarcboPlan() {

    var param_opcion = 'listPricing';
    $("#cboPlan").empty();
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/tarifas/cTarifas.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            $("#cboPlan").html('<option value="0">Seleccionar Plan de Pago...</option>');
            obj.forEach(function(e) {
                $("#cboPlan").append('<option value="' + e.idprecio + '">' + e.nombre + '</option>');
            });
        },
        error: function(data) {

        }
    });
}

function llenarcboPlanActualizar(idprecio) {

    var param_opcion = 'listPricing';
    $("#cboPlanUp").empty();
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/tarifas/cTarifas.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            obj.forEach(function(e) {
                $("#cboPlanUp").append('<option value="' + e.idprecio + '">' + e.nombre + '</option>');
            });

            $("#cboPlanUp").val(idprecio);
            $("#cboPlanUp").change();
        },
        error: function(data) {

        }
    });
}

function cancelar() {
    llenarcboPlan();
    txtBeneficio.value = "";
}

function alCargarDocumento() {
    llenarcboPlan();
    listarBeneficios();
    btnRegistrar.addEventListener("click", registrarBeneficio);
    btnActualizar.addEventListener("click", editar);
    btnCancelar.addEventListener("click", cancelar);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);