//var a espanish datatable
$("#datatable-Cliente").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});


//VARIABLES 
var txtCantidad = document.getElementById('txtCantidad');
var btnAumentar = document.getElementById('btnAumentar');
var idCliente;
var idClienteAumentar;


var txtPtosActuales = document.getElementById('txtPtosActuales');
//funciones



function obtenerID(id, puntos) {
    idCliente = id;
    idClienteAumentar = id;
    txtPtosActuales.value = puntos;
    //$('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function listarClientes() {

    var param_opcion = 'listCliente';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var table = $("#datatable-Cliente").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(e.documento);
                array.push(e.nombres);
                array.push(e.cmp);
                array.push(e.ecografo_modelo);
                array.push(e.ecografo_marca);
                array.push(e.ciudad);
                array.push(e.correo);
                array.push(e.telefono);
                array.push(e.puntos);
                array.push("<button data-toggle='modal' data-target='#puntosModalCenter' class='btn btn-xs btn-warning' onclick='obtenerID(" + e.idCliente + "," + '"' + e.puntos + '"' + ")'>Puntos</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminarCliente(" + e.idCliente + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function editar() {

    var opcion = 'updateCliente';
    var RazonSocial = txtRazonSocial.value;
    var Responsable = txtResponsable.value;
    var RUC = txtRUC.value;
    var DNIResponsable = txtDNIResponsable.value;
    var idZona = cboZona.value;
    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion +
            '&idCliente=' + idCliente +
            '&RazonSocial=' + RazonSocial +
            '&Responsable=' + Responsable +
            '&RUC=' + RUC +
            '&DNIResponsable=' + DNIResponsable +
            '&idZona=' + idZona,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            console.log(data);
            var obj = JSON.parse(data);
            listarClientes();
            if (obj == 1) {
                alert('Cliente editado con exito.');
                cancelar();

            } else if (obj == 4) {
                alert('RUC ya existe.');
            } else {
                alert('No se realizaron modificaciones.');
            }
        },
        error: function(data) {
            alert('Error al editar');
        }
    });
}

function addPuntos() {
    var puntos = txtCantidad.value;
    var param_opcion = 'addPuntos';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion +
            '&idClienteM=' + idClienteAumentar +
            '&Puntos=' + puntos,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj == 1) {

                listarClientes();
                txtCantidad.value = "";
            } else {
                alert('Error al agregar puntos');
            }
            $("#puntosModalCenter").modal("hide");
        },
        error: function(data) {
            alert('Error al agregar puntos');
        }
    });


}

function eliminarCliente(idCliente) {

    var param_opcion = 'deleteCliente';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idClienteM=' + idCliente,
            url: '../../controlador/cliente/cCliente.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                if (obj == 1) {

                    listarClientes();

                } else {
                    alert('Error al eliminar');
                }

            },
            error: function(data) {
                alert('Error al eliminar');
            }
        });
    }

}

function alCargarDocumento() {
    listarClientes();
    btnAumentar.addEventListener("click", addPuntos);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);