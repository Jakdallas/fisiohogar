<?php 
    session_start();  
    if(!isset($_SESSION['S_Cargo']) )
    {
      if($_SESSION['S_Cargo']!='Admin'){
        header("Location:../../index.php");   
      }
    }
    else
    {
      date_default_timezone_set('America/Lima');
    }
 ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SISTEMA | Mantenedor</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Jquery UI -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
    <!-- AdminLTE Skins -->
    <link rel="stylesheet" href="../../assets/css/_all-skins.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.min.css">
    <!-- Main style -->
    <link rel="stylesheet" href="../../assets/css/min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../../assets/css/select2.min.css">
    <link rel="stylesheet" href="../../css/personal/personal.css">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- Loader -->
    <link rel="stylesheet" href="../../css/loader.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Loading (remove the following to stop the loading)-->
    <div class="overlay" id="overlay__loading">
        <div style="padding-left: calc(50% - 40px);padding-top: 25%;">
            <i class="fa fa-refresh fa-spin" style="color: white; font-size:80px"></i>
        </div>
    </div>
    <!-- end loading -->
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- top menu -->
            <?php 
      require('../menus/topNavigation.php');
     ?>
            <!-- /top menu -->
        </header>

        <!-- Columna vertical -->
        <aside class="main-sidebar">
            <section class="sidebar">

                <!-- top menu -->
                <?php 
        require('../menus/topMenu.php');
      ?>
                <!-- /top menu -->

                <!-- sidebar menu -->
                <?php 
        require('../menus/sideMenu.php');
      ?>
                <!-- /sidebar menu -->
            </section>
        </aside>

        <!-- ***** Contenido de la página *****-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Personal
                    <small>Mantenedor</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a href="#">Mantenedor</a></li>
                    <li class="active">Personal</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">

                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">Consultar</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Nuevo</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1" style="overflow-x: auto;">
                                    <table id="datatable-Personal" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nro</th>
                                                <th>Documento</th>
                                                <th>Nombres</th>
                                                <!-- <th>Correo</th> -->
                                                <th>Telefóno</th>
                                                <th>Cargo</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodytable-Personal">

                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    <form class="form-horizontal box-body">
                                        <div class="form-group">
                                            <label for="txtNombres" class="col-sm-2 control-label">Nombres:</label>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="txtNombres"
                                                    placeholder="Nombres">
                                            </div>
                                            <label for="txtApePaterno" class="col-sm-1 control-label">Ap.
                                                Paterno:</label>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="txtApePaterno"
                                                    placeholder="Ap. paterno">
                                            </div>
                                            <label for="txtApeMaterno" class="col-sm-1 control-label">Ap.
                                                Materno:</label>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="txtApeMaterno"
                                                    placeholder="Ap. Materno">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtDNI" class="col-sm-2 control-label">DNI:</label>
                                            <div class="col-sm-2">
                                                <input type="number" class="form-control" id="txtDNI" placeholder="DNI">
                                            </div>
                                            <!-- <label class="col-sm-2 control-label">Zona</label>
                  <div class="col-sm-3">
                     <select id="cboZona" class="form-control select2" style="width:100%;">
                     </select>
                  </div>  -->
                                            <label for="txtTelefono" class="col-sm-1 control-label">Telefono:</label>
                                            <div class="col-sm-2">
                                                <input type="number" class="form-control" id="txtTelefono"
                                                    placeholder="Telefono">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtCargo" class="col-sm-2 control-label">Cargo:</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="txtCargo"
                                                    placeholder="Cargo">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="cbxAdmin" class="col-sm-2 control-label">Administrador:</label>
                                            <div class="col-sm-6">
                                                <input type="checkbox" id="cbxAdmin" class="minimal">
                                                <label>
                                                    ¿El empleado tendra usuario administrador?
                                                </label>
                                            </div>
                                        </div> -->
                                        <div class="box-footer">
                                            <button type="button" class="btn btn-info btn-md"
                                                id="btnRegistrar">Registrar</button>
                                            <!-- <button type="button"  class="btn btn-success btn-md" id="btnEditar">Guardar cambios</button> -->
                                            <button type="button" class="btn btn-danger btn-md"
                                                id="btnCancelar">Cancelar</button>
                                        </div>

                                    </form>


                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>
                    <!-- /.col -->
                </div>

            </section>

            <!-- /.content -->
        </div>
        <!-- ***** Fin del contenido de la página *****-->
        <div class="modal fade" id="updateModalCenter" tabindex="-1" role="dialog" aria-labelledby="updateModalCenter"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Actualizar datos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="txtNombresUp" class="col-sm-2 control-label">Nombres:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="txtNombresUp" placeholder="Nombres">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="txtApePaternoUp" class="col-sm-2 control-label">Ap. Paterno:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="txtApePaternoUp" placeholder="Ap. paterno">
                            </div>
                            <label for="txtApeMaternoUp" class="col-sm-2 control-label">Ap. Materno:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="txtApeMaternoUp" placeholder="Ap. Materno">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="txtDNIUp" class="col-sm-2 control-label">DNI:</label>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" id="txtDNIUp" placeholder="DNI">
                            </div>
                            <label for="txtTelefonoUp" class="col-sm-2 control-label">Telefono:</label>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" id="txtTelefonoUp" placeholder="Telefono">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="txtCargoUp" class="col-sm-2 control-label">Cargo:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="txtCargoUp" placeholder="Cargo">
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            id="btnCloseUp">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnSave">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer content -->
        <?php 
    require('../menus/footerContent.php');
  ?>
        <!-- /footer content -->

    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../assets/js/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../assets/js/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../assets/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../assets/js/demo.js"></script>
    <!-- DataTables -->
    <script src="../../assets/js/jquery.dataTables.min.js"></script>
    <script src="../../assets/js/dataTables.bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../../assets/js/select2.full.min.js"></script>
    <!-- Capa JS -->
    <script src="../js/personal/personal.js"></script>


</body>

</html>