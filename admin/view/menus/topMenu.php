<div class="user-panel">
    <div class="pull-left image">
        <img src="../../assets/img/logo-white.png" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
        <p><?php echo utf8_decode($_SESSION['S_Usuario'])?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>