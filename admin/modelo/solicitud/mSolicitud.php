<?php
session_start();
include_once("../../config/conexion.php");

class Solicitud_model extends Conexion
{
	private $param = array();
	private $con;
	public function __construct(){
		parent::Conexion();
	}

	public function gestionar($param)
	{
		$this->param = $param;
		switch ($this->param['opcion']) {
			case 'insertSolCita':
				echo $this->solicitudCita();
				break;
			case 'inicioSesionCliente':
				echo $this->inicioSesionCliente();
				break;
			case 'listSolicitud':
				echo $this->listarSolicitud();
				break;
			case 'listEstado':
				echo $this->listarEstado();
				break;
			case 'editSolicitud':
				echo $this->editarSolicitud();
				break;
			case 'delete':
				echo $this->eliminar();
				break;
			case 'llenarcboRol':
				echo $this->llenarcboRol();
				break;
		}
	}

	private function solicitudCita(){
		$Nombre = $this->param['Nombre'];
		$Ciudad = $this->param['Ciudad'];
		$DNI = $this->param['DNI'];
		$Email = $this->param['Email'];
		$Telefono = $this->param['Telefono'];
		$Text = $this->param['Text'];

		$sql="INSERT solicitud_citas(nombre,DNI,ciudad,email,telefono,mensaje) values(?,?,?,?,?,?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$Nombre,$DNI,$Ciudad,$Email,$Telefono,$Text]);
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }   

	}

	private function inicioSesion()
	{
		$usuario = $this->param['Usuario'];
		$clave = $this->param['Clave'];
		$sql = "SELECT COUNT(*) from usuario u WHERE u.user='$usuario'
			and u.pass='$clave' ";
		$sentencia = $this->conexion_db->query($sql);
		if ($sentencia->fetchColumn() > 0) {
			$sql2 = "SELECT u.user,u.pass,p.* from personal p INNER JOIN usuario u ON p.id_personal = u.id_personal WHERE u.user='$usuario'
				and u.pass='$clave' limit 1";
			$sentencia = $this->conexion_db->query($sql2);
			$sentencia->execute();
			$resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
			$sentencia->closeCursor();
			$this->conexion_db = null;
			if (count($resultado) == 0) {
				return '4';
			} else {
				foreach ($resultado as $key => $v) {
					$_SESSION['S_IdUsuario'] = utf8_encode($v["user"]);
					$_SESSION['S_Usuario'] = utf8_encode($v["nombre"] . " " . $v["apellido_p"] . " " . $v["apellido_m"]);
					$_SESSION['S_Cargo'] = utf8_encode($v["cargo"]);
					$_SESSION['S_Contraseña'] = utf8_encode($v["pass"]);
				}
				return '1';
			}
		} else {
			return '0';
		}
	}
	private function inicioSesionCliente()
	{
		$usuario = $this->param['Usuario'];
		$clave = $this->param['Clave'];
		$sql = "SELECT COUNT(*) from configuration c WHERE c.admin='$usuario'
			and c.password='$clave' ";
		$sentencia = $this->conexion_db->query($sql);
		if ($sentencia->fetchColumn() > 0) {
			$sql2 = "SELECT *, 'Administrador' as cargo from configuration c WHERE c.admin='$usuario' 
				and c.password='$clave' limit 1";
			$sentencia = $this->conexion_db->query($sql2);
			$sentencia->execute();
			$resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
			$sentencia->closeCursor();
			$this->conexion_db = null;
			if (count($resultado) == 0) {
				return '4';
			} else {
				foreach ($resultado as $key => $v) {
					$_SESSION['S_IdUsuario'] = utf8_encode($v["admin"]);
					$_SESSION['S_Usuario'] = utf8_encode($v["nombre"]);
					$_SESSION['S_Cargo'] = utf8_encode($v["cargo"]);
				}
				return '1';
			}
		} else {
			return '0';
		}
	}
	private function listarSolicitud()
	{
		$sql = "SELECT sc.idcita, sc.nombre, sc.DNI, sc.email, sc.ciudad, sc.telefono, sc.mensaje, ec.idestado, ec.descripcion, sc.fecha_registro, sc.fecha_actualizacion from solicitud_citas sc 
				left join estado_cita ec on ec.idestado = sc.estado
				order by sc.fecha_registro desc ";
		$sentencia = $this->conexion_db->prepare($sql);
		$sentencia->execute();
		$resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
		$sentencia->closeCursor();
		$this->conexion_db = null;
		return json_encode($resultado);
	}

	
	private function listarEstado()
	{
		$sql = "SELECT * FROM estado_cita
				order by idestado ";
		$sentencia = $this->conexion_db->prepare($sql);
		$sentencia->execute();
		$resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
		$sentencia->closeCursor();
		$this->conexion_db = null;
		return json_encode($resultado);
	}

	private function insert()
	{
		$Usuario = $this->param["Usuario"];
		$Clave = $this->param["Clave"];
		$Cargo = $this->param["Cargo"];
		$sql = "SELECT COUNT(*) from usuario u WHERE u.Usuario='$Usuario' and u.Activado <> 0";
		$sentencia = $this->conexion_db->query($sql);
		if ($sentencia->fetchColumn() == 0) {
			$sql = "INSERT usuario(Usuario,Clave,Cargo) values(?,?,?)";
			$stmt = $this->conexion_db->prepare($sql);
			$stmt->execute([$Usuario, $Clave, $Cargo]);
			$this->conexion_db = null;
			return json_encode(1);
		} else {
			$this->conexion_db = null;
			return json_encode(0);
		}
	}
	private function editarSolicitud()
	{
		$idCita = $this->param["idCita"];
		$idEstado = $this->param["idEstado"];
		$date = date('Y-m-d H:i:s');

		$sql = "UPDATE solicitud_citas set estado='$idEstado', fecha_actualizacion='$date' where idcita=$idCita";
		$stmt = $this->conexion_db->prepare($sql);
		$stmt->execute();
		if ($stmt->rowCount() > 0) {
			$this->conexion_db = null;
			return json_encode(1);
		} else {
			$this->conexion_db = null;
			return json_encode(0);
		}
	}
	private function eliminar()
	{
		$idUsuario = $this->param["idUsuario"];
		$sql = "UPDATE usuario set Activado = '0' where idUsuario=$idUsuario";
		$stmt = $this->conexion_db->prepare($sql);
		$stmt->execute();
		if ($stmt->rowCount() > 0) {
			$this->conexion_db = null;
			return json_encode(1);
		} else {
			$this->conexion_db = null;
			return json_encode(0);
		}
	}
	private function llenarcboRol()
	{
		global $database;
		$sql = "exec sp_mnt_Usuario @peticion=1";
		$database->setQuery($sql);
		$rows = $database->loadObjectList();
		$combo = "<option value='0' disabled='disabled' selected='selected'>Seleccionar</option>";
		foreach ($rows as $key => $v) {
			$rol = utf8_encode($v->rol);
			$combo .= "<option value='" . $v->idRol . "'>" . $rol . "</option>";
		}
		return $combo;
	}
}
