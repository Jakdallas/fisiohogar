<?php
session_start();

include_once( "../../config/conexion.php");

class Personal_model extends Conexion{
    private $param = array();
    public $con;
    public function __construct(){
        parent:: Conexion();
    }
    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listPersonal':
                echo $this->listarPersonal();
                break;
			case 'listarUsuario':
				echo $this->listarUsuario();
                break;
            case 'createPersonal':
            	echo $this->createPersonal();
                break;
            case 'updatePersonal':
                echo $this->actualizarPersonal();
                break;
            case 'obtenerPersonal':
                echo $this->obtenerPersonal();
                break;
            case 'validarCliente':
                echo $this->validarCliente(); 
                break;
            case 'deletePersonal':
                echo $this->eliminarPersonal();
                break;
        }
    }

    private function obtenerPersonal(){
    	$idPersonal=$this->param["idPersonal"];
        $sql="SELECT p.id_personal as idPersonal, p.dni, p.nombre, p.apellido_p as apaterno, p.apellido_m as amaterno,
         'ejemplo@ejemplo.com' as correo, p.telefono, p.cargo 
            from personal p where id_personal = '$idPersonal' LIMIT 1";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        if ($fila = $sentencia->fetch(PDO::FETCH_ASSOC)) {
            $resultado = $fila;
        }
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function listarUsuario(){
		$sql="SELECT U.idusuario  ,U.user, U.id_personal, U.admin, U.activo, p.nombre, p.apellido_p, 
                     p.apellido_m, p.cargo, p.DNI, p.telefono, p.activo as activo_personal
					from usuario U 
					inner join personal p
					on p.id_personal = u.id_personal
					where U.activo = '1' order by U.idusuario ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
	}

    private function listarPersonal(){
    	$sql="SELECT p.id_personal as id_personal, p.dni, p.nombre, p.apellido_p as apaterno, p.apellido_m as amaterno,
        'ejemplo@ejemplo.com' as correo, p.telefono, p.cargo 
           from personal p where activo = '1' order by p.id_personal";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function createPersonal(){
        $Dni=$this->param["dni"];
        $Nombres=$this->param["Nombres"];
        $Apat=$this->param["APaterno"];
        $Amat=$this->param["AMaterno"];
        /*$Ciudad=$this->param["Ciudad"];
        $Correo=$this->param["Correo"];*/
        $Telefono=$this->param["Telefono"];
        $Cargo=$this->param["Cargo"];
        $sql="INSERT personal(dni,nombre,apellido_p,apellido_m,telefono,cargo) values(?,?,?,?,?,?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$Dni,$Nombres,$Apat,$Amat,$Telefono,$Cargo]);
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }   
    }        
    private function actualizarPersonal(){

        $Dni=$this->param["dni"];
        $Nombres=$this->param["Nombres"];
        $Apat=$this->param["APaterno"];
        $Amat=$this->param["AMaterno"];
        $Telefono=$this->param["Telefono"];
        $Cargo=$this->param["Cargo"];
        $idPersonal=$this->param["idPersonal"];

        // $sql="SELECT COUNT(*) from cliente c WHERE c.RUC='$RUC' and c.idCliente <> $idCliente";
        // $sentencia=$this->conexion_db->query($sql);
        //  if ($sentencia->fetchColumn()==0) {
            $sql="UPDATE personal set nombre = '$Nombres', apellido_p = '$Apat', apellido_m = '$Amat', dni = '$Dni', telefono = '$Telefono',
                   cargo = '$Cargo' where id_personal = '$idPersonal' ";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
        //  }else{
        //     $this->conexion_db=null;
        //     return json_encode(4); 
        // }        
    }
    private function eliminarPersonal(){
    	$idPersonal=$this->param["idPersonal"];
        $sql="UPDATE personal SET activo=0 WHERE id_personal =$idPersonal";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }

    private function validarCliente(){
        $idCliente=$this->param["idCliente"];
        if ($idCliente != null ){
            return json_encode(1); 
        }else{
            return json_encode(0); 
        }
    }
    
    private function listarClienteBySession(){
        $idCliente=$this->param["idCliente"];
        if($idCliente == "" && isset( $_SESSION['S_Usuario'] ) ){
            $idCliente = $_SESSION['S_IdUsuario'];
        }
        if ($idCliente == "admin"){
            $sql="SELECT *
                from cliente c where activo = 1 and idcliente=1  ";
            $sentencia=$this->conexion_db->prepare($sql);
            $sentencia->execute();
            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
            $sentencia->closeCursor();
            $this->conexion_db=null;
            return json_encode($resultado); 
        }else if($idCliente != ""){
            $sql="SELECT *
                from cliente c where activo = 1 and idcliente=$idCliente ";
            $sentencia=$this->conexion_db->prepare($sql);
            $sentencia->execute();
            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
            $sentencia->closeCursor();
            $this->conexion_db=null;
            return json_encode($resultado);  
        }else {
            return json_encode(0); 
        }
    	
    }
}
