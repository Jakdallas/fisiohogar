<?php
session_start();

include_once( "../../config/conexion.php");

class TipoDocumento_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listTipoDocumento':
                echo $this->listarTipoDocumento();
                break;
        }
    }

    private function listarTipoDocumento(){
    	$sql="SELECT idTipoPago, TipoPago from tipopago";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }
}
?>