<?php
session_start();

include_once( "../../config/conexion.php");

class Letra_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listLetra':
                echo $this->listarLetra();
                break;
            case 'insertLetra':
            	echo $this->insertarLetra();
                break;
            case 'insertPago':
                echo $this->registrarPago();
                break;
            case 'insertExtorno':
                echo $this->registrarExtorno();
                break;
            case 'searchByParameters':
                echo $this->listarLetraByParameters();
                break;
            case 'imprimir':
                echo $this->imprimir();
                break;
        }
    }

    private function listarLetra(){
    	$sql="SELECT l.idLetra,l.NroLetra, l.FechaEmision, 
        l.Plazo,l.FechaVencim, l.Estado, l.FechaPago, 
        l.NroDocPago, l.idMedioPago, l.idUsuario,l.Lugar, 
        l.ObservPago, e.Fecha as Fechaextorno,
        e.Observacion,u.usuario from letra l 
        left join extorno e 
        on e.idLetra=l.idLetra 
        inner join usuario u 
        on u.idusuario = l.idusuario";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function listarLetraByParameters(){
        $fechaInicial=$this->param["fechaInicial"];
        $fechaFinal=$this->param["fechaFinal"];
        $idCliente=$this->param["idCliente"];
        //$Estado=$this->param["Estado"];

        $DateFechaI= strtotime($fechaInicial); 
        $DateFechaI = date('Y-m-d',$DateFechaI);

        $DateFechaF = strtotime($fechaFinal); 
        $DateFechaF = date('Y-m-d',$DateFechaF);
        
        $sql="SELECT v.idventa,c.nombres,c.cmp,c.ecografo_modelo,c.ecografo_marca,v.monto,v.puntos,v.fecha
            from venta v inner join cliente c on c.idcliente=v.idcliente where estado = 1
        and v.fecha between '$DateFechaI' and '$DateFechaF'
        and c.idcliente = $idCliente
        order by v.idventa,v.fecha
        ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function insertarLetra(){
        $NroLetra=$this->param["NroLetra"];
        $FechaEmision=$this->param["FechaEmision"];
        $Plazo=$this->param["Plazo"];
        $Lugar=$this->param["Lugar"];        
        $Girador=$this->param["Girador"];
        $FechaVencim=$this->param["FechaVencim"];
        $idUsuario=$_SESSION['S_IdUsuario'];
        $oDocumento=$this->param["olDocumento"];
        $olDocumento=explode(",",$oDocumento);
        
        $DateFechaE= strtotime($FechaEmision); 
        $DateFechaE = date('Y-m-d',$DateFechaE);

        $DateFechaV = strtotime($FechaVencim); 
        $DateFechaV = date('Y-m-d',$DateFechaV);
        
        $sql="SELECT COUNT(*) from letra l WHERE l.NroLetra='$NroLetra'";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==0) {
            $stmt;
            //return json_encode($idUsuario); 
            $sql="INSERT letra(NroLetra,FechaEmision,Plazo,FechaVencim,Estado,idUsuario,Lugar,Girador) values(?,?,?,?,?,?,?,?)";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute([$NroLetra,$DateFechaE,$Plazo,$DateFechaV,"0",$idUsuario,$Lugar,$Girador]);
            $sql="SELECT @@identity as id";
            $sentencia=$this->conexion_db->prepare($sql);
            $sentencia->execute();
            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);

            $idLetra = $resultado[0]["id"];
            foreach ($olDocumento as $clave => $valor) {
                //return json_encode($idLetra);
                $idDoc=intval($valor);
                $idLet=intval($idLetra);
                $sql="INSERT documletra(idDocumento,idLetra,Activado) values(?,?,?)";
                $stmt= $this->conexion_db->prepare($sql);
                $stmt->execute([$idDoc,$idLet,1]);

                $sql="UPDATE documento set Estado=1 where idDocumento=$idDoc";
                $stmt= $this->conexion_db->prepare($sql);
                $stmt->execute();
            }
            $this->conexion_db=null;
            return json_encode(1); 
        }    
        else{
            $this->conexion_db=null;
            return json_encode(0); 
        }      
    }    

    private function registrarPago(){
        $FechaPago=$this->param["FechaPago"];
        $idLetra=$this->param["idLetra"];
        $Observacion=$this->param["Observacion"];

        $DateFecha = strtotime($FechaPago); 
        $DateFecha = date('Y-m-d',$DateFecha);

        $sql="SELECT COUNT(*) from letra l WHERE l.idLetra='$idLetra'";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()>0) {
            $sql="UPDATE letra set ObservPago='$Observacion',FechaPago='$DateFecha',Estado=1 where idLetra=$idLetra and Activado=1";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();

            //ACTUALIZACION DE ESTADO DE DOCUMENTO -> PAGADO REMOVIDO SOLO SE CONSIDERAN 3 ESTADOS (PENDIENTE,CANJEADO,ANULADO
            //$sql="UPDATE documento inner join documletra on documento.iddocumento=documletra.iddocumento set documento.Estado=2 where documletra.idLetra=$idLetra and documletra.Activado=1";
            //$stmt= $this->conexion_db->prepare($sql);
            //$stmt->execute();

            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
        }    
        else{
            $this->conexion_db=null;
            return json_encode(0); 
        }      
    }    

    private function registrarExtorno(){
        $FechaExtorno=$this->param["FechaExtorno"];
        $idLetra=$this->param["idLetra"];
        $Observacion=$this->param["Observacion"];

        $DateFecha = strtotime($FechaExtorno); 
        $DateFecha = date('Y-m-d',$DateFecha);
        
        $sql="SELECT COUNT(*) from extorno e WHERE e.idLetra='$idLetra'";
        $sentencia=$this->conexion_db->query($sql);
        
        if ($sentencia->fetchColumn()==0) {
            $stmt;
            $sql="INSERT extorno(Fecha,Observacion,idLetra) values(?,?,?)";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute([$DateFecha,$Observacion,$idLetra]);

            $sql="UPDATE letra set Estado=2 where idLetra=$idLetra and Activado=1";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();

            //ACTUALIZACION DE ESTADO DE DOCUMENTO -> EXTORNADO REMOVIDO SOLO SE CONSIDERAN 3 ESTADOS (PENDIENTE,CANJEADO,ANULADO)
            //$sql="UPDATE documento inner join documletra on documento.iddocumento=documletra.iddocumento set documento.Estado=3 where  documletra.idLetra=$idLetra and documletra.Activado=1";
            //$stmt= $this->conexion_db->prepare($sql);
            //$stmt->execute();

            $this->conexion_db=null;

            return json_encode(1); 
        }    
        else{
            $this->conexion_db=null;
            return json_encode(0); 
        }      
    }    


    private function imprimir(){
        $idCliente=$this->param["idCliente"];
        $fechaInicial=$this->param["fechaInicial"];
        $fechaFinal=$this->param["fechaFinal"];

        $DateFechaI= strtotime($fechaInicial); 
        $DateFechaI = date('Y-m-d',$DateFechaI);

        $DateFechaF = strtotime($fechaFinal); 
        $DateFechaF = date('Y-m-d',$DateFechaF);
        
        $sql="SELECT v.idventa,c.nombres,c.cmp,c.ecografo_modelo,c.ecografo_marca,v.monto,v.puntos,v.fecha
            from venta v inner join cliente c on c.idcliente=v.idcliente where estado = 1
        and v.fecha between '$DateFechaI' and '$DateFechaF'
        and c.idcliente = $idCliente
        order by v.fecha";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;

        $array=[];
        $count = 0;
        $nl;
        for ($i = 1; $i <= count($resultado); $i++) {

            if ($i == count($resultado)){
                $nl='0';
              }else{
                $nl=$resultado[$i]['idventa'];
              }

            if ($resultado[$i-1]['idventa']== $nl){
                $array [$i-1]= array(
                    "nombres" => $resultado[$i-1]['nombres'],
                    "cmp" =>$resultado[$i-1]['cmp'],
                    "ecografo_modelo" => $resultado[$i-1]['ecografo_modelo'],
                    "monto"=>$resultado[$i-1]['monto'],
                    "puntos"=>$resultado[$i-1]['puntos'],
                    "idventa"=>$resultado[$i-1]['idventa'],
                    "fecha"=>date_format(date_create($resultado[$i-1]['fecha']), 'd/m/Y'),
                    "ecografo_marca"=>$resultado[$i-1]['ecografo_marca'],
                    "flagLetra" => ''
                );
            }else{
                $array [$i-1]= array(
                    "nombres" => $resultado[$i-1]['nombres'],
                    "cmp" =>$resultado[$i-1]['cmp'],
                    "ecografo_modelo" => $resultado[$i-1]['ecografo_modelo'],
                    "monto"=>$resultado[$i-1]['monto'],
                    "puntos"=>$resultado[$i-1]['puntos'],
                    "idventa"=>$resultado[$i-1]['idventa'],
                    "fecha"=>date_format(date_create($resultado[$i-1]['fecha']), 'd/m/Y'),
                    "ecografo_marca"=>$resultado[$i-1]['ecografo_marca'],
                    "flagLetra" => 'X'
                );
            }
                
          
            
        }
        
        return json_encode($array);


    }
  
}
    /*
    
      foreach($resultado as $key => $v) {

            $array [$count]= array(
                "RazonSocial" => ($v["RazonSocial"]),
                "TipoPago" =>utf8_encode($v["TipoPago"]),
                "Numero" => utf8_encode($v["Numero"]),
                "Monto"=>utf8_encode($v["Monto"]),
                "letra"=>utf8_encode($v["letra"]),
                "NroLetra"=>utf8_encode($v["NroLetra"]),
                "fechaEmision"=>$v["fechaEmision"],
                "plazo"=>$v["plazo"],
                "fechaVencim"=>$v["fechaVencim"],
                "flagLetra" => "X"
            );
            $count = $count + 1; 

        }
    
    
    
    
    
    
    
    */
        

?>

