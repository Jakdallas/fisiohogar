<?php
session_start();

include_once( "../../config/conexion.php");

class Steps_model extends Conexion{
    private $param = array();
    public $con;

    public function __construct(){
      parent::Conexion();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listSteps':
                echo $this->listarSteps();
                break;
            case 'insertSteps':
            	echo $this->insertarSteps();
                break;
            case 'updateSteps':
                echo $this->actualizarSteps();
                break;
            case 'deleteSteps':
                echo $this->eliminarSteps();
                break;
        }
    }

    private function listarSteps(){
    	$sql="SELECT *
            from servicios s where activo = 1 order by s.idservicio ";
        $sentencia=$this->conexion_db->query($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function insertarSteps(){
        $Descripcion=$this->param["Descripcion"];
        
        $sql="INSERT servicios(descripcion) values(?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$Descripcion]);
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }   
    }    

    private function actualizarSteps(){
        $idStep=$this->param["idStep"];
        $Descripcion=$this->param["Descripcion"];
        
        $sql="UPDATE servicios set descripcion='$Descripcion' where idservicio=$idStep";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
        
    }

    private function eliminarSteps(){
    	$idStep=$this->param["idStep"];
        $sql="UPDATE steps set activo=0 where idstep=$idStep";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }
}
?>