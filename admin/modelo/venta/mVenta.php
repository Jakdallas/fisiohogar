<?php
session_start();

include_once( "../../config/conexion.php");
include_once( "culqi/culqi.php");

class Venta_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listVenta':
                echo $this->listarVentas();
                break;
            case 'insertVenta':
            	echo $this->insertarVenta();
                break;
            case 'updatePricing':
                echo $this->actualizarPricing();
                break;
            case 'deletePricing':
                echo $this->eliminarPricing();
                break;
            case 'processVenta':
                echo $this->procesarVenta();
                break;
        }
    }

    private function listarVentas(){
    	$sql="SELECT v.idventa,c.nombres,c.cmp,c.ecografo_modelo,c.ecografo_marca,v.monto,v.puntos,v.fecha
            from venta v inner join cliente c on c.idcliente=v.idcliente where estado = 1 order by v.idventa,v.fecha";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function insertarVenta(){
        
        $idCliente=$this->param["idCliente"];
        $Monto=$this->param["Monto"];
        $Puntos=$this->param["Puntos"];

        $stmt;
        $sql="INSERT pricing_page(idcliente,monto,puntos) values(?,?,?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$idCliente,$Monto,$Puntos]);
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }    

    private function actualizarPricing(){

        $idPrecio=$this->param["idPrecio"];
        $Nombre=$this->param["Nombre"];
        $Precio=$this->param["Precio"];
        $Puntos=$this->param["Puntos"];
        $Orden=$this->param["Orden"];

        $sql="SELECT COUNT(*) from pricing_page WHERE idprecio=$idPrecio";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==1) {
            $sql="UPDATE pricing_page set nombre='$Nombre',precio='$Precio',puntos='$Puntos',orden='$Orden' where idprecio=$idPrecio";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
         }else{
            $this->conexion_db=null;
            return json_encode(4); 
        } 

        

        
    }
    private function eliminarPricing(){
        
        $idVenta=$this->param["idVenta"];
        
        $sql="UPDATE venta SET estado=0 WHERE idventa=$idVenta";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    } 
    
    private function procesarVenta(){
        $card_number=$this->param["card_number"];
        $cvv=$this->param["cvv"];
        $email=$this->param["email"];
        $expiration_month=$this->param["expiration_month"];
        $expiration_year=$this->param["expiration_year"];
        $amount=$this->param["amount"];
        $description=$this->param["description"];
        $Puntos=$this->param["Puntos"];
        $idCliente=$this->param["idCliente"];

        if($idCliente == "" && isset( $_SESSION['S_Usuario'] ) ){
            $idCliente = $_SESSION['S_IdUsuario'];
        }
        $stmt;
        $sql="INSERT venta(idcliente,monto,puntos) values(?,?,?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$idCliente,$Monto,$Puntos]);
        if ($stmt->rowCount()>0) {
            $sql="UPDATE cliente SET puntos=puntos + $Puntos  WHERE idCliente=$idCliente";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();

            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }
}
?>