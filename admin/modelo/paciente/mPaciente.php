<?php
session_start();

include_once( "../../config/conexion.php");

class Paciente_model extends Conexion{
    private $param = array();
    public $con;

    public function __construct(){
      parent::Conexion();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listCliente':
                echo $this->listarCliente();
                break;
            case 'insertCliente':
            	echo $this->insertarCliente();
                break;
            case 'insertCita':
            	echo $this->insertarCita();
                break;
            case 'listCita':
            	echo $this->listarCita();
                break;
            case 'updateCliente':
                echo $this->actualizarCliente();
                break;
            case 'updateAntecedentes':
                echo $this->actualizarAntecedentes();
                break;
            case 'deleteCliente':
                echo $this->eliminarCliente();
                break;
            case 'getCliente':
                echo $this->obtenerCliente();
                break;
            case 'listClienteBySession':
                echo $this->listarClienteBySession();
                break;
            case 'validateCliente':
                echo $this->validarCliente();
                break;
            case 'initSesion':
                echo $this->inicioSesion();
                break;
            case 'searchDNI':
                echo $this->busquedaDNI();
                break;
        }
    }

    private function listarCliente(){
    	$sql="SELECT c.idCliente, c.documento, c.nombres, c.cmp, c.ecografo_modelo, c.ecografo_marca, c.ciudad, c.correo, c.telefono, c.puntos 
            from cliente c where activo = 1 order by idCliente ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function obtenerCliente(){
        $Documento=$this->param["Documento"];
        $Nombre=$this->param["Nombres"];
        $apellidoPaterno=$this->param["apellidoPaterno"];
        $apellidoMaterno=$this->param["apellidoMaterno"];

        $sql="SELECT * from paciente p WHERE p.documento ='$Documento' ";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();
         if ($stmt->rowCount()==0) {
            $sql="INSERT paciente(documento,nombres,apellidoPaterno,apellidoMaterno) values(?,?,?,?)";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute([$Documento,$Nombre,$apellidoPaterno,$apellidoMaterno]);
            if ($stmt->rowCount()>0) {
                $sql="INSERT antecedentes(documento) values(?)";
                $stmt= $this->conexion_db->prepare($sql);
                $stmt->execute([$Documento]);
                if ($stmt->rowCount()>0) {
                    $sql="SELECT DISTINCT p.documento, p.nombres, p.apellidoPaterno, p.apellidoMaterno,
                                 p.correo, p.telefono, a.patologicas, a.quirurgicas, a.alergicas,
                                 a.familiares, a.epidemiologicos, a.otros, a.examenes, sc.email, 
                                 sc.telefono AS telefono2
                            FROM paciente p 
                            LEFT JOIN antecedentes a
                            ON a.documento = p.documento
                            LEFT JOIN solicitud_citas sc
                            ON sc.DNI = p.documento
                            where p.documento ='$Documento' and p.activo = 1 ";
                    $sentencia=$this->conexion_db->prepare($sql);
                    $sentencia->execute();
                    $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
                    $sentencia->closeCursor();
                    $this->conexion_db=null;
                    return json_encode($resultado);  
                }
            }/*else{
                $this->conexion_db=null;
                return json_encode(0); 
            }   */
        }else{
            $sql="SELECT DISTINCT p.documento, p.nombres, p.apellidoPaterno, p.apellidoMaterno, p.direccion,
                        p.correo, p.telefono, a.patologicas, a.quirurgicas, a.alergicas,
                        a.familiares, a.epidemiologicos, a.otros, a.examenes, sc.email, 
                        sc.telefono AS telefono2
                FROM paciente p 
                LEFT JOIN antecedentes a
                ON a.documento = p.documento
                LEFT JOIN solicitud_citas sc
                ON sc.DNI = p.documento
                where p.documento ='$Documento' and p.activo = 1 ";
            $sentencia=$this->conexion_db->prepare($sql);
            $sentencia->execute();
            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
            $sentencia->closeCursor();
            $this->conexion_db=null;
            return json_encode($resultado);  
        }

    }

    private function busquedaDNI(){
        $Documento = $this->param["Documento"];

        $token = 'apis-token-1451.7uhKkp38wpvO9tbtKQhTYJHSJwMXFDkT';
        $dni = $Documento;

        // Iniciar llamada a API
        $curl = curl_init();

        // Buscar dni
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.apis.net.pe/v1/dni?numero=' . $dni,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 2,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Referer: https://apis.net.pe/consulta-dni-api',
                'Authorization: Bearer ' . $token
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response; 
        // Datos listos para usar
        //$persona = json_decode($response);
        //return $persona;

    }

    private function insertarCliente(){
        $Documento=$this->param["Documento"];
        $Nombres=$this->param["Nombres"];
        $Cmp=$this->param["Cmp"];
        $Ciudad=$this->param["Ciudad"];
        $Modelo=$this->param["Modelo"];
        $Marca=$this->param["Marca"];
        $Correo=$this->param["Correo"];
        $Clave=$this->param["Clave"];
        $Telefono=$this->param["Telefono"];
        $Puntos=$this->param["Puntos"];
        
        $sql="INSERT cliente(documento,nombres,cmp,ciudad,ecografo_modelo,ecografo_marca,correo,clave,telefono,puntos) values(?,?,?,?,?,?,?,?,?,?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$Documento,$Nombres,$Cmp,$Ciudad,$Modelo,$Marca,$Correo,$Clave,$Telefono,$Puntos]);
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }   
    }    

    private function actualizarCliente(){

        $Documento=$this->param["Documento"];
        $Direccion=$this->param["Direccion"];
        $Correo=$this->param["Correo"];
        $Telefono=$this->param["Telefono"];

        $sql="SELECT COUNT(*) from paciente c WHERE c.documento='$Documento' ";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()>0) {
            $sql="UPDATE paciente set direccion='$Direccion',correo='$Correo',telefono='$Telefono' where documento ='$Documento' ";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
         }else{
            $this->conexion_db=null;
            return json_encode(4); 
        } 
        
    }

    private function actualizarAntecedentes(){

        $Documento=$this->param["Documento"];
        $patologicas=$this->param["Patologicas"];
        $quirurgicas=$this->param["Quirurgicas"];
        $alergicas=$this->param["Alergicas"];
        $familiares=$this->param["Familiares"];
        $epidemiologicos=$this->param["Epidemiologicos"];
        $otros=$this->param["Otros"];
        $examenes=$this->param["Examenes"];
        //$fechaupdate=$this->param["fechaupdate"];

        $sql="SELECT COUNT(*) from antecedentes a WHERE a.documento='$Documento' ";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==0) {
            $sql="INSERT antecedentes(documento,patologicas,quirurgicas,alergicas,familiares,epidemiologicos,otros, examenes) values(?,?,?,?,?,?,?,?)";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute([$Documento,$patologicas,$quirurgicas,$alergicas,$familiares,$epidemiologicos,$otros,$examenes]);
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }   
         }else{
            $sql="UPDATE antecedentes set patologicas='$patologicas',quirurgicas='$quirurgicas',alergicas='$alergicas',
                    familiares='$familiares',epidemiologicos='$epidemiologicos', otros='$otros', examenes='$examenes' where documento='$Documento' ";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
        }
        
    }

    private function insertarCita(){
        $Documento=$this->param["Documento"];
        $idservicio=$this->param["Servicio"];
        $diagnostico=$this->param["Diagnostico"];
        $tratamiento=$this->param["Tratamiento"];
        $usuario = $_SESSION['S_IdUsuario'];
        $sql="INSERT cita(documento,idservicio ,diagnostico,tratamiento, user) values(?,?,?,?,?)";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute([$Documento,$idservicio ,$diagnostico,$tratamiento, $usuario]);
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }   
    }

    private function listarCita(){
        $Documento=$this->param["Documento"];
        $sql="SELECT c.idcita, c.documento, c.idservicio, c.diagnostico, c.tratamiento, c.user,
                     c.fecha_inicio, s.descripcion, p.nombre, p.apellido_p, p.apellido_m,
                     p.cargo, p.DNI, p.telefono, YEAR(c.fecha_inicio) as YEAR, MONTH(c.fecha_inicio) as MONTH,
                     DAY(c.fecha_inicio) as DAY
                        FROM cita c
                        LEFT JOIN servicios s
                        ON s.idservicio = c.idservicio
                        LEFT JOIN usuario u
                        ON u.user = c.user
                        LEFT JOIN personal p
                        ON p.id_personal = u.id_personal
                        where c.documento ='$Documento' GROUP BY c.idcita, c.documento, c.idservicio, c.diagnostico, c.tratamiento, c.user,
                     c.fecha_inicio, s.descripcion, p.nombre, p.apellido_p, p.apellido_m,
                     p.cargo, p.DNI, p.telefono, YEAR(c.fecha_inicio), MONTH(c.fecha_inicio),
                     DAY(c.fecha_inicio) ORDER BY c.fecha_inicio DESC";
                $sentencia=$this->conexion_db->prepare($sql);
                $sentencia->execute();
                $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
                $sentencia->closeCursor();
                $this->conexion_db=null;
                return json_encode($resultado);  
    }

    private function eliminarCliente(){
    	$idCliente=$this->param["idClienteM"];
        $sql="UPDATE cliente SET activo=0 WHERE idCliente=$idCliente";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }

    private function validarCliente(){
        $idCliente=$this->param["idCliente"];
        if ($idCliente != null ){
            return json_encode(1); 
        }else{
            return json_encode(0); 
        }
    }

    private function inicioSesion(){
		$Correo=$this->param['Correo'];
		$Clave=$this->param['Clave'];
		$sql="SELECT COUNT(*) from cliente c WHERE c.correo='$Correo'
		and c.clave='$Clave' and activo = 1";
		$sentencia=$this->conexion_db->query($sql);
		if ($sentencia->fetchColumn()>0) {
			$sql2="SELECT * from cliente c WHERE c.correo='$Correo'
			and c.clave='$Clave' limit 1";
			$sentencia=$this->conexion_db->query($sql2);
			$sentencia->execute();
       		$resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
			$sentencia->closeCursor();
			$this->conexion_db=null;
			if (count($resultado) == 0) {
				return '4';
			}else{
				foreach ($resultado as $key => $v) {
					$_SESSION['S_IdUsuario']= utf8_encode($v["idcliente"]);
					$_SESSION['S_Usuario']= utf8_encode($v["nombres"]);
					$_SESSION['S_Cargo']= "Cliente";
				}
   				 return json_encode(1);//'1'; 
			}
		}else{
			return '0';
		}
    }
    
    private function listarClienteBySession(){
        $idCliente=$this->param["idCliente"];
        if($idCliente == "" && isset( $_SESSION['S_Usuario'] ) ){
            $idCliente = $_SESSION['S_IdUsuario'];
        }
        if ($idCliente == "admin"){
            $sql="SELECT *
                from cliente c where activo = 1 and idcliente=1  ";
            $sentencia=$this->conexion_db->prepare($sql);
            $sentencia->execute();
            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
            $sentencia->closeCursor();
            $this->conexion_db=null;
            return json_encode($resultado); 
        }else if($idCliente != ""){
            $sql="SELECT *
                from cliente c where activo = 1 and idcliente=$idCliente ";
            $sentencia=$this->conexion_db->prepare($sql);
            $sentencia->execute();
            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
            $sentencia->closeCursor();
            $this->conexion_db=null;
            return json_encode($resultado);  
        }else {
            return json_encode(0); 
        }
    	
    }
}
?>