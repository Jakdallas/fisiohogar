<?php
session_start();

include_once( "../../config/conexion.php");

class Tarifas_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listPricing':
                echo $this->listarPricing();
                break;
            case 'insertPricing':
            	echo $this->insertarPricing();
                break;
            case 'updatePricing':
                echo $this->actualizarPricing();
                break;
            case 'deletePricing':
                echo $this->eliminarPricing();
                break;
        }
    }

    private function listarPricing(){
    	$sql="SELECT *
            from pricing_page where estado = 1 order by orden";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function insertarPricing(){
        
        $Nombre=$this->param["Nombre"];
        $Precio=$this->param["Precio"];
        $Puntos=$this->param["Puntos"];
        $Orden=$this->param["Orden"];

        $stmt;
        $sql="INSERT pricing_page(nombre,precio,puntos,orden) values(?,?,?,?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$Nombre,$Precio,$Puntos,$Orden]);
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }    

    private function actualizarPricing(){

        $idPrecio=$this->param["idPrecio"];
        $Nombre=$this->param["Nombre"];
        $Precio=$this->param["Precio"];
        $Puntos=$this->param["Puntos"];
        $Orden=$this->param["Orden"];

        $sql="SELECT COUNT(*) from pricing_page WHERE idprecio=$idPrecio";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==1) {
            $sql="UPDATE pricing_page set nombre='$Nombre',precio='$Precio',puntos='$Puntos',orden='$Orden' where idprecio=$idPrecio";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
         }else{
            $this->conexion_db=null;
            return json_encode(4); 
        } 

        

        
    }
    private function eliminarPricing(){
        
        $idPrecio=$this->param["idPrecio"];
        
        $sql="UPDATE pricing_page SET estado=0 WHERE idPrecio=$idPrecio";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }
}
?>