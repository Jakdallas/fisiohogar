<?php
session_start();

include_once( "../../config/conexion.php");

class Firma_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listSignatureByLoggedClient':
                echo $this->listSignatureByLoggedClient();
                break;
            case 'insertarFirma':
                echo $this->insertarFirma();
                break;
            case 'eliminarFirma':
            	echo $this->eliminarFirma();
                break;
        }
    }

    private function listSignatureByLoggedClient(){
        
        $idCliente=$this->param["idCliente"] ?: $_SESSION['S_IdUsuario'] ;
        $sql="SELECT idfirma,url 
            FROM firmas f
            WHERE f.idcliente = ?";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute([$idCliente]);
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);
    }    

    private function insertarFirma(){
        $idCliente=$this->param["idCliente"] ?: $_SESSION['S_IdUsuario'];
        $signature_route=$this->param["signature_routes"];
        $server_path = $this->param["server_path"];
        $signature_route=$signature_route."/".(string)$idCliente;
        $img_type = $this->param["img_type"];
        if ($img_type == "file"){
            $file=$this->param["file"];
        }else if($img_type == "b64"){
            $file=$this->param["b64file"];
        }
        //$date = date('Y-m-d H:i:s');
        $url = $this->create_signature_file($file,$img_type,uniqid().'.png',(string)$signature_route);
        if (file_exists($url)) {
            $url = str_replace($server_path,"",$url);
            $stmt;
            $sql="INSERT firmas(idcliente,url) values(?,?)";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute([$idCliente,$url]);
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
        }
    }

    private function eliminarFirma(){
        
        $idFirma=$this->param["idFirma"];
        $sql="SELECT idfirma,url
            FROM firmas f
            WHERE f.idFirma = ?";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute([$idFirma]);
        $resultado=$sentencia->fetch(PDO::FETCH_ASSOC);
        if($resultado["url"] != null){
            $server_path = $this->param["server_path"];
            $store_path = $server_path . $resultado["url"];
            if (!unlink($store_path)){
                //4 no se puede eliminar fichero
                $this->conexion_db=null;
                return json_encode(4); 
            }else{
                $sql="DELETE FROM firmas WHERE idFirma=$idFirma";
                $stmt= $this->conexion_db->prepare($sql);
                $stmt->execute();
                if ($stmt->rowCount()>0) {
                    $this->conexion_db=null;
                    return json_encode(1); 
                }else{
                    $this->conexion_db=null;
                    return json_encode(0); 
                }
            }
        }else{
            //2 No tiene url
            $this->conexion_db=null;
            return json_encode(2);
        }
        
        
    }

    

    function create_signature_file($img_data,$img_data_type,$fileName,$store_path){
        function base64_to_jpeg($base64_string, $output_file) {
            $image_parts = explode(";base64,", $base64_string);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            file_put_contents($output_file, $image_base64);
            return $output_file; 
        }
        function store_file($File, $targetPath,$fileName) {
            if(!empty($File["type"])){
                $valid_extensions = array("jpeg", "jpg", "png");
                $temporary = explode(".", $File["name"]);
                $file_extension = end($temporary);
                if((($File["type"] == "image/png") || ($File["type"] == "image/jpg") || ($File["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
                    $sourcePath = $File['tmp_name'];
                    if(move_uploaded_file($sourcePath,$targetPath)){
                        $uploadedFile = $fileName;
                        return $targetPath;
                    }
                }
            }
        }

        if (!file_exists($store_path)) {
            mkdir($store_path, 0777, true);
        }
        $file_path = (string)$store_path."/".(string)$fileName;
        switch ($img_data_type){
            case "b64":
                return base64_to_img($img_data,$file_path);
                break;
            case "file":
                return store_file($img_data,$file_path,$fileName);
                break;
        }
    }

}
?>