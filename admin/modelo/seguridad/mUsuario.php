<?php 
    session_start();  
	include_once( "../../config/conexion.php");

	class Usuario_model extends Conexion{
		private $param = array();
	    public $con;
	    public function __construct(){
		    parent::Conexion();
	    }	
	    public function gestionar($param){
	    	$this->param = $param;
	    	switch ($this->param['opcion'])
			{
                case 'inicioSesion':
                    echo $this->inicioSesion();
                    break;
				case 'listarUsuario':
					echo $this->listarUsuario();
					break;	
				case 'insert':
					echo $this->insert();
					break;	
				case 'editar':
					echo $this->editar();
					break;	
				case 'delete':
					echo $this->eliminar();
                    break;
                case 'llenarcboRol':
                    echo $this->llenarcboRol();
                    break;
                case 'updatePassword':
                    echo $this->actualizarContra();
                    break;
                case 'resetContra':
                    echo $this->resetContra();
                    break;
					
			}
	    }	
		private function inicioSesion(){
			$usuario = $this->param['Usuario'];
			$clave = $this->param['Clave'];
			$sql = "SELECT COUNT(*) from usuario u WHERE u.user='$usuario'
				and u.pass='$clave' ";
			$sentencia = $this->conexion_db->query($sql);
			if ($sentencia->fetchColumn() > 0) {
				$sql2 = "SELECT u.user,u.pass,u.admin,p.* from personal p INNER JOIN usuario u ON p.id_personal = u.id_personal WHERE u.user='$usuario'
					and u.pass='$clave' limit 1";
				$sentencia = $this->conexion_db->query($sql2);
				$sentencia->execute();
				$resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
				$sentencia->closeCursor();
				$this->conexion_db = null;
				if (count($resultado) == 0) {
					return '4';
				} else {
					foreach ($resultado as $key => $v) {
						$_SESSION['S_IdUsuario'] = utf8_encode($v["user"]);
						$_SESSION['S_Usuario'] = utf8_encode($v["nombre"] . " " . $v["apellido_p"] . " " . $v["apellido_m"]);
						$_SESSION['S_Cargo'] = utf8_encode($v["cargo"]);
						$_SESSION['S_Contraseña'] = utf8_encode($v["pass"]);
						$_SESSION['S_Admin'] = utf8_encode($v["admin"]);
					}
					return '1';
				}
			} else {
				return '0';
			}
		}

        private function listarUsuario(){
		$sql="SELECT U.idusuario  ,U.user, U.id_personal, U.admin, U.activo, p.nombre, p.apellido_p, p.apellido_m, p.cargo, p.DNI, p.telefono, p.activo as activo_personal
					from usuario U 
					inner join personal p
					on p.id_personal = u.id_personal
					where U.activo = '1' order by U.idusuario ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
		return json_encode($resultado);  
		}

		private function insert(){	    	
			$Usuario=$this->param["Usuario"];
			$Clave=$this->param["Clave"];
			$Cargo=$this->param["Cargo"];
			$sql="INSERT usuario(user,pass,id_personal ) values(?,?,?)";
			$stmt= $this->conexion_db->prepare($sql);
			$stmt->execute([$Usuario,$Clave,$Cargo]);
			$this->conexion_db=null;
			return json_encode(1);     
		}
		private function editar(){	    	
			$idUsuario=$this->param["idUsuario"];
			$Usuario=$this->param["Usuario"];
	
			$sql="UPDATE usuario set user='$Usuario' where idusuario =$idUsuario";
			$stmt= $this->conexion_db->prepare($sql);
			$stmt->execute();
			if ($stmt->rowCount()>0) {
				$this->conexion_db=null;
				return json_encode(1); 
			}else{
				$this->conexion_db=null;
				return json_encode(0); 
			}
	    }	 

		private function actualizarContra(){	    	
			$usuario = $_SESSION['S_IdUsuario'];
			$clave = $this->param['Clave'];
			$nclave = $this->param['NuevaClave'];
			$sql = "SELECT COUNT(*) from usuario u WHERE u.user='$usuario'
				and u.pass='$clave' ";
			$sentencia=$this->conexion_db->query($sql);
			 if ($sentencia->fetchColumn()>0) {
				$sql="UPDATE usuario set pass='$nclave' where user='$usuario'";
				$stmt= $this->conexion_db->prepare($sql);
				$stmt->execute();
				if ($stmt->rowCount()>0) {
					$this->conexion_db=null;
					return json_encode(1); 
				}else{
					$this->conexion_db=null;
					return json_encode($usuario.'-'.$clave.'-'.$nclave); 
				}
			 }else{
				$this->conexion_db=null;
				return json_encode($usuario.'-'.$clave.'-'.$nclave); 
			} 
	    }

		private function resetContra(){	    	
			$usuario = $this->param['idUsuario'];
			$sql = "SELECT COUNT(*) from usuario u WHERE u.idusuario='$usuario' ";
			$sentencia=$this->conexion_db->query($sql);
			 if ($sentencia->fetchColumn()>0) {
				$sql="UPDATE usuario set pass='FisioHog@r2022' where idusuario='$usuario' ";
				$stmt= $this->conexion_db->prepare($sql);
				$stmt->execute();
				if ($stmt->rowCount()>0) {
					$this->conexion_db=null;
					return json_encode(1); 
				}else{
					$this->conexion_db=null;
					return json_encode(0); 
				}
			 }else{
				$this->conexion_db=null;
				return json_encode(4); 
			} 
	    }


	    private function eliminar(){
	    	$idUsuario=$this->param["idUsuario"];
       		$sql="UPDATE usuario set activo = '0' where idusuario =$idUsuario";
        	$stmt= $this->conexion_db->prepare($sql);
       		$stmt->execute();
       		if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        	}else{
            $this->conexion_db=null;
            return json_encode(0); 
       		}
        }
        private function llenarcboRol(){
            global $database;
			$sql = "exec sp_mnt_Usuario @peticion=1";
			$database->setQuery($sql);
			$rows=$database->loadObjectList();
            $combo = "<option value='0' disabled='disabled' selected='selected'>Seleccionar</option>";
		    foreach ($rows as $key => $v) {
                $rol=utf8_encode($v->rol);
                $combo .= "<option value='".$v->idRol."'>".$rol."</option>";
		    }
		    return $combo;
        }
        
        
		
	}
?>