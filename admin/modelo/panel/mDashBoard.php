<?php
session_start();

include_once( "../../config/conexion.php");

class DashBoard_model extends Conexion{
    private $param = array();
    public $con;

    public function __construct(){
      parent::Conexion();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'obtenCantidadDoc':
                echo $this->obtenCantidadDoc();
                break;
            case 'listVisita':
                echo $this->listarVisita();
                break;
            case 'listCita':
                echo $this->listarCitas();
                break;
        }
    }

    private function obtenCantidadDoc(){  
    }

    private function listarVisita(){
        
        $fechaInicial=$this->param["FechaInicio"];
        $fechaFinal=$this->param["FechaFin"];

        $DateFechaI= strtotime($fechaInicial); 
        $MesInicial = date("m", $DateFechaI);
        $DateFechaI = date('Y-m-d',$DateFechaI);

        
        $DateFechaF= strtotime($fechaFinal); 
        $MesFinal = date("m", $DateFechaF);
        $DateFechaF = date('Y-m-d',$DateFechaF);
        if($MesInicial != $MesFinal){
            $sql="SELECT YEAR(Fecha) AS YEAR, MONTH(Fecha) AS MONTH,COUNT(IP) as IP, date_format(fecha, '%Y-%m') AS FECHA
                    FROM visita 
                    where Fecha between '$DateFechaI' and '$DateFechaF'
                    GROUP BY YEAR(Fecha), MONTH(Fecha)";

        }else{
            $sql="SELECT YEAR(Fecha) AS YEAR, MONTH(Fecha) AS MONTH, DAY(Fecha) AS DAY,COUNT(IP) as IP, date_format(fecha, '%Y-%m-%d') AS FECHA
            FROM visita 
            where Fecha between '$DateFechaI' and '$DateFechaF'
            GROUP BY YEAR(Fecha), MONTH(Fecha), DAY(Fecha)";
        }
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    
    private function listarCitas(){
        
        $fechaInicial=$this->param["FechaInicio"];
        $fechaFinal=$this->param["FechaFin"];

        $DateFechaI= strtotime($fechaInicial); 
        $DateFechaI = date('Y-m-d',$DateFechaI);

        
        $DateFechaF= strtotime($fechaFinal); 
        $DateFechaF = date('Y-m-d',$DateFechaF);
    	$sql="SELECT ec.descripcion,COUNT(*) as cont
                FROM solicitud_citas sc 
                INNER JOIN estado_cita ec ON ec.idestado = sc.estado
                where sc.fecha_registro between '$DateFechaI' and '$DateFechaF'
                GROUP BY ec.descripcion";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }
}
?>