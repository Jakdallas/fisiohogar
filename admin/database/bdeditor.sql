-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-11-2020 a las 05:40:23
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdeditor`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficios`
--

CREATE TABLE `beneficios` (
  `idbeneficio` int(11) NOT NULL,
  `idprecio` int(11) DEFAULT NULL,
  `beneficio` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `activo` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `beneficios`
--

INSERT INTO `beneficios` (`idbeneficio`, `idprecio`, `beneficio`, `activo`) VALUES
(1, 2, 'Acceso al editor', 1),
(2, 2, 'Acceso a imágenes prediseñadas', 1),
(3, 3, 'Acceso al editor', 1),
(4, 3, 'Acceso a imágenes prediseñadas', 1),
(5, 3, '+5 Puntos Bonus', 1),
(6, 4, 'Acceso al editor', 1),
(7, 4, 'Acceso a imágenes prediseñadas', 1),
(8, 4, '+10 Puntos Bonus', 1),
(9, 4, 'Guarda tus firmas', 1),
(10, 3, '123', 0),
(11, 4, 'wwesds', 0),
(12, 3, 'eeesf', 0),
(13, 3, 'asdad', 0),
(14, 2, 'Firmas gratis 2', 0),
(15, 2, 'asda', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `documento` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombres` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cmp` varchar(6) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ciudad` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ecografo_modelo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ecografo_marca` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `clave` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(9) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `puntos` int(11) DEFAULT 0,
  `firmas` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `documento`, `nombres`, `cmp`, `ciudad`, `ecografo_modelo`, `ecografo_marca`, `correo`, `clave`, `telefono`, `puntos`, `firmas`, `activo`) VALUES
(1, '72048088', 'Carlos Andres Sucre Gamboa', '123456', 'Trujillo', 'AG8P21H62', 'SONIC', 'csucre.420@gmail.com', '0', '972621077', 100, 1, 1),
(2, '72048048', 'Hector Eduardo Rodriguez Lazaro', '123457', 'Trujillo', 'AFEE32SXCWWS', 'SAMSUMG', 'leduardo@gmail.com', '0', '987654321', 0, 0, 1),
(4, '72048058', 'Caroline Ruiz Vidaurre', '123456', 'Trujillo', 'SAMSUMG', 'SAMSUMG', 'csucre.420@gmail.com', '0', '972621077', 35, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `collage`
--

CREATE TABLE `collage` (
  `idcollage` int(11) NOT NULL,
  `url` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `collage_venta`
--

CREATE TABLE `collage_venta` (
  `idcolventa` int(11) NOT NULL,
  `idcollage` int(11) DEFAULT NULL,
  `idcliente` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `admin` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `url` varchar(500) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(9) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `empresa` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `configuration`
--

INSERT INTO `configuration` (`id`, `admin`, `password`, `nombre`, `url`, `telefono`, `email`, `logo`, `empresa`) VALUES
(1, 'admin', 'admin', 'Administrador', '<iframe width=\"100%\" height=\"720\" src=\"https://www.youtube.com/embed/0RgroIMOH2M?list=RD0RgroIMOH2M?autoplay=1&mute=1\" frameborder=\"0\" allow=\"accelerometer; autoplay; mute; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '972621077', 'csucre.420@gmail.com', NULL, 'ECOTINTA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `firmas`
--

CREATE TABLE `firmas` (
  `idfirma` int(11) NOT NULL,
  `idcliente` int(11) DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pricing_page`
--

CREATE TABLE `pricing_page` (
  `idprecio` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `puntos` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `fecha` datetime DEFAULT current_timestamp(),
  `estado` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `pricing_page`
--

INSERT INTO `pricing_page` (`idprecio`, `nombre`, `precio`, `puntos`, `orden`, `fecha`, `estado`) VALUES
(2, 'Silver', '30.00', 30, 1, '2020-11-15 23:10:00', 1),
(3, 'Gold', '50.00', 55, 2, '2020-11-15 23:10:00', 1),
(4, 'Platinum', '100.00', 110, 3, '2020-11-15 23:10:10', 1),
(5, 'Carlos Andres', '3.00', 2, 23, '2020-11-16 22:00:34', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `steps`
--

CREATE TABLE `steps` (
  `idstep` int(11) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `activo` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `steps`
--

INSERT INTO `steps` (`idstep`, `orden`, `url`, `titulo`, `descripcion`, `activo`) VALUES
(1, 1, 'img/services/steps-1.png', 'SELECCIONA FOTOS', 'Selecciona fotos con un tema, para contar una historia con un collage.', 1),
(2, 2, 'img/services/steps-2.png', 'ACOMODA LAS FOTOS', 'Selecciona un diseño y acomoda tus fotos para que tu historia cobre vida.', 1),
(3, 3, 'img/services/steps-3.png', 'PERSONALIZA', 'Añade imagenes prediseñadas y patrones para resaltar tu collage.', 1),
(4, 4, 'img/services/steps-4.png', 'IMPRIME', 'De forma rápida y sencilla', 1),
(9, 5, 'img/services/1605759305_Mesa de trabajo 7.png', 'Example1', 'Example2', 0),
(10, 6, 'img/services/1605758890_Mesa de trabajo 6.png', 'Example1', 'Example2', 0),
(11, 5, 'img/services/1605759718_Mesa de trabajo 6_1.png', 'Example', 'Example', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idventa` int(11) NOT NULL,
  `idcliente` int(11) DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `puntos` int(11) NOT NULL,
  `fecha` datetime DEFAULT current_timestamp(),
  `estado` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idventa`, `idcliente`, `monto`, `puntos`, `fecha`, `estado`) VALUES
(1, 1, '150.00', 100, '2020-11-08 22:54:25', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `beneficios`
--
ALTER TABLE `beneficios`
  ADD PRIMARY KEY (`idbeneficio`),
  ADD KEY `beneficios_pricing` (`idprecio`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indices de la tabla `collage`
--
ALTER TABLE `collage`
  ADD PRIMARY KEY (`idcollage`);

--
-- Indices de la tabla `collage_venta`
--
ALTER TABLE `collage_venta`
  ADD PRIMARY KEY (`idcolventa`),
  ADD KEY `collage_venta_collage` (`idcollage`),
  ADD KEY `collage_cliente_venta` (`idcliente`);

--
-- Indices de la tabla `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `firmas`
--
ALTER TABLE `firmas`
  ADD PRIMARY KEY (`idfirma`),
  ADD KEY `collage_cliente_firmas` (`idcliente`);

--
-- Indices de la tabla `pricing_page`
--
ALTER TABLE `pricing_page`
  ADD PRIMARY KEY (`idprecio`);

--
-- Indices de la tabla `steps`
--
ALTER TABLE `steps`
  ADD PRIMARY KEY (`idstep`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idventa`),
  ADD KEY `cliente_venta` (`idcliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `beneficios`
--
ALTER TABLE `beneficios`
  MODIFY `idbeneficio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `collage`
--
ALTER TABLE `collage`
  MODIFY `idcollage` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `collage_venta`
--
ALTER TABLE `collage_venta`
  MODIFY `idcolventa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `firmas`
--
ALTER TABLE `firmas`
  MODIFY `idfirma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pricing_page`
--
ALTER TABLE `pricing_page`
  MODIFY `idprecio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `steps`
--
ALTER TABLE `steps`
  MODIFY `idstep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idventa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `beneficios`
--
ALTER TABLE `beneficios`
  ADD CONSTRAINT `beneficios_pricing` FOREIGN KEY (`idprecio`) REFERENCES `pricing_page` (`idprecio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `collage_venta`
--
ALTER TABLE `collage_venta`
  ADD CONSTRAINT `collage_cliente_venta` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `collage_venta_collage` FOREIGN KEY (`idcollage`) REFERENCES `collage` (`idcollage`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `firmas`
--
ALTER TABLE `firmas`
  ADD CONSTRAINT `collage_cliente_firmas` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `cliente_venta` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
