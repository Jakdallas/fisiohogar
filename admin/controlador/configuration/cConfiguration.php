<?php 

	include_once '../../modelo/configuration/mConfiguration.php';

	$param = array();
	$param['opcion'] = '';
	$param['Usuario'] = '';
	$param['Clave'] = '';
	$param['NuevaClave'] = '';
	$param['Nombre'] = '';
	$param['Telefono'] = '';
	$param['Correo'] = '';	
	$param['Empresa'] = '';	
	$param['Logo'] = '';	
	$param['Video'] = '';	

	$param['IP'] = '';	
	$param['Pais'] = '';	
	$param['Zona'] = '';	
	$param['Localidad'] = '';	
	

	if(isset($_POST['opcion'])){ $param['opcion'] = $_POST['opcion'];}
	if(isset($_POST['Usuario'])){ $param['Usuario'] = $_SESSION['S_IdUsuario'];}
	if(isset($_POST['Clave'])){ $param['Clave'] = $_POST['Clave'];}
	if(isset($_POST['NuevaClave'])){ $param['NuevaClave'] = $_POST['NuevaClave'];}
	if(isset($_POST['Nombre'])){$param['Nombre'] = $_POST['Nombre'];}
	if(isset($_POST['Telefono'])){$param['Telefono'] = $_POST['Telefono'];}
	if(isset($_POST['Correo'])){$param['Correo'] = $_POST['Correo'];}
	if(isset($_POST['Empresa'])){$param['Empresa'] = $_POST['Empresa'];}
	if(isset($_FILES['Logo'])){$param['Logo'] = $_FILES['Logo'];}
	if(isset($_FILES['Video'])){$param['Video'] = $_FILES['Video'];}

	if(isset($_POST['IP'])){$param['IP'] = $_POST['IP'];}
	if(isset($_POST['Pais'])){$param['Pais'] = $_POST['Pais'];}
	if(isset($_POST['Zona'])){$param['Zona'] = $_POST['Zona'];}
	if(isset($_POST['Localidad'])){$param['Localidad'] = $_POST['Localidad'];}
	
	$Configuration = new Configuration_model();
	echo $Configuration->crud($param);
?>