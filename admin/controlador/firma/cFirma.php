<?php 
	include_once '../../modelo/firma/mFirma.php';

	$param = array();
	$param['opcion'] = '';
	$param['idCliente'] = '';
	$param['idFirma'] = '';
    $param['file'] = '';
    $param['b64file'] = '';
	$param['origin_url'] = '';
	
	if(isset($_POST['opcion'])){ $param['opcion'] = $_POST['opcion'];}
	if(isset($_POST['idCliente'])){ $param['idCliente'] = $_POST['idCliente'];}else{$param['idCliente'] = FALSE;}
	if(isset($_POST['idFirma'])){ $param['idFirma'] = $_POST['idFirma'];}
	if(isset($_FILES['File'])){$param['file'] = $_FILES['File'];}
	if(isset($_POST['b64file'])){$param['b64file'] = $_POST['b64file'];}
	if(isset($_POST['origin_url'])){$param['origin_url'] = $_POST['origin_url'];}

	if(isset($param['file'])){$param['img_type']="file";}else if(isset($param['b64file'])){$param['img_type']="b64";}


	$routes = file_get_contents('../../config/route_map.json');
	$routes = json_decode($routes, true);
	$csig_route = $routes["client_signatures"];
	$server_path = "../../";

	$param["signature_routes"] = $server_path . "/" . $csig_route;
	$param["server_path"] = $server_path;

	
	//echo $_POST['opcion'] . "-" . (string)$param['idCliente'];
	//echo $server_path . " - " . $csig_route;
	$Firma = new Firma_Model();
	echo $Firma->crud($param);
?>