<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FisioHogar - Profesionales Especializados</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Slick Slider CSS -->
    <link href="css/slick-theme.css" rel="stylesheet" />
    <!-- ICONS CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- ICONS CSS -->
    <link href="css/animation.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/style5.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/demo.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/fig-hover.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/component.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="css/shotcode.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="svg-icon.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
</head>

<body class="demo-5">
    <!--WRAPPER START-->
    <div class="wrapper">
        <!-- HEADER -->
        <?php include('includes/header.php') ?>
        <!-- /HEADER -->

        <!--CITY MAIN BANNER START-->
        <div class="city_main_banner">
            <div class="main-banner-slider">
                <div>
                    <figure class="overlay">
                        <img src="extra-images/main-banner-6.jpg" alt="">
                        <div class="banner_text" style="border-radius: 25px;">
                            <!--div class="small_text animated">Bienvienido</div-->
                            <div class="medium_text animated">FisioHogar</div>
                            <div class="large_text animated">Saca tu cita para una mejor atención</div>
                            <div class="banner_btn">
                                <!--a class="theam_btn animated" href="#">Conocer más</a-->
                                <a class="theam_btn animated" href="#" data-toggle="modal"
                                    data-target="#exampleModalCenter">Reservar Cita</a>
                            </div>
                            <!--div class="banner_search_form">
                                <label>Search Here</label>
                                <div class="banner_search_field animated">
                                    <input type="text" placeholder="What  do you want to do">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </div>
                            </div-->
                        </div>
                    </figure>
                </div>
                <div>
                    <figure class="overlay">
                        <img src="extra-images/main-banner-7.jpg" alt="">
                        <div class="banner_text" style="border-radius: 25px;">
                            <!--div class="small_text animated">Bienvienido</div-->
                            <div class="medium_text animated">FisioHogar</div>
                            <div class="large_text animated">Saca tu cita para una mejor atención</div>
                            <div class="banner_btn">
                                <!--a class="theam_btn animated" href="#">Conocer más</a-->
                                <a class="theam_btn animated" href="#" data-toggle="modal"
                                    data-target="#exampleModalCenter">Reservar Cita</a>
                            </div>
                            <!--div class="banner_search_form">
                                <label>Search Here</label>
                                <div class="banner_search_field animated">
                                    <input type="text" placeholder="What  do you want to do">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </div>
                            </div-->
                        </div>
                    </figure>
                </div>
                <div>
                    <figure class="overlay">
                        <img src="extra-images/main-banner-4.jpg" alt="">
                        <div class="banner_text" style="border-radius: 25px;">
                            <!--div class="small_text animated">Bienvienido</div-->
                            <div class="medium_text animated">FisioHogar</div>
                            <div class="large_text animated">Saca tu cita para una mejor atención</div>
                            <!--div class="large_text animated"><span id='text'></span>
                                <div class='console-underscore' id='console'>&#95;</div>
                            </div-->
                            <div class="banner_btn">
                                <!--a class="theam_btn animated" href="#">Conocer más</a-->
                                <a class="theam_btn animated" href="#" data-toggle="modal"
                                    data-target="#exampleModalCenter">Reservar Cita</a>
                            </div>
                            <!--div class="banner_search_form">
                                <label>Search Here</label>
                                <div class="banner_search_field animated">
                                    <input type="text" placeholder="What  do you want to do">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </div>
                            </div-->
                        </div>
                    </figure>
                </div>
                <div>
                    <figure class="overlay">
                        <img src="extra-images/main-banner-8.jpg" alt="">
                        <div class="banner_text" style="border-radius: 25px;">
                            <!--div class="small_text animated">Bienvienido</div-->
                            <div class="medium_text animated">FisioHogar</div>
                            <div class="large_text animated">Saca tu cita para una mejor atención</div>
                            <!--div class="large_text animated"><span id='text'></span>
                                <div class='console-underscore' id='console'>&#95;</div>
                            </div-->
                            <div class="banner_btn">
                                <!--a class="theam_btn animated" href="#">Conocer más</a-->
                                <a class="theam_btn animated" href="#" data-toggle="modal"
                                    data-target="#exampleModalCenter">Reservar Cita</a>
                            </div>
                            <!--div class="banner_search_form">
                                <label>Search Here</label>
                                <div class="banner_search_field animated">
                                    <input type="text" placeholder="What  do you want to do">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </div>
                            </div-->
                        </div>
                    </figure>
                </div>
                <div>
                    <figure class="overlay">
                        <img src="extra-images/main-banner-5.jpg" alt="">
                        <div class="banner_text" style="border-radius: 25px;">
                            <!--div class="small_text animated">Bienvienido</div-->
                            <div class="medium_text animated">FisioHogar</div>
                            <div class="large_text animated">Saca tu cita para una mejor atención</div>
                            <div class="banner_btn">
                                <!--a class="theam_btn animated" href="#">Conocer más</a-->
                                <a class="theam_btn animated" href="#" data-toggle="modal"
                                    data-target="#exampleModalCenter">Reservar Cita</a>
                            </div>
                            <!--div class="banner_search_form">
                                <label>Search Here</label>
                                <div class="banner_search_field animated">
                                    <input type="text" placeholder="What  do you want to do">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </div>
                            </div-->
                        </div>
                    </figure>
                </div>
            </div>
        </div>
        <!--CITY MAIN BANNER END-->

        <!--CITY BANNER SERVICES START-->
        <div class="city_banner_services">
            <div class="container-fluid">
                <div class="city_service_list">
                    <ul>
                        <li>
                            <div class="city_service_text">
                                <span><i class="fa icon-news"></i></span>
                                <h5><a href="#">Blog</a></h5>
                            </div>
                        </li>
                        <li>
                            <div class="city_service_text">
                                <span><i class="fa icon-cursor"></i></span>
                                <h5><a href="https://wa.link/fxm9m8" target="_blank">Atención en
                                        linea</a></h5>
                            </div>
                        </li>
                        <li>
                            <div class="city_service_text">
                                <span><i class="fa icon-heart"></i></span>
                                <h5><a href="#" data-toggle="modal" data-target="#exampleModalCenter">Solicita una
                                        cita</a></h5>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--CITY BANNER SERVICES END-->

        <!--CITY ABOUT WRAP START-->
        <div class="city_about_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="city_about_list">
                            <!--SECTION HEADING START-->
                            <div class="section_heading">
                                <span>NOSOTROS</span>
                                <h2>FisioHogar</h2>
                            </div>
                            <!--SECTION HEADING END-->
                            <div class="city_about_text">
                                <h6>Todos los servicios se proporcionan con integridad, rapidez, profesionalismo y
                                    ética.</h6>
                                <p>Somos licenciados especializados y colegiados en Medicina Física y Rehabilitación,
                                    con un alto rango profesional, orientados a la excelencia y comprometidos con el
                                    éxito de nuestros pacientes, ellos son nuestro principal y único objetivo, por eso
                                    queremos ver al paciente como evoluciona ante su dolor, compartimos una parte de su
                                    vida durante su tratamiento, queremos de esta manera brindar un servicio completo y
                                    de altísima calidad a nivel nacional. Nos destacamos por ser una empresa de
                                    fisioterapia que ofrece atención a domicilio con equipos biomédicos portátiles.
                                </p>
                            </div>
                            <ul class="city_about_link">
                                <li><a href="#"><i class="fa fa-star"></i>Excelencia en la Atención Médica y
                                        Fisioterapéutica.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Calidez en la atención al paciente.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Respeto por la persona.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Compromiso con el paciente y su familia.</a>
                                </li>
                                <li><a href="#"><i class="fa fa-star"></i>Manejo integral y multidisciplinario.</a></li>
                                <li><a href="#"><i class="fa fa-star"></i>Integridad, rapidez, profesionalismo y
                                        ética.</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="city_about_fig">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/about-fisiohogar.jpg" alt="">
                            </figure>
                            <div class="city_about_video">
                                <figure class="">
                                    <img src="extra-images/about_testimonio.jpg" alt="">
                                    <a class="paly_btn hvr-pulse" data-rel="prettyPhoto"
                                        href="https://www.youtube.com/watch?v=drjZX0Pbw6A"><i
                                            class="fa icon-play-button"></i></a>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--CITY ABOUT WRAP END-->
        <!--CITY PROJECT WRAP START-->
        <div class="city_project_wrap">
            <div class="container-fluid">
                <!--SECTION HEADING START-->
                <div class="section_heading center">
                    <span>Servicios</span>
                    <h2>Servicio de laboratorio confiable y de alta calidad </h2>
                </div>
                <!--SECTION HEADING END-->
                <!--div class="city_project_mansory">
                    <ul id="filterable-item-filter-1">
                        <li><a data-value="all" href="#">All</a></li>
                        <li><a data-value="1" href="#">Categories</a></li>
                        <li><a data-value="2" href="#">Eco</a></li>
                        <li><a data-value="3" class="active" href="#">Programs</a></li>
                        <li><a data-value="4" href="#">Social life</a></li>
                        <li><a data-value="5" href="#">Sport</a></li>
                        <li><a data-value="6" href="#">Technology</a></li>
                    </ul>
                </div-->
                <div class="city-project-slider">
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-pediatrica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_pediatrica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_pediatrica">FISIOTERAPIA PEDIÁTRICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-lenguaje.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="terapia_lenguaje">TERAPIAS</a>
                                    <h3><a href="terapia_lenguaje">TERAPIA DE LENGUAJE</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-traumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_traumatologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_traumatologica">FISIOTERAPIA TRAUMATOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-reumatologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_reumatologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_reumatologica">FISIOTERAPIA REUMATOLÓGICA</a>
                                    </h3>
                                </div>
                            </figure>
                        </div>
                    </div>

                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-neurologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_neurologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_neurologica">FISIOTERAPIA NEUROLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-respiratoria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_respiratoria">TERAPIAS</a>
                                    <h3><a href="fisioterapia_respiratoria">FISIOTERAPIA RESPIRATORIA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-complementaria.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="terapia_complementaria">TERAPIAS</a>
                                    <h3><a href="terapia_complementaria">TERAPIA COMPLEMENTARIA Y ALTERNATIVA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-ocupacional.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="terapia_ocupacional">TERAPIAS</a>
                                    <h3><a href="terapia_ocupacional">TERAPIA OCUPACIONAL</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <div>
                        <div class="city_project_fig">
                            <figure class="overlay">
                                <img src="extra-images/service-uroginecologica.jpg" alt="">
                                <div class="city_project_text">
                                    <span><i class="fa "></i></span>
                                    <a href="fisioterapia_uroginecologica">TERAPIAS</a>
                                    <h3><a href="fisioterapia_uroginecologica">FISIOTERAPIA UROGINECOLÓGICA</a></h3>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--CITY PROJECT WRAP END-->

        <!--CITY PROJECT WRAP START-->
        <!--div class="city_project_wrap">
            <div class="container-fluid">
                <div class="section_heading center">
                    <span>Staff</span>
                    <h2>Contamos con el mejor equipo de trabajo</h2>
                </div>
                <div class="city_health2_text text2">
                    <div class="row">
                        <div class="col-md-2 col-sm-2">
                            <div class="city_senior_team">
                                <figure class="box">
                                    <div class="box-layer layer-1"></div>
                                    <div class="box-layer layer-2"></div>
                                    <div class="box-layer layer-3"></div>
                                    <img src="extra-images/senir-team-fig.jpg" alt="">
                                </figure>
                                <div class="city_senior_team_text">
                                    <span>Doctor</span>
                                    <h5>Richard Renouf</h5>
                                    <a href="#">r.renouf@gov.je</a>
                                    <a href="#">01534 482016</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <div class="city_senior_team">
                                <figure class="box">
                                    <div class="box-layer layer-1"></div>
                                    <div class="box-layer layer-2"></div>
                                    <div class="box-layer layer-3"></div>
                                    <img src="extra-images/senir-team-fig1.jpg" alt="">
                                </figure>
                                <div class="city_senior_team_text">
                                    <span>Doctor</span>
                                    <h5>Julie Garbutt</h5>
                                    <a href="#">r.renouf@gov.je</a>
                                    <a href="#">01534 482016</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <div class="city_senior_team">
                                <figure class="box">
                                    <div class="box-layer layer-1"></div>
                                    <div class="box-layer layer-2"></div>
                                    <div class="box-layer layer-3"></div>
                                    <img src="extra-images/senir-team-fig2.jpg" alt="">
                                </figure>
                                <div class="city_senior_team_text">
                                    <span>Doctor</span>
                                    <h5>Richard Renouf</h5>
                                    <a href="#">r.renouf@gov.je</a>
                                    <a href="#">01534 482016</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <div class="city_senior_team">
                                <figure class="box">
                                    <div class="box-layer layer-1"></div>
                                    <div class="box-layer layer-2"></div>
                                    <div class="box-layer layer-3"></div>
                                    <img src="extra-images/senir-team-fig2.jpg" alt="">
                                </figure>
                                <div class="city_senior_team_text">
                                    <span>Doctor</span>
                                    <h5>Richard Renouf</h5>
                                    <a href="#">r.renouf@gov.je</a>
                                    <a href="#">01534 482016</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <div class="city_senior_team">
                                <figure class="box">
                                    <div class="box-layer layer-1"></div>
                                    <div class="box-layer layer-2"></div>
                                    <div class="box-layer layer-3"></div>
                                    <img src="extra-images/senir-team-fig2.jpg" alt="">
                                </figure>
                                <div class="city_senior_team_text">
                                    <span>Doctor</span>
                                    <h5>Richard Renouf</h5>
                                    <a href="#">r.renouf@gov.je</a>
                                    <a href="#">01534 482016</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <div class="city_senior_team">
                                <figure class="box">
                                    <div class="box-layer layer-1"></div>
                                    <div class="box-layer layer-2"></div>
                                    <div class="box-layer layer-3"></div>
                                    <img src="extra-images/senir-team-fig2.jpg" alt="">
                                </figure>
                                <div class="city_senior_team_text">
                                    <span>Doctor</span>
                                    <h5>Richard Renouf</h5>
                                    <a href="#">r.renouf@gov.je</a>
                                    <a href="#">01534 482016</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
                <!--CITY HEALTH TEXT END-->
        <!--</div>
    </div-->
        <!--CITY PROJECT WRAP END-->
        <!--CITY DEPARTMENT WRAP START-->
        <!--<div class="city_department_wrap overlay">
            <div class="bg_white">
                <div class="container-fluid">-->
        <!--SECTION HEADING START-->
        <!--<div class="section_heading margin-bottom">
                        <span>FisioHogar | Profesionales Especializados</span>
                        <h2>Servicios</h2>
                    </div>-->
        <!--SECTION HEADING END-->
        <!--<<div class="city-department-slider">
                        <div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Administration</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig1.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Employment </h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig2.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Education</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig3.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Health Care</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig4.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Travel & Tourism</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig5.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Police Department</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Administration</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig1.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Employment </h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig2.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Education</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig3.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Health Care</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig4.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Travel & Tourism</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="width_control">
                                <div class="city_department_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/department-fig5.jpg" alt="">
                                        <a class="paly_btn" data-rel="prettyPhoto"
                                            href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
                                    </figure>
                                    <div class="city_department_text">
                                        <h5>Police Department</h5>
                                        <p>This is Photoshop's version of Ipsum. </p>
                                        <a href="#">See More<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>>-->
        <!--CITY DEPARTMENT WRAP END-->

        <!--CITY OFFICE WRAP START-->
        <!--<div class="city_office_wrap">
				<div class="bg_white bg_none">
					<div class="container-fluid">
						<div class="city_office_row">
							<div class="city_triagle">
								<span></span>
							</div>
							<div class="center_text">
								<div class="city_office_list">
									<div class="city_office_text">
										<h6>City</h6>
										<h3>Mayor’s Office</h3>
										<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
									</div>
									<div class="city_office_logo">
										<a href="#"><img src="images/office-logo.png" alt=""></a>
									</div>
									<div class="city_office_text pull_right">
										<h6>City</h6>
										<h3>Council Office</h3>
										<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
							<div class="city_triagle text-right">
								<span></span>
							</div>
						</div>
					</div>
				</div>
			</div>-->
        <!--CITY OFFICE WRAP END-->

        <!--CITY BLOG WRAP START-->
        <!--<div class="city_blog_wrap">
            <div class="container">-->
        <!--SECTION HEADING START-->
        <!--<div class="heding_full">
                    <div class="section_heading">
                        <span>FisioHogar | Profesionales Especializados</span>
                        <h2>Programas</h2>
                    </div>
                    <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet Aenean
                        sollicitudin, lorem quis bibendum auctor Proin gravida nibh vel velit auctor aliquet Aenean
                        sollicitudin, lorem quis bibendum auctor</p>
                </div>-->
        <!--SECTION HEADING END-->
        <!--<<div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="city_blog_fig">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/blog-fig.jpg" alt="">
                            </figure>
                            <div class="city_blog_text">
                                <span>Programa</span>
                                <h4>Individual</h4>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor
                                    aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
                                <div class="city_blog_social">
                                    <a class="theam_btn border-color color" href="#" tabindex="0">Ver más</a>
                                    <div class="city_blog_icon_list">
                                        <ul class="social_icon">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                                        </ul>
                                        <a class="share_icon" href="#"><i class="fa icon-social"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="city_blog_fig position">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/blog-hover.jpg" alt="">
                            </figure>
                            <div class="city_blog_text">
                                <span>Programa</span>
                                <h4>Individual</h4>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor
                                    aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
                                <div class="city_blog_social">
                                    <a class="theam_btn border-color color" href="#" tabindex="0">Ver más</a>
                                    <div class="city_blog_icon_list">
                                        <ul class="social_icon">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                                        </ul>
                                        <a class="share_icon" href="#"><i class="fa icon-social"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="city_blog_fig">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/blog-fig1.jpg" alt="">
                            </figure>
                            <div class="city_blog_text">
                                <span>Programa</span>
                                <h4>Empresarial</h4>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor
                                    aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
                                <div class="city_blog_social">
                                    <a class="theam_btn border-color color" href="#" tabindex="0">Ver más</a>
                                    <div class="city_blog_icon_list">
                                        <ul class="social_icon">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                                        </ul>
                                        <a class="share_icon" href="#"><i class="fa icon-social"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="city_blog_fig position">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/blog-hover.jpg" alt="">
                            </figure>
                            <div class="city_blog_text">
                                <span>Programa</span>
                                <h4>Empresarial</h4>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor
                                    aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
                                <div class="city_blog_social">
                                    <a class="theam_btn border-color color" href="#" tabindex="0">Ver más</a>
                                    <div class="city_blog_icon_list">
                                        <ul class="social_icon">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                                        </ul>
                                        <a class="share_icon" href="#"><i class="fa icon-social"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="city_blog_fig">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/blog-fig2.jpg" alt="">
                            </figure>
                            <div class="city_blog_text">
                                <span>Programa</span>
                                <h4>Fisioterapéutico</h4>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor
                                    aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
                                <div class="city_blog_social">
                                    <a class="theam_btn border-color color" href="#" tabindex="0">Ver más</a>
                                    <div class="city_blog_icon_list">
                                        <ul class="social_icon">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                                        </ul>
                                        <a class="share_icon" href="#"><i class="fa icon-social"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="city_blog_fig position">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/blog-hover.jpg" alt="">
                            </figure>
                            <div class="city_blog_text">
                                <span>Programa</span>
                                <h4>Fisioterapéutico</h4>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor
                                    aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
                                <div class="city_blog_social">
                                    <a class="theam_btn border-color color" href="#" tabindex="0">Ver más</a>
                                    <div class="city_blog_icon_list">
                                        <ul class="social_icon">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                                        </ul>
                                        <a class="share_icon" href="#"><i class="fa icon-social"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!--CITY BLOG WRAP END-->

        <!--CITY EVENT WRAP START-->
        <!--<div class="city_event_wrap">
            <div class="bg_white width">
                <div class="container-fluid">-->
        <!--SECTION HEADING START-->
        <!--<div class="heding_full">
                        <div class="section_heading">
                            <span>FisioHogar | Profesionales Especializados</span>
                            <h2>Consultas</h2>
                        </div>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet
                            Aenean sollicitudin, lorem quis bibendum auctor Proin gravida nibh vel velit auctor aliquet
                            Aenean sollicitudin, lorem quis bibendum auctor</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="event_booking_field">
                                <input type="text" placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="event_booking_field">
                                <input type="text" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6">
                            <div class="event_booking_field">
                                <input type="text" placeholder="Subject">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="event_booking_area">
                                <textarea>Enter Your Message Here</textarea>
                            </div>
                            <a class="theam_btn btn2" href="#">Submit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!--CITY EVENT WRAP END-->

        <!--CITY SPECIAL SERVICE START-->
        <!-- <div class="city_special_service"
            style="background: rgba(0, 0, 0, 0) url(images/loking-bg-2.png) repeat scroll 0 0;">
            <div class="container center">
                <div class="col-md-4 ">
                    <div class="special_service_text overlay">
                        <h2 class="custom_size2">Descubre nuestros últimos artículos de interés</h2>
                        <h3></h3>
                        <a class="theam_btn border-color color" href="#">Ver blog</a>
                    </div>
                </div>
            </div>
        </div> -->
        <!--CITY SPECIAL SERVICE END-->
         <!-- CITY TREATMENT WRAP START-->
         <div class="city_treatment_wrap">
            <div class="container">
                <ul class="bxslider bx-pager">
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-1.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://www.youtube.com/watch?v=drjZX0Pbw6A"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Yuliana Pezantes</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-2.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://www.youtube.com/watch?v=AJ3z_l8EWzU"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Valeria Ricalde</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-3.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://www.youtube.com/watch?v=w6RHdyyJFy0"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Shirley Diaz Plasencia</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="city_treatment_fig">
                            <figure class="overlay">
                                <img src="extra-images/bx-pager-4.jpg" alt="">
                                <div class="city_treatment_text">
                                    <a class="paly_btn" data-rel="prettyPhoto"
                                        href="https://www.youtube.com/watch?v=127_A3qv_uA"><i
                                            class="fa fa-play"></i></a>
                                    <h4>Fisihogar tu bienestar en nuestras manos</h4>
                                    <h5>Testimonio - Marcelina</h5>
                                </div>
                            </figure>
                        </div>
                    </li>
                </ul>
                <div id="bx-pager" class="city_treatment_list">
                    <a class="overlay" data-slide-index="0" href=""><img src="extra-images/pagar-list-1.jpg" alt=""></a>
                    <a class="overlay" data-slide-index="1" href=""><img src="extra-images/pagar-list-2.jpg" alt=""></a>
                    <a class="overlay" data-slide-index="2" href=""><img src="extra-images/pagar-list-3.jpg" alt=""></a>
                    <a class="overlay" data-slide-index="3" href=""><img src="extra-images/pagar-list-4.jpg" alt=""></a>
                </div>
            </div>
        </div>
        <!-- CITY TREATMENT WRAP END-->


        <!--SECTION HEADING START-->
        <div class="section_heading center" style="margin-top: 50px;">
            <span>TESTIMONIOS</span>
            <h2>Nuestros clientes son nuestra mejor carta de recomendación</h2>
        </div>
        <!--SECTION HEADING END-->
        <!--CITY CLIENT WRAP START-->
        <div class="city_client_wrap">
            <div class="container">
                <div class="city_client_row">
                    <ul class="bxslider bx-pager">
                        <li>
                            <div class="city_client_fig">
                                <figure class="box">
                                    <div class="box-layer layer-1"></div>
                                    <div class="box-layer layer-2"></div>
                                    <div class="box-layer layer-3"></div>
                                    <img src="extra-images/client-1.jpg" alt="">
                                </figure>
                                <div class="city_client_text">
                                    <p>Para mí fue genial por la clase de profesional que tuve que me ayudo bastante y
                                        la
                                        atencion en el calor de mi hogar. </p>
                                    <h4><a href="https://www.youtube.com/watch?v=drjZX0Pbw6A" target="_blank">Yuliana
                                            Pesantes</a> </h4>
                                    <span><a href="https://www.youtube.com/watch?v=drjZX0Pbw6A"
                                            target="_blank">Paciente</a></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="city_client_fig">
                                <figure class="box">
                                    <div class="box-layer layer-1"></div>
                                    <div class="box-layer layer-2"></div>
                                    <div class="box-layer layer-3"></div>
                                    <img src="extra-images/client-2.jpg" alt="">
                                </figure>
                                <div class="city_client_text">
                                    <p>El ambiente es bonito y hay paciencia para los niños. Lo recomiendo porque su
                                        trato
                                        es paciente y el precio es accesible, gracias a las tarifas promocionales.</p>
                                    <h4><a href="https://www.youtube.com/watch?v=g10-lETzLM8" target="_blank">Jennifer
                                            Baca</a> </h4>
                                    <span><a href="https://www.youtube.com/watch?v=g10-lETzLM8" target="_blank">Madre de
                                            paciente</a></span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="city_client_link" id="bx-pager">
                    <a data-slide-index="0" href="">
                        <div class="client_arrow">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/client-min-1.jpg" alt="">
                            </figure>
                        </div>
                    </a>
                    <a data-slide-index="1" href="">
                        <div class="client_arrow">
                            <figure class="box">
                                <div class="box-layer layer-1"></div>
                                <div class="box-layer layer-2"></div>
                                <div class="box-layer layer-3"></div>
                                <img src="extra-images/client-min-2.jpg" alt="">
                            </figure>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!--CITY CLIENT WRAP END-->

        <!--CITY SPECIAL SERVICE START-->
        <div class="city_special_service">
            <div class="container">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="special_service_text overlay">
                        <h2 class="custom_size2">¿Tienes alguna consulta?</h2>
                        <h3>Escríbenos</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="event_booking_field">
                                    <input type="text" id="txtNombreSide" placeholder="Nombre">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="event_booking_field">
                                    <input type="text" id="txtCiudadSide" placeholder="Ciudad de Procedencia">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="event_booking_field">
                                    <input type="text" id="txtDNISide" placeholder="DNI">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="event_booking_field">
                                    <input type="text" id="txtEmailSide" placeholder="Email">
                                </div>
                            </div>
                            <!--div class="col-md-12">
                                <div class="event_booking_field">
                                    <select class="small">
                                        <option data-display="Please select the service you require ">Please select the
                                            service you require </option>
                                        <option value="1">All Event 1</option>
                                        <option value="2">All Event 2</option>
                                        <option value="4">All Event 3</option>
                                        <option value="4">All Event 4</option>
                                        <option value="4">All Event 5</option>
                                        <option value="4">All Event 6</option>
                                    </select>
                                </div>
                            </div-->
                            <!--div class="col-md-6">
                                <div class="event_booking_field">
                                    <input type="text" placeholder="Subject">
                                </div>
                            </div-->
                            <div class="col-md-12">
                                <div class="event_booking_area">
                                    <textarea id="txtTextSide">Hola! Quiero reservar una cita</textarea>
                                </div>
                                <a class="theam_btn btn2" href="#" id="btnConsultarSide" data-dismiss="modal">Solicitar
                                    cita</a>
                            </div>
                        </div>
                        <!--p><span>Call us at 1800 – 123 456 78 or</span> <span>Visit the Contact Page for more
                                detailed</span> information. </p>
                        <a class="theam_btn border-color color" href="#">Contact Now</a>-->
                    </div>
                </div>
            </div>
        </div>
        <!--CITY SPECIAL SERVICE END-->

        <!--CITY NEWS WRAP START-->
        <!--<div class="city_news_wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">-->
        <!--SECTION HEADING START-->
        <!--<div class="section_heading margin-bottom">
                            <span>FisioHogar | Profesionales Especializados</span>
                            <h2>Blog</h2>
                        </div>-->
        <!--SECTION HEADING START-->
        <!--<<div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="city_news_fig">
                                    <figure class="box">
                                        <div class="box-layer layer-1"></div>
                                        <div class="box-layer layer-2"></div>
                                        <div class="box-layer layer-3"></div>
                                        <img src="extra-images/news-fig.jpg" alt="">
                                    </figure>
                                    <div class="city_news_text">
                                        <h2>A Fundraiser for the City Club</h2>
                                        <ul class="city_news_meta">
                                            <li><a href="#">May 22, 2018</a></li>
                                            <li><a href="#">Public Notices</a></li>
                                        </ul>
                                        <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec
                                            sollicitudin</p>
                                        <a class="theam_btn border-color color" href="#" tabindex="0">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="city_news_row">
                                    <ul>
                                        <li>
                                            <div class="city_news_list">
                                                <figure class="box">
                                                    <div class="box-layer layer-1"></div>
                                                    <div class="box-layer layer-2"></div>
                                                    <div class="box-layer layer-3"></div>
                                                    <img src="extra-images/news-fig1.jpg" alt="">
                                                </figure>
                                                <div class="city_news_list_text">
                                                    <h5>Lorem Ipsum Proin gravida nibh </h5>
                                                    <ul class="city_news_meta">
                                                        <li><a href="#">May 22, 2018</a></li>
                                                        <li><a href="#">Public Notices</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="city_news_list">
                                                <figure class="box">
                                                    <div class="box-layer layer-1"></div>
                                                    <div class="box-layer layer-2"></div>
                                                    <div class="box-layer layer-3"></div>
                                                    <img src="extra-images/news-fig2.jpg" alt="">
                                                </figure>
                                                <div class="city_news_list_text">
                                                    <h5>Lorem Ipsum Proin gravida nibh </h5>
                                                    <ul class="city_news_meta">
                                                        <li><a href="#">May 22, 2018</a></li>
                                                        <li><a href="#">Public Notices</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="city_news_list">
                                                <figure class="box">
                                                    <div class="box-layer layer-1"></div>
                                                    <div class="box-layer layer-2"></div>
                                                    <div class="box-layer layer-3"></div>
                                                    <img src="extra-images/news-fig3.jpg" alt="">
                                                </figure>
                                                <div class="city_news_list_text">
                                                    <h5>Lorem Ipsum Proin gravida nibh </h5>
                                                    <ul class="city_news_meta">
                                                        <li><a href="#">May 22, 2018</a></li>
                                                        <li><a href="#">Public Notices</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="city_news_list">
                                                <figure class="box">
                                                    <div class="box-layer layer-1"></div>
                                                    <div class="box-layer layer-2"></div>
                                                    <div class="box-layer layer-3"></div>
                                                    <img src="extra-images/news-fig4.jpg" alt="">
                                                </figure>
                                                <div class="city_news_list_text">
                                                    <h5>Lorem Ipsum Proin gravida nibh </h5>
                                                    <ul class="city_news_meta">
                                                        <li><a href="#">May 22, 2018</a></li>
                                                        <li><a href="#">Public Notices</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="city_news_list">
                                                <figure class="box">
                                                    <div class="box-layer layer-1"></div>
                                                    <div class="box-layer layer-2"></div>
                                                    <div class="box-layer layer-3"></div>
                                                    <img src="extra-images/news-fig5.jpg" alt="">
                                                </figure>
                                                <div class="city_news_list_text">
                                                    <h5>Lorem Ipsum Proin gravida nibh </h5>
                                                    <ul class="city_news_meta">
                                                        <li><a href="#">May 22, 2018</a></li>
                                                        <li><a href="#">Public Notices</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="city_news_form">
                            <div class="city_news_feild">
                                <span>Signup</span>
                                <h4>Newsletter</h4>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida </p>
                                <div class="city_news_search">
                                    <input type="text" name="text" placeholder="Enter Your Email Adress Here" required>
                                    <button class="theam_btn border-color color">Subcribe Now</button>
                                </div>
                            </div>
                            <div class="city_news_feild feild2">
                                <span>Recent</span>
                                <h4>Documents</h4>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida </p>
                                <div class="city_document_list">
                                    <ul>
                                        <li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015
                                                (27 kB)</a></li>
                                        <li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015
                                                (27 kB)</a></li>
                                        <li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015
                                                (27 kB)</a></li>
                                        <li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015
                                                (27 kB)</a></li>
                                        <li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015
                                                (27 kB)</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!--CITY NEWS WRAP END-->


        <!-- FOOTER -->
        <?php include('includes/footer.php') ?>
        <!-- /FOOTER -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content special_service_text overlay">

                    <button type="button" style=" font-size: 30px; color: #fff !important;" class="close"
                        data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="custom_size2">¿Tienes alguna consulta?</h2>
                    <h3>Escríbenos</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="event_booking_field">
                                <input type="text" id="txtNombreModal" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="event_booking_field">
                                <input type="text" id="txtCiudadModal" placeholder="Ciudad de Procedencia">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="event_booking_field">
                                <input type="text" id="txtDNIModal" placeholder="DNI">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="event_booking_field">
                                <input type="mail" id="txtEmailModal" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="event_booking_field">
                                <input type="text" id="txtTelefonoModal" placeholder="Telefono">
                            </div>
                        </div>
                        <!--div class="col-md-12">
                                <div class="event_booking_field">
                                    <select class="small">
                                        <option data-display="Please select the service you require ">Please select the
                                            service you require </option>
                                        <option value="1">All Event 1</option>
                                        <option value="2">All Event 2</option>
                                        <option value="4">All Event 3</option>
                                        <option value="4">All Event 4</option>
                                        <option value="4">All Event 5</option>
                                        <option value="4">All Event 6</option>
                                    </select>
                                </div>
                            </div-->
                        <!--div class="col-md-6">
                                <div class="event_booking_field">
                                    <input type="text" placeholder="Subject">
                                </div>
                            </div-->
                        <div class="col-md-12">
                            <div class="event_booking_area">
                                <textarea id="txtTextModal">Hola! Quiero reservar una cita</textarea>
                            </div>
                            <a class="theam_btn btn2" href="#" id="btnConsultarModal" data-dismiss="modal">Solicitar
                                cita</a>
                        </div>
                    </div>
                    <!--p><span>Call us at 1800 – 123 456 78 or</span> <span>Visit the Contact Page for more
                                detailed</span> information. </p>
                        <a class="theam_btn border-color color" href="#">Contact Now</a>-->

                    <!--<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar</button>
      </div>>-->
                </div>
            </div>
        </div>

    </div>
    <!--WRAPPER END-->
    <!--Jquery Library-->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <!--<script src="js/jquery.js"></script>-->
    <!--Bootstrap core JavaScript-->
    <script src="js/bootstrap.js"></script>
    <!--Slick Slider JavaScript-->
    <script src="js/slick.min.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.prettyPhoto.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/index.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/modernizr.custom.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/jquery.dlmenu.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/downCount.js"></script>
    <!--Counter up JavaScript-->
    <script src="js/waypoints.js"></script>
    <!--Pretty Photo JavaScript-->
    <script src="js/waypoints-sticky.js"></script>

    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>
    <!--Custom JavaScript-->
    <script src="js/fisiohogar.js"></script>
    <!--Custom JavaScript-->
    <script src="js/visita.js"></script>
    <script>
    document.documentElement.className = 'js';
    </script>
</body>

</html>